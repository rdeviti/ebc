package org.mpisws.ebcmessageapp.recving;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.mpisws.ebcmessageapp.R;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.mpisws.ebcmessageapp.MainActivity.ebc;
import static org.mpisws.ebcmessageapp.recving.MessageView.HIDDEN_TEXT_ID;
import static org.mpisws.ebcmessageapp.recving.MessageView.MSG_TEXT_ID;
import static org.mpisws.helpers.Utils.objFromString;

public class GetMessagesFragment extends Fragment {
    private static ProgressBar mProgress;
    private static Button mRefreshButton;
    private static ListView mMessageList;

    private static final String DATE_FORMAT = "%d/%d/%d";
    private LocalDateTime date = LocalDateTime.now();

    private static final long ENCOUNTER_DURATION = 30000;
    private static final double ENCOUNTER_RADIUS = 0.001; // approximately 1/10 of a mile

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.get_messages, container, false);
        mProgress = view.findViewById(R.id.get_msgs_progressbar);
        mRefreshButton = view.findViewById(R.id.refresh_messages);
        mMessageList = view.findViewById(R.id.messages_list);

        mMessageList.setOnItemClickListener(
                (parent, view1, position, id) -> {
                    ReceivedMessageWrapper rmw = null;
                    try {
                        rmw = (ReceivedMessageWrapper) objFromString(
                                ((TextView) view1.findViewById(HIDDEN_TEXT_ID)).getText().toString());
                        if (rmw == null) return;
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                        return;
                    }
                    final ReceivedMessageWrapper finalRmw = rmw;
                    LayoutInflater li = LayoutInflater.from(getActivity());
                    View layout = li.inflate(R.layout.message_dialog, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                            .setTitle("Reply")
                            .setPositiveButton("Send", (dialog1, whichButton) -> {
                                String msgTxt = ((TextView) layout.findViewById(R.id.subject)).getText().toString()
                                        + ": " + ((TextView) layout.findViewById(R.id.message)).getText().toString();
                                   Log.d("SENDING REPLY", msgTxt + " to " + finalRmw.getEncounterID());
                                    ebc.getCommunicationClient().replyToMessage(msgTxt, finalRmw);
                            });
                    if (rmw.getMsgType() == ReceivedMessageWrapper.MsgTyp.PRIVATE_MESSAGE) {
                        builder.setNegativeButton("GET CHANNEL MSGS", (dialog12, whichButton) -> {
                            Log.d("GETTING MESSAGES from ", finalRmw.getEncounterID());
                            ebc.getCommunicationClient().getConversationOnPrivateChannel(finalRmw.getEncounterID(),
                                    new ESClient.GetReceivedMessagesCallback() {
                                        @Override
                                        public void processReceivedMessages(List<ReceivedMessageWrapper> msgs) {
                                            String displayMsg = "";
                                            for (ReceivedMessageWrapper msg : msgs) {
                                                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd:MM:yyyy");
                                                displayMsg += Instant.ofEpochMilli(msg.getTimestamp())
                                                        .atZone(ZoneId.systemDefault())
                                                        .toLocalDateTime().format(dtf)
                                                        .toString();
                                                displayMsg += msg.getIsMe() ? " (me): " : " (other): " +
                                                        msg.getPrivateMessage().getMsgText() + "\n";
                                            }
                                            String finalDisplayMsg = displayMsg;
                                            getActivity().runOnUiThread(() -> ((TextView) view1.findViewById(MSG_TEXT_ID)).setText(finalDisplayMsg));
                                        }
                                    });

                        });
                    } else {
                        builder.setNegativeButton("Cancel", ((dialog, which) -> {}));
                    }
                    builder.setView(layout)
                            .show();
                });

        mRefreshButton.setOnClickListener(v ->
        {
            mRefreshButton.setEnabled(false);
            getMessages();
            mRefreshButton.setEnabled(true);
        });
        return view;
    }

     /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getMessages() {
        if (! isDeviceOnline()) {
            Toast.makeText(getContext(), "No network connection available.", Toast.LENGTH_LONG).show();
        } else {
            new MakeRequestTask().execute();
        }
    }

    /**
     * An asynchronous task that handles the Google Calendar API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<ReceivedMessageWrapper>> {
        private Exception mLastError = null;

        MakeRequestTask() { }

        /**
         * Background task to call Google Calendar API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<ReceivedMessageWrapper> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private List<ReceivedMessageWrapper> getDataFromApi() throws InterruptedException {
            Object notifyObject = new Object();
            final boolean[] done = {false};
            List<ReceivedMessageWrapper> msgs = new ArrayList<>();

            ESClient.GetReceivedMessagesCallback callback = new ESClient.GetReceivedMessagesCallback() {
                @Override
                public void processReceivedMessages(List<ReceivedMessageWrapper> unreadmsgs) {
                    for (ReceivedMessageWrapper msg : unreadmsgs) {
                        msgs.add(msg);
                    }
                    synchronized(notifyObject) {
                        done[0] = true;
                        notifyObject.notifyAll();
                    }
                }
            };
            ebc.getCommunicationClient().getUnreadMsgs(callback);
            synchronized(notifyObject) {
                if (!done[0]) notifyObject.wait();
            }
            return msgs;
        }

        @Override
        protected void onPreExecute() {
            mMessageList.setAdapter(null);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<ReceivedMessageWrapper> output) {
            mProgress.setVisibility(View.INVISIBLE);
            if (output == null || output.size() == 0) {
                Toast.makeText(getContext(),"No results returned.", Toast.LENGTH_LONG).show();
            } else {
                MessageViewAdapter lvAdapter = new MessageViewAdapter(getContext(), output);
                mMessageList.setAdapter(lvAdapter);
            }
       }

        @Override
        protected void onCancelled() {
            mProgress.setVisibility(View.INVISIBLE);
            if (mLastError != null) {
                Toast.makeText(getContext(),"Error: "+mLastError.getMessage(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(),"Request cancelled.", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
