package org.mpisws.ebcmessageapp.recving;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.ebcmessageapp.R;
import org.mpisws.encounters.encounterhistory.models.MLocation;
import org.mpisws.encounters.lib.time.TimeInterval;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static org.mpisws.ebcmessageapp.MainActivity.ebc;
import static org.mpisws.helpers.Utils.objToString;

class MessageView extends LinearLayout {
    public static final int HIDDEN_TEXT_ID = 22;
    public static final int MSG_TEXT_ID = 20;

    public MessageView(Context context, ReceivedMessageWrapper message)
    {
        super( context );
        setOrientation(LinearLayout.HORIZONTAL);
        setPadding(10,10,10,10);

        LayoutParams Params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LinearLayout PanelV = new LinearLayout(context, null, R.style.Widget_AppCompat_CompoundButton_CheckBox);
        PanelV.setOrientation(LinearLayout.VERTICAL);
        PanelV.setGravity(Gravity.BOTTOM);

        // get encounter information
        String timeStr = "";
        String msgTxt = "";
        List<Address> addresses = null;

        TextView textTitle = new TextView(context);
        textTitle.setTextSize(16);
        textTitle.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        textTitle.setTextColor(Color.BLACK);
        textTitle.setText(message.getEncounterID());
        PanelV.addView(textTitle);


        switch(message.getMsgType()) {
            case ENCOUNTER_FORWARDING_MESSAGE:
                Pair<List<MLocation>, TimeInterval> encInfo = ebc.getSDDRClient().getEncounterInfoFromEID(message.getEncounterID());
                // try and get location
                if (encInfo.getLeft().size() != 0) {
                    Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(encInfo.getLeft().get(0).getLatitude(), encInfo.getLeft().get(0).getLongitude(), 10);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                timeStr = (String.format("%s", encInfo.getRight().toBriefStartEndString()));
                msgTxt = ( message.getEncounterForwardingMessage().getMsgText() );


                TextView textDate = new TextView(context);
                textDate.setTextSize(16);
                textDate.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textDate.setTextColor(Color.BLACK);
                textDate.setText(timeStr);

                TextView textLocation = new TextView(context);
                if (addresses != null && addresses.size() > 0) {
                    textLocation.setTextSize(16);
                    textLocation.setTypeface(Typeface.DEFAULT, Typeface.BOLD_ITALIC);
                    textLocation.setTextColor(Color.DKGRAY);
                    textLocation.setText(String.format("%s", addresses.get(0).getAddressLine(0)));
                }
                PanelV.addView(textDate);
                PanelV.addView(textLocation);

                break;
            case PRIVATE_MESSAGE:
                msgTxt = ( message.getPrivateMessage().getMsgText() );
                break;
            default:
                break;
        }
        TextView textMsg = new TextView( context );
        textMsg.setId(MSG_TEXT_ID);
        textMsg.setTextColor(Color.BLACK);
        textMsg.setTextSize(16);
        textMsg.setText(msgTxt);

        TextView textHidden = new TextView( context );
        textHidden.setId(HIDDEN_TEXT_ID);
        textHidden.setVisibility(INVISIBLE);
        textHidden.setTextSize(0);
        try {
            textHidden.setText(objToString(message));
        } catch (IOException e) {
            textHidden.setText("");
            e.printStackTrace();
        }

        PanelV.addView(textMsg);
        PanelV.addView(textHidden);
        addView(PanelV, Params);
    }
}
