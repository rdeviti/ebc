package org.mpisws.ebcmessageapp.recving;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.util.List;

public class MessageViewAdapter extends BaseAdapter {
    private Context context;
    private List<ReceivedMessageWrapper> messageList;

    public MessageViewAdapter(Context context, List<ReceivedMessageWrapper> messageList) {
        this.context = context;
        this.messageList = messageList;
    }

    public int getCount() {
        return messageList.size();
    }

    public Object getItem(int position) {
        return messageList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ReceivedMessageWrapper msg = messageList.get(position);
        View v = new MessageView(this.context, msg);
        return v;
    }
}
