package org.mpisws.ebcmessageapp.sending;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.api.services.calendar.model.Event;

import org.mpisws.ebcmessageapp.R;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

class CalEventView extends LinearLayout {
    public static final int HIDDEN_TEXT_ID = 22;

    public CalEventView(Context context, Event event)
    {
        super( context );

        long start, end;
        DateTimeFormatter dtf;
        if (event.getStart().getDateTime() != null) {
            start = event.getStart().getDateTime().getValue();
            //start = Instant.ofEpochMilli(event.getStart().getDateTime().getValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
            dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        } else {
            start = event.getStart().getDate().getValue();
            //start = Instant.ofEpochMilli(event.getStart().getDate().getValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
            dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        }
        if (event.getEnd().getDateTime() != null) {
            end = event.getEnd().getDateTime().getValue();
            //end = Instant.ofEpochMilli(event.getEnd().getDateTime().getValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        } else {
            end = event.getEnd().getDate().getValue();
            //end = Instant.ofEpochMilli(event.getEnd().getDate().getValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }

        // try and get location
        List<Address> addresses = null;
        if (event.getLocation() != null) {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocationName(event.getLocation(), 10);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        setTag(event.getId());
        setOrientation(LinearLayout.HORIZONTAL);
        setPadding(10,10,10,10);

        TextView textName = new TextView( context );
        textName.setTextSize(16);
        textName.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        textName.setTextColor(Color.BLACK);
        textName.setText( event.getSummary() );

        TextView textInfo = new TextView( context );
        textInfo.setTextColor(Color.BLACK);
        textInfo.setTextSize(16);

        if (addresses != null && addresses.size() > 0) {
            textInfo.setText(String.format("%s-%s\n%s",
                    Instant.ofEpochMilli(start).atZone(ZoneId.systemDefault()).toLocalDateTime().format(dtf),
                    Instant.ofEpochMilli(end).atZone(ZoneId.systemDefault()).toLocalDateTime().format(dtf),
                    addresses.get(0).getAddressLine(0)));
        }
        else textInfo.setText(String.format("%s-%s",
                Instant.ofEpochMilli(start).atZone(ZoneId.systemDefault()).toLocalDateTime().format(dtf),
                Instant.ofEpochMilli(end).atZone(ZoneId.systemDefault()).toLocalDateTime().format(dtf)));

        TextView textHidden = new TextView( context );
        textHidden.setId(HIDDEN_TEXT_ID);
        textHidden.setVisibility(INVISIBLE);
        textHidden.setTextSize(0);
        textHidden.setText(String.format("%d$%d$%s$%s",
                start, end,
                (addresses != null && addresses.size() > 0) ? Double.toString(addresses.get(0).getLatitude()) : Double.toString(-1),
                (addresses != null && addresses.size() > 0) ? addresses.get(0).getLongitude() : -1));

        LayoutParams Params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LinearLayout PanelV = new LinearLayout(context, null, R.style.Widget_AppCompat_CompoundButton_CheckBox);
        PanelV.setOrientation(LinearLayout.VERTICAL);
        PanelV.setGravity(Gravity.BOTTOM);
        PanelV.addView(textName);
        PanelV.addView(textInfo);
        PanelV.addView(textHidden);
        addView(PanelV, Params);
    }
}
