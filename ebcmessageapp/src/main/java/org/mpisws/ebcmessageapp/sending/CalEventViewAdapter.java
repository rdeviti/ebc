package org.mpisws.ebcmessageapp.sending;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.google.api.services.calendar.model.Event;

import java.util.List;

public class CalEventViewAdapter extends BaseAdapter {
    private Context context;
    private List<Event> eventList;

    public CalEventViewAdapter(Context context, List<Event> eventList ) {
        this.context = context;
        this.eventList = eventList;
    }

    public int getCount() {
        return eventList.size();
    }

    public Object getItem(int position) {
        return eventList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Event event = eventList.get(position);
        View v = new CalEventView(this.context, event);
        return v;
    }
}
