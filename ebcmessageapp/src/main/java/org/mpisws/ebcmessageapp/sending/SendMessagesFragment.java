package org.mpisws.ebcmessageapp.sending;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import org.mpisws.ebcmessageapp.R;
import org.mpisws.messaging.constraints.AllEncountersConstraint;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.SpaceTimeIntersectConstraint;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.regex.Pattern;

import static org.mpisws.ebcmessageapp.MainActivity.ebc;
import static org.mpisws.ebcmessageapp.sending.CalEventView.HIDDEN_TEXT_ID;
import static org.mpisws.ebcmessageapp.MainActivity.REQUEST_AUTHORIZATION;
import static org.mpisws.ebcmessageapp.MainActivity.mCredential;
import static org.mpisws.ebcmessageapp.MainActivity.showGooglePlayServicesAvailabilityErrorDialog;

public class SendMessagesFragment extends Fragment {
    private static ProgressBar mProgress;
    private static Button mDateButton;
    private static ListView mEventListView;

    private static final String DATE_FORMAT = "%d/%d/%d";
    private LocalDateTime date = LocalDateTime.now();

    private static final long ENCOUNTER_DURATION = 30000;
    private static final double ENCOUNTER_RADIUS = 0.001; // approximately 1/10 of a mile

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_messages, container, false);
        mProgress = view.findViewById(R.id.send_msgs_progressbar);
        mDateButton = view.findViewById(R.id.getcalendarevents);
        mDateButton.setText(String.format(DATE_FORMAT, date.getMonthValue(), date.getDayOfMonth(), date.getYear()));
        mEventListView = view.findViewById(R.id.calendar_events_list);

        mEventListView.setOnItemClickListener(
                (parent, view1, position, id) -> {
                    String text = ((TextView) view1.findViewById(HIDDEN_TEXT_ID)).getText().toString();
                    Log.d("Clicked!", text);
                    String[] parts = text.split(Pattern.quote("$"));
                    if (parts.length != 4) return;
                    long startTimeMilli = Long.valueOf(parts[0]);
                    long endTimeMilli = Long.valueOf(parts[1]);
                    double lat = Double.valueOf(parts[2]);
                    double longi = Double.valueOf(parts[3]);
                    SpaceTimeIntersectConstraint constraint;
                    if (lat == -1.0) {
                        constraint = new SpaceTimeIntersectConstraint(startTimeMilli, endTimeMilli, ENCOUNTER_DURATION, null, false);
                    } else {
                        EncounterQueryConstraint.SpaceRegion spaceRegion = new EncounterQueryConstraint.SpaceRegion(lat, longi, ENCOUNTER_RADIUS);
                        constraint = new SpaceTimeIntersectConstraint(startTimeMilli, endTimeMilli, ENCOUNTER_DURATION, spaceRegion, false);
                    }
                    Log.d("New Constraint!", constraint.toQueryString());

                    LayoutInflater li = LayoutInflater.from(getActivity());
                    View layout = li.inflate(R.layout.message_dialog, null);
                    new AlertDialog.Builder(getContext())
                            .setTitle("Send a Message")
                            .setPositiveButton("Send", (dialog1, whichButton) -> {
                                String msgTxt = ((TextView) layout.findViewById(R.id.subject)).getText().toString()
                                        + ": " + ((TextView) layout.findViewById(R.id.message)).getText().toString();
                                Log.d("Message", msgTxt);

                                // TODO
                                AllEncountersConstraint allconstraint = new AllEncountersConstraint();
                                // enabling replies
                                ebc.getSDDRClient().updateDatabaseOnAgent();
                                ebc.getCommunicationClient().sendDirectMessageToEncounters(msgTxt, allconstraint, true);
                            })
                            .setNegativeButton("Cancel", (dialog12, whichButton) -> {
                            })
                            .setView(layout)
                            .show();
                });

        mDateButton.setOnClickListener(v ->
        {
            mDateButton.setEnabled(false);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getView().getContext(),
                    (view12, year, monthOfYear, dayOfMonth) -> {
                        date = LocalDateTime.of(year, monthOfYear + 1, dayOfMonth, 0, 0, 0);
                        mDateButton.setText(String.format(DATE_FORMAT, date.getMonthValue(), date.getDayOfMonth(), date.getYear()));

                        // actually set the data once we get the date
                        getResultsFromApi();
                        mDateButton.setEnabled(true);
                    }, date.getYear(), date.getMonthValue() - 1, date.getDayOfMonth());
            datePickerDialog.setOnCancelListener(dialog -> {
                mDateButton.setEnabled(true);
            });
            datePickerDialog.show();
        });
        return view;
    }

     /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (! isDeviceOnline()) {
            Toast.makeText(getContext(), "No network connection available.", Toast.LENGTH_LONG).show();
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    /**
     * An asynchronous task that handles the Google Calendar API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<Event>> {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("EbC")
                    .build();
        }

        /**
         * Background task to call Google Calendar API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<Event> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private List<Event> getDataFromApi() throws IOException {
            // List the next 50 events from the primary calendar during the selected day
            DateTime start = new DateTime(date.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli());
            DateTime end = new DateTime(date.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()+(24*60*60*1000));
            Events events = mService.events().list("primary")
                    .setMaxResults(50)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .setTimeMin(start)
                    .setTimeMax(end)
                    .execute();
            return events.getItems();
        }

        @Override
        protected void onPreExecute() {
            mEventListView.setAdapter(null);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<Event> output) {
            mProgress.setVisibility(View.INVISIBLE);
            if (output == null || output.size() == 0) {
                Toast.makeText(getContext(),"No results returned.", Toast.LENGTH_LONG).show();
            } else {
                CalEventViewAdapter lvAdapter = new CalEventViewAdapter(getContext(), output);
                mEventListView.setAdapter(lvAdapter);
            }
       }

        @Override
        protected void onCancelled() {
            mProgress.setVisibility(View.INVISIBLE);
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode(),
                            getActivity());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(), REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getContext(),"Error: "+mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getContext(),"Request cancelled.", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
