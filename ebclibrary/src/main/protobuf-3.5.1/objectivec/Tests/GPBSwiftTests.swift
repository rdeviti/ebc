// Protocol Buffers - Google's data interchange format
// Copyright 2015 Google Inc.  All rights reserved.
// https://developers.google.com/protocol-buffers/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation
import XCTest

// Test some usage of the ObjC library from Swift.

class GPBBridgeTests: XCTestCase {

  func testProto2Basics() {
    let esMessage = Message2()
    let msg2 = Message2()
    let msg3 = Message2_OptionalGroup()

    esMessage.optionalInt32 = 100
    esMessage.optionalString = "abc"
    esMessage.optionalEnum = .bar
    msg2.optionalString = "other"
    esMessage.optional = msg2
    msg3.a = 200
    esMessage.optionalGroup = msg3
    esMessage.repeatedInt32Array.addValue(300)
    esMessage.repeatedInt32Array.addValue(301)
    esMessage.repeatedStringArray.add("mno")
    esMessage.repeatedStringArray.add("pqr")
    esMessage.repeatedEnumArray.addValue(Message2_Enum.bar.rawValue)
    esMessage.repeatedEnumArray.addValue(Message2_Enum.baz.rawValue)
    esMessage.mapInt32Int32.setInt32(400, forKey:500)
    esMessage.mapInt32Int32.setInt32(401, forKey:501)
    esMessage.mapStringString.setObject("foo", forKey:"bar" as NSString)
    esMessage.mapStringString.setObject("abc", forKey:"xyz" as NSString)
    esMessage.mapInt32Enum.setEnum(Message2_Enum.bar.rawValue, forKey:600)
    esMessage.mapInt32Enum.setEnum(Message2_Enum.baz.rawValue, forKey:601)

    // Check has*.
    XCTAssertTrue(esMessage.hasOptionalInt32)
    XCTAssertTrue(esMessage.hasOptionalString)
    XCTAssertTrue(esMessage.hasOptionalEnum)
    XCTAssertTrue(msg2.hasOptionalString)
    XCTAssertTrue(esMessage.hasOptionalMessage)
    XCTAssertTrue(msg3.hasA)
    XCTAssertTrue(esMessage.hasOptionalGroup)
    XCTAssertFalse(esMessage.hasOptionalInt64)
    XCTAssertFalse(esMessage.hasOptionalFloat)

    // Check values.
    XCTAssertEqual(esMessage.optionalInt32, Int32(100))
    XCTAssertEqual(esMessage.optionalString, "abc")
    XCTAssertEqual(msg2.optionalString, "other")
    XCTAssertTrue(esMessage.optional === msg2)
    XCTAssertEqual(esMessage.optionalEnum, Message2_Enum.bar)
    XCTAssertEqual(msg3.a, Int32(200))
    XCTAssertTrue(esMessage.optionalGroup === msg3)
    XCTAssertEqual(esMessage.repeatedInt32Array.count, UInt(2))
    XCTAssertEqual(esMessage.repeatedInt32Array.value(at: 0), Int32(300))
    XCTAssertEqual(esMessage.repeatedInt32Array.value(at: 1), Int32(301))
    XCTAssertEqual(esMessage.repeatedStringArray.count, Int(2))
    XCTAssertEqual(esMessage.repeatedStringArray.object(at: 0) as? String, "mno")
    XCTAssertEqual(esMessage.repeatedStringArray.object(at: 1) as? String, "pqr")
    XCTAssertEqual(esMessage.repeatedEnumArray.count, UInt(2))
    XCTAssertEqual(esMessage.repeatedEnumArray.value(at: 0), Message2_Enum.bar.rawValue)
    XCTAssertEqual(esMessage.repeatedEnumArray.value(at: 1), Message2_Enum.baz.rawValue)
    XCTAssertEqual(esMessage.repeatedInt64Array.count, UInt(0))
    XCTAssertEqual(esMessage.mapInt32Int32.count, UInt(2))
    var intValue: Int32 = 0
    XCTAssertTrue(esMessage.mapInt32Int32.getInt32(&intValue, forKey: 500))
    XCTAssertEqual(intValue, Int32(400))
    XCTAssertTrue(esMessage.mapInt32Int32.getInt32(&intValue, forKey: 501))
    XCTAssertEqual(intValue, Int32(401))
    XCTAssertEqual(esMessage.mapStringString.count, Int(2))
    XCTAssertEqual(esMessage.mapStringString.object(forKey: "bar") as? String, "foo")
    XCTAssertEqual(esMessage.mapStringString.object(forKey: "xyz") as? String, "abc")
    XCTAssertEqual(esMessage.mapInt32Enum.count, UInt(2))
    XCTAssertTrue(esMessage.mapInt32Enum.getEnum(&intValue, forKey:600))
    XCTAssertEqual(intValue, Message2_Enum.bar.rawValue)
    XCTAssertTrue(esMessage.mapInt32Enum.getEnum(&intValue, forKey:601))
    XCTAssertEqual(intValue, Message2_Enum.baz.rawValue)

    // Clearing a string with nil.
    msg2.optionalString = nil
    XCTAssertFalse(msg2.hasOptionalString)
    XCTAssertEqual(msg2.optionalString, "")

    // Clearing a message with nil.
    esMessage.optionalGroup = nil
    XCTAssertFalse(esMessage.hasOptionalGroup)
    XCTAssertTrue(esMessage.optionalGroup !== msg3)  // New instance

    // Clear.
    esMessage.clear()
    XCTAssertFalse(esMessage.hasOptionalInt32)
    XCTAssertFalse(esMessage.hasOptionalString)
    XCTAssertFalse(esMessage.hasOptionalEnum)
    XCTAssertFalse(esMessage.hasOptionalMessage)
    XCTAssertFalse(esMessage.hasOptionalInt64)
    XCTAssertFalse(esMessage.hasOptionalFloat)
    XCTAssertEqual(esMessage.optionalInt32, Int32(0))
    XCTAssertEqual(esMessage.optionalString, "")
    XCTAssertTrue(esMessage.optional !== msg2)  // New instance
    XCTAssertEqual(esMessage.optionalEnum, Message2_Enum.foo)  // Default
    XCTAssertEqual(esMessage.repeatedInt32Array.count, UInt(0))
    XCTAssertEqual(esMessage.repeatedStringArray.count, Int(0))
    XCTAssertEqual(esMessage.repeatedEnumArray.count, UInt(0))
    XCTAssertEqual(esMessage.mapInt32Int32.count, UInt(0))
    XCTAssertEqual(esMessage.mapStringString.count, Int(0))
    XCTAssertEqual(esMessage.mapInt32Enum.count, UInt(0))
  }

  func testProto3Basics() {
    let esMessage = Message3()
    let msg2 = Message3()

    esMessage.optionalInt32 = 100
    esMessage.optionalString = "abc"
    esMessage.optionalEnum = .bar
    msg2.optionalString = "other"
    esMessage.optional = msg2
    esMessage.repeatedInt32Array.addValue(300)
    esMessage.repeatedInt32Array.addValue(301)
    esMessage.repeatedStringArray.add("mno")
    esMessage.repeatedStringArray.add("pqr")
    // "proto3" syntax lets enum get unknown values.
    esMessage.repeatedEnumArray.addValue(Message3_Enum.bar.rawValue)
    esMessage.repeatedEnumArray.addRawValue(666)
    SetMessage3_OptionalEnum_RawValue(msg2, 666)
    esMessage.mapInt32Int32.setInt32(400, forKey:500)
    esMessage.mapInt32Int32.setInt32(401, forKey:501)
    esMessage.mapStringString.setObject("foo", forKey:"bar" as NSString)
    esMessage.mapStringString.setObject("abc", forKey:"xyz" as NSString)
    esMessage.mapInt32Enum.setEnum(Message2_Enum.bar.rawValue, forKey:600)
    // "proto3" syntax lets enum get unknown values.
    esMessage.mapInt32Enum.setRawValue(666, forKey:601)

    // Has only exists on for message fields.
    XCTAssertTrue(esMessage.hasOptionalMessage)
    XCTAssertFalse(msg2.hasOptionalMessage)

    // Check values.
    XCTAssertEqual(esMessage.optionalInt32, Int32(100))
    XCTAssertEqual(esMessage.optionalString, "abc")
    XCTAssertEqual(msg2.optionalString, "other")
    XCTAssertTrue(esMessage.optional === msg2)
    XCTAssertEqual(esMessage.optionalEnum, Message3_Enum.bar)
    XCTAssertEqual(esMessage.repeatedInt32Array.count, UInt(2))
    XCTAssertEqual(esMessage.repeatedInt32Array.value(at: 0), Int32(300))
    XCTAssertEqual(esMessage.repeatedInt32Array.value(at: 1), Int32(301))
    XCTAssertEqual(esMessage.repeatedStringArray.count, Int(2))
    XCTAssertEqual(esMessage.repeatedStringArray.object(at: 0) as? String, "mno")
    XCTAssertEqual(esMessage.repeatedStringArray.object(at: 1) as? String, "pqr")
    XCTAssertEqual(esMessage.repeatedInt64Array.count, UInt(0))
    XCTAssertEqual(esMessage.repeatedEnumArray.count, UInt(2))
    XCTAssertEqual(esMessage.repeatedEnumArray.value(at: 0), Message3_Enum.bar.rawValue)
    XCTAssertEqual(esMessage.repeatedEnumArray.value(at: 1), Message3_Enum.gpbUnrecognizedEnumeratorValue.rawValue)
    XCTAssertEqual(esMessage.repeatedEnumArray.rawValue(at: 1), 666)
    XCTAssertEqual(msg2.optionalEnum, Message3_Enum.gpbUnrecognizedEnumeratorValue)
    XCTAssertEqual(Message3_OptionalEnum_RawValue(msg2), Int32(666))
    XCTAssertEqual(esMessage.mapInt32Int32.count, UInt(2))
    var intValue: Int32 = 0
    XCTAssertTrue(esMessage.mapInt32Int32.getInt32(&intValue, forKey:500))
    XCTAssertEqual(intValue, Int32(400))
    XCTAssertTrue(esMessage.mapInt32Int32.getInt32(&intValue, forKey:501))
    XCTAssertEqual(intValue, Int32(401))
    XCTAssertEqual(esMessage.mapStringString.count, Int(2))
    XCTAssertEqual(esMessage.mapStringString.object(forKey: "bar") as? String, "foo")
    XCTAssertEqual(esMessage.mapStringString.object(forKey: "xyz") as? String, "abc")
    XCTAssertEqual(esMessage.mapInt32Enum.count, UInt(2))
    XCTAssertTrue(esMessage.mapInt32Enum.getEnum(&intValue, forKey:600))
    XCTAssertEqual(intValue, Message2_Enum.bar.rawValue)
    XCTAssertTrue(esMessage.mapInt32Enum.getEnum(&intValue, forKey:601))
    XCTAssertEqual(intValue, Message3_Enum.gpbUnrecognizedEnumeratorValue.rawValue)
    XCTAssertTrue(esMessage.mapInt32Enum.getRawValue(&intValue, forKey:601))
    XCTAssertEqual(intValue, 666)

    // Clearing a string with nil.
    msg2.optionalString = nil
    XCTAssertEqual(msg2.optionalString, "")

    // Clearing a message with nil.
    esMessage.optional = nil
    XCTAssertFalse(esMessage.hasOptionalMessage)
    XCTAssertTrue(esMessage.optional !== msg2)  // New instance

    // Clear.
    esMessage.clear()
    XCTAssertFalse(esMessage.hasOptionalMessage)
    XCTAssertEqual(esMessage.optionalInt32, Int32(0))
    XCTAssertEqual(esMessage.optionalString, "")
    XCTAssertTrue(esMessage.optional !== msg2)  // New instance
    XCTAssertEqual(esMessage.optionalEnum, Message3_Enum.foo)  // Default
    XCTAssertEqual(esMessage.repeatedInt32Array.count, UInt(0))
    XCTAssertEqual(esMessage.repeatedStringArray.count, Int(0))
    XCTAssertEqual(esMessage.repeatedEnumArray.count, UInt(0))
    msg2.clear()
    XCTAssertEqual(msg2.optionalEnum, Message3_Enum.foo)  // Default
    XCTAssertEqual(Message3_OptionalEnum_RawValue(msg2), Message3_Enum.foo.rawValue)
    XCTAssertEqual(esMessage.mapInt32Int32.count, UInt(0))
    XCTAssertEqual(esMessage.mapStringString.count, Int(0))
    XCTAssertEqual(esMessage.mapInt32Enum.count, UInt(0))
  }

  func testAutoCreation() {
    let esMessage = Message2()

    XCTAssertFalse(esMessage.hasOptionalGroup)
    XCTAssertFalse(esMessage.hasOptionalMessage)

    // Access shouldn't result in has* but should return objects.
    let msg2 = esMessage.optionalGroup
    let msg3 = esMessage.optional.optional
    let msg4 = esMessage.optional
    XCTAssertNotNil(msg2)
    XCTAssertNotNil(msg3)
    XCTAssertFalse(esMessage.hasOptionalGroup)
    XCTAssertFalse(esMessage.optional.hasOptionalMessage)
    XCTAssertFalse(esMessage.hasOptionalMessage)

    // Setting things should trigger has* getting set.
    esMessage.optionalGroup.a = 10
    esMessage.optional.optional.optionalInt32 = 100
    XCTAssertTrue(esMessage.hasOptionalGroup)
    XCTAssertTrue(esMessage.optional.hasOptionalMessage)
    XCTAssertTrue(esMessage.hasOptionalMessage)

    // And they should be the same pointer as before.
    XCTAssertTrue(msg2 === esMessage.optionalGroup)
    XCTAssertTrue(msg3 === esMessage.optional.optional)
    XCTAssertTrue(msg4 === esMessage.optional)

    // Clear gets us new objects next time around.
    esMessage.clear()
    XCTAssertFalse(esMessage.hasOptionalGroup)
    XCTAssertFalse(esMessage.optional.hasOptionalMessage)
    XCTAssertFalse(esMessage.hasOptionalMessage)
    esMessage.optionalGroup.a = 20
    esMessage.optional.optional.optionalInt32 = 200
    XCTAssertTrue(esMessage.hasOptionalGroup)
    XCTAssertTrue(esMessage.optional.hasOptionalMessage)
    XCTAssertTrue(esMessage.hasOptionalMessage)
    XCTAssertTrue(msg2 !== esMessage.optionalGroup)
    XCTAssertTrue(msg3 !== esMessage.optional.optional)
    XCTAssertTrue(msg4 !== esMessage.optional)

    // Explicit set of a message, means autocreated object doesn't bind.
    esMessage.clear()
    let autoCreated = esMessage.optional
    XCTAssertFalse(esMessage.hasOptionalMessage)
    let msg5 = Message2()
    msg5.optionalInt32 = 123
    esMessage.optional = msg5
    XCTAssertTrue(esMessage.hasOptionalMessage)
    // Modifing the autocreated doesn't replaced the explicit set one.
    autoCreated?.optionalInt32 = 456
    XCTAssertTrue(esMessage.hasOptionalMessage)
    XCTAssertTrue(esMessage.optional === msg5)
    XCTAssertEqual(esMessage.optional.optionalInt32, Int32(123))
  }

  func testProto2OneOfSupport() {
    let esMessage = Message2()

    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.gpbUnsetOneOfCase)
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(110.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.baz)  // Default
    let autoCreated = esMessage.oneof  // Default create one.
    XCTAssertNotNil(autoCreated)
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.gpbUnsetOneOfCase)

    esMessage.oneofInt32 = 10
    XCTAssertEqual(esMessage.oneofInt32, Int32(10))
    XCTAssertEqual(esMessage.oneofFloat, Float(110.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.baz)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofInt32)

    esMessage.oneofFloat = 20.0
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(20.0))
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.baz)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofFloat)

    esMessage.oneofEnum = .bar
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(110.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.bar)
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofEnum)

    // Sets via the autocreated instance.
    esMessage.oneof.optionalInt32 = 200
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(110.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.baz)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(200))
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofMessage)

    // Clear the oneof.
    Message2_ClearOOneOfCase(esMessage)
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(110.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message2_Enum.baz)  // Default
    let autoCreated2 = esMessage.oneof  // Default create one
    XCTAssertNotNil(autoCreated2)
    XCTAssertTrue(autoCreated2 !== autoCreated)  // New instance
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.gpbUnsetOneOfCase)

    esMessage.oneofInt32 = 10
    XCTAssertEqual(esMessage.oneofInt32, Int32(10))
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofInt32)

    // Confirm Message.clear() handles the oneof correctly.
    esMessage.clear()
    XCTAssertEqual(esMessage.oneofInt32, Int32(100))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.gpbUnsetOneOfCase)

    // Sets via the autocreated instance.
    esMessage.oneof.optionalInt32 = 300
    XCTAssertTrue(esMessage.oneof !== autoCreated)  // New instance
    XCTAssertTrue(esMessage.oneof !== autoCreated2)  // New instance
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(300))
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.oneofMessage)

    // Set message to nil clears the oneof.
    esMessage.oneof = nil
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message2_O_OneOfCase.gpbUnsetOneOfCase)
}

  func testProto3OneOfSupport() {
    let esMessage = Message3()

    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.gpbUnsetOneOfCase)
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(0.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.foo)  // Default
    let autoCreated = esMessage.oneof  // Default create one.
    XCTAssertNotNil(autoCreated)
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.gpbUnsetOneOfCase)

    esMessage.oneofInt32 = 10
    XCTAssertEqual(esMessage.oneofInt32, Int32(10))
    XCTAssertEqual(esMessage.oneofFloat, Float(0.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.foo)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofInt32)

    esMessage.oneofFloat = 20.0
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(20.0))
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.foo)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofFloat)

    esMessage.oneofEnum = .bar
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(0.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.bar)
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofEnum)

    // Sets via the autocreated instance.
    esMessage.oneof.optionalInt32 = 200
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(0.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.foo)  // Default
    XCTAssertTrue(esMessage.oneof === autoCreated)  // Still the same
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(200))
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofMessage)

    // Clear the oneof.
    Message3_ClearOOneOfCase(esMessage)
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oneofFloat, Float(0.0))  // Default
    XCTAssertEqual(esMessage.oneofEnum, Message3_Enum.foo)  // Default
    let autoCreated2 = esMessage.oneof  // Default create one
    XCTAssertNotNil(autoCreated2)
    XCTAssertTrue(autoCreated2 !== autoCreated)  // New instance
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.gpbUnsetOneOfCase)

    esMessage.oneofInt32 = 10
    XCTAssertEqual(esMessage.oneofInt32, Int32(10))
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofInt32)

    // Confirm Message.clear() handles the oneof correctly.
    esMessage.clear()
    XCTAssertEqual(esMessage.oneofInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.gpbUnsetOneOfCase)

    // Sets via the autocreated instance.
    esMessage.oneof.optionalInt32 = 300
    XCTAssertTrue(esMessage.oneof !== autoCreated)  // New instance
    XCTAssertTrue(esMessage.oneof !== autoCreated2)  // New instance
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(300))
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.oneofMessage)

    // Set message to nil clears the oneof.
    esMessage.oneof = nil
    XCTAssertEqual(esMessage.oneof.optionalInt32, Int32(0))  // Default
    XCTAssertEqual(esMessage.oOneOfCase, Message3_O_OneOfCase.gpbUnsetOneOfCase)
  }

  func testSerialization() {
    let esMessage = Message2()

    esMessage.optionalInt32 = 100
    esMessage.optionalInt64 = 101
    esMessage.optionalGroup.a = 102
    esMessage.repeatedStringArray.add("abc")
    esMessage.repeatedStringArray.add("def")
    esMessage.mapInt32Int32.setInt32(200, forKey:300)
    esMessage.mapInt32Int32.setInt32(201, forKey:201)
    esMessage.mapStringString.setObject("foo", forKey:"bar" as NSString)
    esMessage.mapStringString.setObject("abc", forKey:"xyz" as NSString)

    let data = esMessage.data()

    let msg2 = try! Message2(data: data!)
    XCTAssertTrue(msg2 !== esMessage)  // New instance
    XCTAssertEqual(esMessage.optionalInt32, Int32(100))
    XCTAssertEqual(esMessage.optionalInt64, Int64(101))
    XCTAssertEqual(esMessage.optionalGroup.a, Int32(102))
    XCTAssertEqual(esMessage.repeatedStringArray.count, Int(2))
    XCTAssertEqual(esMessage.mapInt32Int32.count, UInt(2))
    XCTAssertEqual(esMessage.mapStringString.count, Int(2))
    XCTAssertEqual(msg2, esMessage)
  }

}
