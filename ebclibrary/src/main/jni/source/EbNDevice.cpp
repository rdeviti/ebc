#include "EbNDevice.h"

#include "Logger.h"

extern uint64_t sddrStartTimestamp;

using namespace std;

EbNDevice::EbNDevice(DeviceID id, const std::string &advert)
   : id_(id),
     advert_(advert),
     rssiToReport_(),
     lastReportTime_(0),
     shakenHands_(false)
{
}

EbNDevice::~EbNDevice()
{
}

bool EbNDevice::getEncounterInfo(EncounterEvent &dest, bool expired, bool retroactive)
{
  return getEncounterInfo(dest, numeric_limits<uint64_t>::max(), expired, retroactive);
}

void EbNDevice::getAdvert(EncounterEvent &dest)
{
    dest.advert = advert_;
}

bool EbNDevice::getEncounterInfo(EncounterEvent &dest, uint64_t rssiReportingInterval, bool expired, bool retroactive)
{
  bool success = false;

  if(expired)
  {
    LOG_P(TAG, "getEncounterInfo -- expired");
    dest.type = EncounterEvent::Ended;
    dest.id = id_;

    if(!rssiToReport_.empty())
    {
      dest.rssiEvents = rssiToReport_;
      rssiToReport_.clear();
    }

    success = true;
  }
  else
  {
    const bool reportRSSI = !rssiToReport_.empty() && ((getTimeMS() - lastReportTime_) > rssiReportingInterval);
    const bool isUpdated = shakenHands_ && reportRSSI;
    LOG_P(TAG, "getEncounterInfo -- Updated ? %d", isUpdated);
    if(isUpdated)
    {
      dest.type = EncounterEvent::Updated;
      dest.id = id_;

      if(!rssiToReport_.empty())
      {
        dest.rssiEvents = rssiToReport_;
        rssiToReport_.clear();
      }
      dest.advert = advert_;
      lastReportTime_ = getTimeMS();
   }

    success = isUpdated;
  }

  return success;
}
