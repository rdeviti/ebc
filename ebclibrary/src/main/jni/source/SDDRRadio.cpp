#include "SDDRRadio.h"

using namespace std;

uint64_t sddrStartTimestamp = getTimeMS();

SDDRRadio::SDDRRadio(size_t keySize, int adapterID, EbNHystPolicy hystPolicy, uint64_t rssiReportInterval)
: nextDeviceID_(0),
     keySize_(keySize),
     setMutex_(),
     recentDevices_(),
     idToRecentDevices_(),
     deviceMap_(),
     advert_dhExchange_(keySize),
     advert_dhExchangeMutex_(),
     new_message_dhExchange_(keySize),
     hystPolicy_(hystPolicy),
     rssiReportInterval_(rssiReportInterval)
{
    // initialize the random number generator
    FILE *urand = fopen("/dev/urandom", "r");
    unsigned int seed = 0;
    for(int b = 0; b < sizeof(seed); b++)
    {
        seed <<= 8;
        seed |= (uint8_t)getc(urand);
    }
    srand(seed);
	setAdvert();
}

void SDDRRadio::changeEpoch()
{
    lock_guard<mutex> advert_dhExchangeLock(advert_dhExchangeMutex_);

    // Generate a new secret for this epoch's DH exchanges
    advert_dhExchange_.generateSecret();
    setAdvert();
}

/* 
 * DISCOVERY FUNCTIONS 
 * */

void SDDRRadio::preDiscovery()
{
    discovered_.clear();
}

long SDDRRadio::processScanResponse(int8_t rssi, std::string advert, long scanTime)
{
    LOG_P(TAG, "Processing scan response with rssi %d, and data %s", rssi, advert.c_str());
    
    //uint64_t scanTime = getTimeMS();
    EbNDevice *device = deviceMap_.get(advert);
    if(device == NULL)
    {
      lock_guard<mutex> setLock(setMutex_);

      device = new EbNDevice(generateDeviceID(), advert);
      deviceMap_.add(advert, device);
      LOG_P("ENCOUNTERS_TEST", "-- Discovered new SDDR device (ID %ld, Advert %s)", 
              device->getID(), device->getAdvert().c_str());
    }

    device->addRSSIMeasurement(scanTime, rssi);
    discovered_.push_back(DiscoverEvent(scanTime, device->getID(), rssi));
    LOG_P(TAG, "-- Discovered device %d", device->getID());
    addRecentDevice(device);
	return sddrStartTimestamp + device->getID(); 
}

std::vector<std::string> SDDRRadio::postDiscoveryGetEncounters()
{
    // discovered_ is set from processScanResult
    // get the encounters from this discovery and store them away
    LOG_P(TAG, "-- discovered_ %d devices", discovered_.size());
    list<pair<DeviceID, uint64_t> > newlyDiscovered;
    set<DeviceID> toHandshake = hystPolicy_.discovered(discovered_, newlyDiscovered);
    hystPolicy_.encountered(handshake(toHandshake));

    list<EncounterEvent> encounters;

    for(auto ndIt = newlyDiscovered.begin(); ndIt != newlyDiscovered.end(); ndIt++)
    {
      EncounterEvent event(EncounterEvent::UnconfirmedStarted, ndIt->second, ndIt->first);
      getDeviceAdvert(event, ndIt->first);
      encounters.push_back(event);
    }

    for(auto discIt = discovered_.begin(); discIt != discovered_.end(); discIt++)
    {
      EncounterEvent event(discIt->time);
      if(getDeviceEvent(event, discIt->id, rssiReportInterval_))
      {
        encounters.push_back(event);
      } 
    }

    list<pair<DeviceID, uint64_t> > expired = hystPolicy_.checkExpired();
    for(auto expIt = expired.begin(); expIt != expired.end(); expIt++)
    {
      EncounterEvent expireEvent = doneWithDevice(expIt->first);
      expireEvent.time = expIt->second;
      encounters.push_back(expireEvent);
    }

    std::vector<std::string> messages;
    for(auto encIt = encounters.begin(); encIt != encounters.end(); encIt++)
    {
        messages.push_back(encounterToMsg(*encIt)); 
    }

    LOG_P(TAG, "-- Sending %d encounters", messages.size());
    return messages;
}

/* 
 * ENCOUNTER / HANDSHAKE FUNCTIONS 
 */
std::string SDDRRadio::encounterToMsg(const EncounterEvent &event)
{
  switch(event.type)
  {
  case EncounterEvent::UnconfirmedStarted:
    LOG_P(TAG, "Type = UnconfirmedStarted");
    break;
  case EncounterEvent::Started:
    LOG_P(TAG, "Type = Started");
    break;
  case EncounterEvent::Updated:
    LOG_P(TAG, "Type = Updated");
    break;
  case EncounterEvent::Ended:
    LOG_P(TAG, "Type = Ended");
    break;
  }
  LOG_P(TAG, "ID = %d", event.id);
  LOG_P(TAG, "Time = %" PRIu64, event.time);
  LOG_P(TAG, "# RSSI Entries = %d", event.rssiEvents.size());

  SDDR::Event_EncounterEvent *encounterEvent = new SDDR::Event_EncounterEvent();
  encounterEvent->set_type((SDDR::Event_EncounterEvent_EventType)event.type);
  encounterEvent->set_time(event.time);
  encounterEvent->set_id(event.id);
  encounterEvent->set_pkid(event.getPKID());
  encounterEvent->set_matchingsetupdated(false);
  encounterEvent->set_address("dummyaddr");

  for(auto it = event.rssiEvents.begin(); it != event.rssiEvents.end(); it++)
  {
    SDDR::Event_EncounterEvent_RSSIEvent *rssiEvent = encounterEvent->add_rssievents();
    rssiEvent->set_time(it->time);
    rssiEvent->set_rssi(it->rssi);
  }

  encounterEvent->add_sharedsecrets(event.advert.c_str(), event.advert.size());

  std::string str;
  SDDR::Event fullEvent;
  fullEvent.set_allocated_encounterevent(encounterEvent);
  fullEvent.SerializeToString(&str);
  return str;
}

std::set<DeviceID> SDDRRadio::handshake(const std::set<DeviceID> &deviceIDs)
{
    set<DeviceID> encountered;

    for(auto it = deviceIDs.begin(); it != deviceIDs.end(); it++)
    {
        EbNDevice *device = deviceMap_.get(*it);
        device->setShakenHands(true);
    }

    // Going through all devices to report 'encountered' devices, meaning
    // the devices we have shaken hands with
    for(auto it = deviceMap_.begin(); it != deviceMap_.end(); it++)
    {
        EbNDevice *device = it->second;
        if(device->hasShakenHands())
        {
          encountered.insert(device->getID());
        }
    }

    LOG_P(TAG, "Ending handshake with %d encounters", encountered.size());
    return encountered;
}

EncounterEvent SDDRRadio::doneWithDevice(DeviceID id)
{
  EbNDevice *device = deviceMap_.get(id);

  EncounterEvent expiredEvent(getTimeMS());
  device->getEncounterInfo(expiredEvent, true);

  deviceMap_.remove(id);
  removeRecentDevice(id);

  LOG_P(TAG, "Done with device %d", id);
  return expiredEvent;
}

/* 
 * ADVERT PROCESSING AND GENERATION FUNCTIONS
 */
void SDDRRadio::setAdvert()
{
  	size_t messageSize = advert_dhExchange_.getPublicSize();
  	vector<uint8_t> message(messageSize, 0);

  	// Adding the full public key (X and Y coordinates) to the message
  	memcpy(message.data(), advert_dhExchange_.getPublic(), advert_dhExchange_.getPublicSize());

  	unsigned char hash[SHA_DIGEST_LENGTH]; // == 20
  	SHA1(message.data(), messageSize, hash);
    
    advert_dhpubkey_ = std::string((const char*) message.data(), messageSize);
    advert_dhkey_ = advert_dhExchange_.getSerializedKey();

	advert_ = std::string((const char*) hash, SHA_DIGEST_LENGTH);
    LOG_P("c_sddr", "Advert is %s: ", advert_.c_str());
}

void SDDRRadio::addRecentDevice(EbNDevice *device)
{
  removeRecentDevice(device->getID());

  recentDevices_.push_front(device);
  idToRecentDevices_.insert(make_pair(device->getID(), recentDevices_.begin()));
  LOG_P(TAG, "Recent device %d", device->getID());
}

void SDDRRadio::removeRecentDevice(DeviceID id)
{
  IDToRecentDeviceMap::iterator it = idToRecentDevices_.find(id);
  if(it != idToRecentDevices_.end())
  {
    recentDevices_.erase(it->second);
    idToRecentDevices_.erase(it);
  }
}

bool SDDRRadio::getDeviceEvent(EncounterEvent &event, DeviceID id, uint64_t rssiReportInterval)
{
  IDToRecentDeviceMap::iterator it = idToRecentDevices_.find(id);
  if(it != idToRecentDevices_.end())
  {
    EbNDevice *device = *it->second;
    if(device->getEncounterInfo(event, rssiReportInterval))
    {
      return true;
    }
  }

  return false;
}

void SDDRRadio::getDeviceAdvert(EncounterEvent &event, DeviceID id)
{
  IDToRecentDeviceMap::iterator it = idToRecentDevices_.find(id);
  if(it != idToRecentDevices_.end())
  {
    EbNDevice *device = *it->second;
    device->getAdvert(event);
  }
}

std::string SDDRRadio::computeSecretKey(std::string myDHKey, std::string SHA1DHKey, std::string otherDHKey) {
  	size_t messageSize = advert_dhExchange_.getPublicSize();
  	vector<uint8_t> message(messageSize, 0);

  	memcpy(message.data(), otherDHKey.c_str(), messageSize);

    if (SHA1DHKey.length() != 0) {
        unsigned char hash[SHA_DIGEST_LENGTH]; // == 20
        SHA1(message.data(), messageSize, hash);
        std::string hashStr = std::string((const char*) hash, SHA_DIGEST_LENGTH);
        if (hashStr.compare(SHA1DHKey) != 0) {
            LOG_P("c_SDDR", "UhOh.... hash is %s, otherDHKey is %s", hashStr.c_str(), SHA1DHKey.c_str());
            return std::string("");
        }
    }
    char dest[keySize_ / 8];
    ECDH::computeSharedSecret(
            (char*)dest,
            myDHKey,
            (const uint8_t*)otherDHKey.substr(0, messageSize).c_str(),
            keySize_
    );
    LOG_P("c_sddr", "shared secret!!!! %s ", std::string(dest, keySize_/8).c_str());
    return std::string(dest, keySize_/8);
}

void SDDRRadio::changeMessagingDHKey() 
{
    new_message_dhExchange_.generateSecret();	
    
    size_t messageSize = new_message_dhExchange_.getPublicSize();
  	vector<uint8_t> message(messageSize, 0);

  	// Adding the full public key (X and Y coordinates) to the message
  	memcpy(message.data(), new_message_dhExchange_.getPublic(), new_message_dhExchange_.getPublicSize());
    new_message_dhpubkey_ = std::string((const char*) message.data(), messageSize);
    new_message_dhkey_ = new_message_dhExchange_.getSerializedKey();
}
