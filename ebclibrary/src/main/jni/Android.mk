MY_PATH := $(call my-dir)
include $(call all-subdir-makefiles) # this changes my-dir...
include $(CLEAR_VARS)
LOCAL_PATH := $(MY_PATH)

PROJECT_ROOT := $(LOCAL_PATH)
include $(CLEAR_VARS)

LOCAL_MODULE := c_SDDRRadio
LOCAL_CFLAGS += -DLOG_W_ENABLED -DLOG_D_ENABLED -DLOG_P_ENABLED

LOCAL_SRC_FILES := $(COMMON_SOURCE_FILES)
LOCAL_SRC_FILES += $(EXECUTABLE_SOURCE_FILES)

LOCAL_C_INCLUDES += $(PROJECT_ROOT)/include
LOCAL_C_INCLUDES += $(PROJECT_ROOT)/third-party/jerasure
LOCAL_C_INCLUDES += $(PROJECT_ROOT)/third-party/protobuf/src
LOCAL_C_INCLUDES += $(PROJECT_ROOT)/third-party/openssl/include
LOCAL_LDLIBS += -lc -ldl -llog -landroid
LOCAL_LDLIBS += $(PROJECT_ROOT)/../jniLibs/$(TARGET_ARCH_ABI)/libssl.a
LOCAL_LDLIBS += $(PROJECT_ROOT)/../jniLibs/$(TARGET_ARCH_ABI)/libcrypto.a
LOCAL_SHARED_LIBRARIES += libprotobuf
include $(BUILD_SHARED_LIBRARY)
