#include "Logger.h"
#include <openssl/ssl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string>
#include <string.h>

static void init_openssl()
{
	OpenSSL_add_ssl_algorithms();
    OpenSSL_add_all_ciphers();
	SSL_load_error_strings();
}

static void cleanup_openssl()
{
    EVP_cleanup();
}

static int create_socket_client(const char *ip, uint32_t port) 
{
	int sockfd;
	struct sockaddr_in dest_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) {
		LOG_D("SSLClient","socket");
        return -1;
    }

	dest_addr.sin_family=AF_INET;
	dest_addr.sin_port=htons(port);
	dest_addr.sin_addr.s_addr = (long)inet_addr(ip);
	memset(&(dest_addr.sin_zero), '\0', 8);

	LOG_D("SSLClient","Connecting...\n");
	if (connect(sockfd, (struct sockaddr *) &dest_addr, sizeof(struct sockaddr)) == -1) {
		LOG_D("SSLClient","Cannot connect");
        return 0;
	}

	return sockfd;
}

static SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = SSLv23_client_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
        LOG_D("SSLClient","Unable to create SSL context");
        return NULL;
    }
    return ctx;
}

int call_client_with_message(const char* send_buf)
{
	SSL *ssl;
	int sock;
    SSL_CTX *ctx;
    const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
    const char *serv_ip = "139.19.168.145";
    uint32_t serv_port = 4433;

    LOG_D("SSLClient","OPENSSL Version = %s\n", SSLeay_version(SSLEAY_VERSION));
    init_openssl();
    ctx = create_context();
    if (ctx == NULL) {
        return -1;
    }
    SSL_CTX_set_options(ctx, flags);
    sock = create_socket_client(serv_ip, serv_port);
    if (!sock) {
        return -1;
    }
    LOG_D("SSLClient","Connects to TLS server success\n");

    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, sock);
	if (SSL_connect(ssl) <= 0) {
        LOG_D("SSLClient","SSL_connect error\n");
        return -1;
	}
 
    // Send buffer to TLS server 
    LOG_D("SSLClient", "Sending buffer %s", send_buf);
	SSL_write(ssl, send_buf, strlen(send_buf)+1);

    // Get return value 
    char read_buf[4];
    SSL_read(ssl, &read_buf, sizeof(read_buf));
    int ret = atoi(read_buf);
    
    LOG_D("SSLClient","Close SSL/TLS client\n");
	SSL_free(ssl);
    SSL_CTX_free(ctx);
    shutdown(sock,2);
    cleanup_openssl();
    
    LOG_D("SSLClient","Got response: %d\n", ret);
    return ret;
}
