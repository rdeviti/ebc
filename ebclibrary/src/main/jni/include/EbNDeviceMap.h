#ifndef EBNDEVICEMAP_H
#define EBNDEVICEMAP_H

#include <type_traits>
#include <unordered_map>

#include "EbNDevice.h"

// Maps adverts and device IDs to particular devices
template<typename TDevice>
class EbNDeviceMap
{
  static_assert(std::is_base_of<EbNDevice, TDevice>::value, "TDevice must be derived from EbNDevice");

private:
  typedef typename std::unordered_map<std::string, TDevice *> AdvertToDeviceMap;
  typedef typename AdvertToDeviceMap::iterator AdvertToDeviceMap_Iter;
  typedef typename AdvertToDeviceMap::const_iterator AdvertToDeviceMap_ConstIter;
  typedef typename AdvertToDeviceMap::const_local_iterator AdvertToDeviceMap_ConstLocalIter;

  typedef typename std::unordered_map<DeviceID, TDevice *> DeviceIDToDeviceMap;
  typedef typename DeviceIDToDeviceMap::iterator DeviceIDToDeviceMap_Iter;
  typedef typename DeviceIDToDeviceMap::const_iterator DeviceIDToDeviceMap_ConstIter;

  AdvertToDeviceMap advertToDevice_;
  DeviceIDToDeviceMap idToDevice_;

public:
  EbNDeviceMap();
  ~EbNDeviceMap();

  TDevice* get(DeviceID id);
  TDevice* get(const std::string& advert);
  void add(const std::string& advert, TDevice *device);
  bool remove(const std::string& advert);
  bool remove(DeviceID id);

  void clear();

  DeviceIDToDeviceMap_Iter begin();
  DeviceIDToDeviceMap_Iter end();
};

template<typename TDevice>
EbNDeviceMap<TDevice>::EbNDeviceMap()
{
}

template<typename TDevice>
EbNDeviceMap<TDevice>::~EbNDeviceMap()
{
  for(DeviceIDToDeviceMap_Iter it = idToDevice_.begin(); it != idToDevice_.end(); it++)
  {
    delete it->second;
  }
}


template<typename TDevice>
TDevice* EbNDeviceMap<TDevice>::get(DeviceID id)
{
  DeviceIDToDeviceMap_Iter it = idToDevice_.find(id);
  if(it != idToDevice_.end())
  {
    return it->second;
  }

  return NULL;
}

template<typename TDevice>
TDevice* EbNDeviceMap<TDevice>::get(const std::string &advert)
{
 	TDevice *device = NULL;
    AdvertToDeviceMap_Iter it = advertToDevice_.find(advert);
    if (it != advertToDevice_.end()) { 
        return it->second;
    } else {
	    LOG_P("Device", "Didn't find advert %s in %d entries", 
                advert.c_str(), advertToDevice_.size());
        return NULL;
    }
}

template<typename TDevice>
void EbNDeviceMap<TDevice>::add(const std::string &advert, TDevice *device)
{
  advertToDevice_.insert(std::make_pair(advert, device));
  idToDevice_.insert(std::make_pair(device->getID(), device));
  LOG_P("ADDRESS", "Inserting addr %s vs. %s id %d", 
          advert.c_str(), device->getAdvert().c_str(), device->getID());
}

template<typename TDevice>
bool EbNDeviceMap<TDevice>::remove(const std::string &advert)
{
  AdvertToDeviceMap_Iter it = advertToDevice_.find(advert);
  if(it != advertToDevice_.end())
  {
    TDevice *device = it->second;

    idToDevice_.erase(idToDevice_.find(device->getID()));
    advertToDevice_.erase(it);
    delete device;

    return true;
  }

  return false;
}

template<typename TDevice>
bool EbNDeviceMap<TDevice>::remove(DeviceID id)
{
  DeviceIDToDeviceMap_Iter it = idToDevice_.find(id);
  if(it != idToDevice_.end())
  {
    TDevice *device = it->second;

    advertToDevice_.erase(advertToDevice_.find(device->getAdvert()));
    idToDevice_.erase(it);
    delete device;

    return true;
  }

  return false;
}

template<typename TDevice>
void EbNDeviceMap<TDevice>::clear()
{
  advertToDevice_.clear();
  idToDevice_.clear();
}

template<typename TDevice>
typename EbNDeviceMap<TDevice>::DeviceIDToDeviceMap_Iter EbNDeviceMap<TDevice>::begin()
{
  return idToDevice_.begin();
}

template<typename TDevice>
typename EbNDeviceMap<TDevice>::DeviceIDToDeviceMap_Iter EbNDeviceMap<TDevice>::end()
{
  return idToDevice_.end();
}

#endif // EBNDEVICEMAP_H
