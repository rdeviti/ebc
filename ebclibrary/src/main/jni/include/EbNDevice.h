#ifndef EBNDEVICE_H
#define EBNDEVICE_H

#include <algorithm>
#include <cstdint>
#include <list>
#include <mutex>
#include <string>
#include <vector>

#include "BloomFilter.h"
#include "EbNEvents.h"
#include "LinkValue.h"
#include "SharedArray.h"
#include "ECDH.h"
#include "SegmentedBloomFilter.h"
#include "RSErasureDecoder.h"

class EbNDevice
{
  friend class SDDRRadio;

private:
  DeviceID id_;
  std::string advert_;
  std::list<RSSIEvent> rssiToReport_;
  uint64_t lastReportTime_;
  bool confirmed_;
  bool shakenHands_;

public:
  EbNDevice(DeviceID id, const std::string &advert);
  ~EbNDevice();

  DeviceID getID() const;

  void setShakenHands(bool value);
  bool hasShakenHands() const;

  void addRSSIMeasurement(uint64_t time, uint8_t rssi);

  bool getEncounterInfo(EncounterEvent &dest, bool expired = false, bool retroactive = true);
  bool getEncounterInfo(EncounterEvent &dest, uint64_t rssiReportingInterval, bool expired = false, bool retroactive = true);
  void getAdvert(EncounterEvent &dest);
  std::string getAdvert();
};

inline DeviceID EbNDevice::getID() const
{
  return id_;
}

inline std::string EbNDevice::getAdvert()
{
  return advert_;
}

inline void EbNDevice::setShakenHands(bool value)
{
  shakenHands_ = value;
}

inline bool EbNDevice::hasShakenHands() const
{
  return shakenHands_;
}

inline void EbNDevice::addRSSIMeasurement(uint64_t time, uint8_t rssi)
{
  rssiToReport_.push_back(RSSIEvent(time, rssi));
}

#endif // EBNDEVICE_H
