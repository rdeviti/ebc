package org.mpisws.encounters;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.microsoft.embeddedsocial.autorest.models.Reason;

import net.sqlcipher.database.SQLiteDatabase;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.database.DBEncounterInfo;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.encounters.encounterformation.SDDR_Native;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.encounters.clients.CommunicationClient;
import org.mpisws.encounters.lib.Options;
import org.mpisws.encounters.clients.SDDRClient;
import org.mpisws.encounters.clients.UserAccountClient;
import org.mpisws.encounters.encounterhistory.models.MLocation;
import org.mpisws.encounters.lib.Preferences;
import org.mpisws.encounters.lib.time.TimeInterval;
import org.mpisws.messaging.ReceivedMessageWrapper;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.ForwardingConstraint;
import org.mpisws.messaging.constraints.TrajectoryConstraint;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

/**
 * Created by tslilyai on 2/28/18.
 */

/**
 * EncounterBasedCommunication provides access to different clients for handling EbC accounts (UserAccountClient),
 * encounter formation and confirmation (SDDRClient), and communication over encounters (CommunicationClient).
 *
 * Before using the EncounterBasedCommunication instance, initialize the instance with your application context
 * and configuration file.
 *
 **/
public class EncounterBasedCommunication {
    public static final long CHANGE_EPOCH_TIME = 2*60000;
    public static final long SCAN_BATCH_INTERVAL = (long) (2*60000);
    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_ACCESS_FINE_LOCATION = 2;
    private static final String TAG = EncounterBasedCommunication.class.getSimpleName();

    private UserAccountClient userAccountClient;
    private SDDRClient sddrClient;
    private CommunicationClient communicationClient;
    public static EncounterBasedCommunication instance = new EncounterBasedCommunication();

    private EncounterBasedCommunication(){}
    public static EncounterBasedCommunication getInstance() {
        return instance;
    }

    /**
     * Indicates whether the application's ebc authentication key is valid and the current user is signed in.
     * @return the sign-in status of the current ebc user account
     **/
    public static boolean isSignedIn() {
        return (ESClient.getInstance().isSignedIn() && Preferences.getInstance().getEbCKey() != null);
    }

     /**
     * The UserAccountClient interface exposes functions to sign in an EbC user, sign out an EbC user,
     * and delete a signed-in user's account.
     **/
    public interface IUserAccountClient {
        /**
        * Log in an EbC user with an acquired google token.
        * Creates the user account if it does not exist.
        *
        * @param googletoken OAuth token acquired from Google
        * @return Signed-in status of the EbC user
        */
        boolean loginAccountWithGoogle(String googletoken);

        /**
        * Logs out the current EbC user (does nothing if
        * no user is currently signed in)
        */
        void signOut();

        /**
        * Deletes the current EbC user account (does nothing
        * if no user is currently signed in)
        */
        void deleteAccount();
    }

    public interface ISDDRClient {
        /**
         * Begin the encounter formation service, which emits BLE adverts and occasional BLE scans to
         * detect and form encounters with nearby EbC devices
         */
        void startFormingEncounters();

        /**
         * Stop the encounter formation service
         */
        void stopFormingEncounters();

        /**
         * Activate encounter confirmation (a DH-key-pair exchange and shared secret formation). This
         * takes place every epoch (currently 15 min) using a network key-value store.
         */
        void confirmAndCommunicateUsingNetwork();

        /**
         * Activate encounter confirmation (a DH-key-pair exchange and shared secret formation). This
         * takes place every epoch (currently 15 min) over BLE. However, only encountered devices
         * still in BLE range, and which also wish to confirm over BLE, can be confirmed.
         *
         */
        void confirmAndCommunicateUsingBLE();

        /**
         * Turn off encounter confirmation. All discovered devices cannot be contacted unless their
         * encounters have been confirmed.
         */
        void disableConfirmation();

        Pair<List<MLocation>, TimeInterval> getEncounterInfoFromEID(String encounterID);

        /**
         * Return all encounters satisfying the specified EncounterQueryConstraint
         * @param constraint The constraint (including time, place, and/or trajectory)
         * @return A list of DBEncounterInfo objects with information on the encounters satisfying
         * the constraint
         */
        List<DBEncounterInfo> getEncountersWithConstraint(EncounterQueryConstraint constraint);

        /**
         * Update the cloud agent's database since the last time the database
         * was updated.
         */
        void updateDatabaseOnAgent();
    }

    /**
     * The Communication Client Interface allows an application to perform all communication functions,
     * such as sending messages to various encounters, reading (unread) messages, and reporting or
     * blocking unwanted communication.
     */
    public interface ICommunicationClient {
        /**
         * Block all encounters satisfying the specified constraint. No communication to/from
         * these encounters will be possible afterward.
         * @param constraint The time/space/trajectory constraint specifying which encounters channels
         *                   to delete
         */
        void blockEncounters(EncounterQueryConstraint constraint);

        /**
         * Reply to the specified received message, in the process creating a private messaging channel
         * with the sender if one does not exist
         */
        void replyToMessage(String msgToSend, ReceivedMessageWrapper receivedMessageWrapper);

        /**
         * Send the specified EbC messages (where each message contains a EbCMsgContent object along with a
         * specified constraint indicating which encounters should receive the message)
         */
        void sendDirectMessageToEncounters(String msg, EncounterQueryConstraint encounterQueryConstraint, boolean enableReplies);

        void sendMultiHopMessageToEncounters(String msg, EncounterQueryConstraint encounterQueryConstraint, ForwardingConstraint forwardingConstraint, boolean enableReplies);

        /**
         * Get all the unread messages (i.e., all messages that the application has not yet
         * requested be read)
         * @param callback Callback to handle a list of unread messages' contents
         */
        void getUnreadMsgs(ESClient.GetReceivedMessagesCallback callback);

        void getConversationOnPrivateChannel(String encounterID, ESClient.GetReceivedMessagesCallback callback);

        /**
         * Report a particular message as unwanted communication
         * @param cursor The unique handle of the message to report
         * @param reason The reason the message is being reported
         */
        void reportMsg(String cursor, Reason reason);
    }

    /**
     * Initialize the EncounterBasedCommunication agent for a specific application's ebc configuration.
     * Must be called before using any EbC Clients.
     *
     * @param context The calling application context
     */
    public void initialize(Context context, int configResId) {
        SDDR_Native.c_mallocRadio();
        Preferences preferences = new Preferences(context);
        GlobalObjectRegistry.addObject(preferences);

        // try to get ES Credentials
        Log.d(TAG, "UserHandle, Auth: " + preferences.getUserHandle() + ", " + preferences.getAuthorizationToken());
        ESClient.getInstance().setESCredentials(preferences.getUserHandle(), preferences.getAuthorizationToken());

        // get ebckey for ES
        InputStream is = context.getResources().openRawResource(configResId);
        Reader reader = new InputStreamReader(is);
        Options options = new Gson().fromJson(reader, Options.class);
        try {options.verify();}
        catch (IllegalArgumentException e) {
            Log.d(TAG, e.getMessage());
        }
        preferences.setEbCKey(options.getEbCKey());
        ESClient.getInstance().setESAPIKey(options.getEbCKey());

        userAccountClient = new UserAccountClient();
        sddrClient = new SDDRClient(context);
        communicationClient = new CommunicationClient(context);
        GlobalObjectRegistry.addObject(this);

        //FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        //GlobalObjectRegistry.addObject(mFirebaseAnalytics);
        SQLiteDatabase.loadLibs(context);
    }

    /**
     * Get a UserAccountClient object to perform ebc user account actions.
     * @return A UserAccountClient object
     */
    public IUserAccountClient getUserAccountClient() {
        return userAccountClient;
    }

    /**
     * Get an SDDRClient object to perform ebc device encounter formation/confirmation actions.
     * An EbC user must be signed in to access this object.
     *
     * @return An SDDRClient object
     * @throws SecurityException
     */
    public ISDDRClient getSDDRClient() throws SecurityException {
        if (!isSignedIn()) throw new SecurityException("not signed in");
        return sddrClient;
    }

    /**
     * Get a CommunicationClient object to perform ebc encounter communication actions.
     * An EbC user must be signed in to access this object.
     *
     * @return A CommunicationClient object
     * @throws SecurityException
     */
    public ICommunicationClient getCommunicationClient() throws SecurityException{
        if (!isSignedIn()) throw new SecurityException("not signed in");
        return communicationClient;
    }
}
