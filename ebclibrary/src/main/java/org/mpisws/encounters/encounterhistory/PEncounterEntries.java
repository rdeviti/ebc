package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;

import org.mpisws.database.databaseSchema.DBEncounterEntries;

public class PEncounterEntries extends DBEncounterEntries {
    private static PEncounterEntries instance = new PEncounterEntries();
    private PEncounterEntries() {
        super();
    };
    public static PEncounterEntries getInstance() {return instance;}

    public static String extractMyTopicHandle(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(Columns.myTopicHandle));
    }

    public static String extractOtherTopicHandle(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(Columns.otherTopicHandle));
    }

    public static long extractEncounterPKID(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(Columns.encounterPKID));
    }

    public static byte[] extractSharedSecret(final Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.sharedSecret));
    }
    
    public static byte[] extractEncounterID(final Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.encounterID));
    }

    public static long extractStartTime(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(Columns.timestampStart));
    }

    public static long extractEndTime(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(Columns.timestampEnd));
    }

    public static byte[] extractMyNonce(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.myNonce));
    }
    public static byte[] extractReceivedNonce(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.receivedNonce));
    }
    public static byte[] extractMyDHPrivKey(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.myDHPrivKey));
    }
    public static byte[] extractMyDHPubKey(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.myDHPubKey));
    }
    public static boolean extractPostedNonceTopic(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(Columns.postedNonceTopic)) == 1;
    }
    public static boolean extractShouldPostForLinking(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(Columns.shouldPostNonceToLink)) == 1;
    }

    public static String extractUserHandle(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(Columns.userHandle));
    }
}
