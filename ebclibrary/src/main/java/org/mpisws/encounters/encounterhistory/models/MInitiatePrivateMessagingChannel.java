package org.mpisws.encounters.encounterhistory.models;

import org.mpisws.encounters.dbplatform.AbstractMemoryObject;

public class MInitiatePrivateMessagingChannel extends AbstractMemoryObject {

    private long pkid;
    private String pubKeyTopicHandle;
    private byte[] myDHPrivKey;
    private byte[] myDHPubKey;

    public long getPkid() {
        return pkid;
    }

    public String getPubKeyTopicHandle() {
        return pubKeyTopicHandle;
    }

    public byte[] getMyDHPrivKey() {
        return myDHPrivKey;
    }

    public byte[] getMyDHPubKey() {
        return myDHPubKey;
    }

    public MInitiatePrivateMessagingChannel(long pkid, byte[] myDHPrivKey, byte[]myDHPubKey, String pubKeyTopicHandle) {
        super(pkid);
        this.pkid = pkid;
        this.myDHPrivKey = myDHPrivKey;
        this.myDHPubKey = myDHPubKey;
        this.pubKeyTopicHandle = pubKeyTopicHandle;
    }
}
