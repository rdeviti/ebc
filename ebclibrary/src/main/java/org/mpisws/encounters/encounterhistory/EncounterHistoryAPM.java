package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;

import org.mpisws.database.DBModel;
import org.mpisws.helpers.Utils;
import org.mpisws.encounters.dbplatform.AggregatePersistenceModel;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class EncounterHistoryAPM extends AggregatePersistenceModel {


    protected static AggregatePersistenceModel getEncounterAggregatePersistenceModel() {
        return EncounterHistoryAPM.getInstance();
    }

    private static final EncounterHistoryAPM instance = new EncounterHistoryAPM();
    public static final PEncounterEntries encounterEntries = PEncounterEntries.getInstance();
    public static final PLocation locations = PLocation.getInstance();
    public static final PPrivateMessagingChannels messageChannels = PPrivateMessagingChannels.getInstance();
    public static final PInitiatePrivateMessagingChannels initiateMessagingChannels = PInitiatePrivateMessagingChannels.getInstance();
    public static final List<DBModel> models = new LinkedList<>();

    static {
        models.add(encounterEntries);
        models.add(locations);
        models.add(messageChannels);
        models.add(initiateMessagingChannels);
    }

    private EncounterHistoryAPM() {}

    public String getContentProviderAuthority() {
        return "org.mpisws.encounters.encounterhistory";
    }

    public List<DBModel> getPersistenceModels() {
        return Collections.unmodifiableList(models);
    }

    public static EncounterHistoryAPM getInstance() {
        return instance;
    }

    public static Uri getContentURI(DBModel model) {
        return Uri.parse("content://" + getEncounterAggregatePersistenceModel().getContentProviderAuthority() + "/" + model.getTableName());
    }

    public static void drop(final SQLiteDatabase db, DBModel model) {
        db.execSQL("DROP TABLE " + model.getTableName());
    }

    public static void createTable(final SQLiteDatabase db, DBModel model) {
        final StringBuilder sb = new StringBuilder();
        Log.v("PersistenceModel", "Creating table " + model.getTableName());
        sb.append("CREATE TABLE IF NOT EXISTS ").append(model.getTableName()).append(" (");
        sb.append(Utils.collectionToStringV2(model.getColumns(), ","));
        sb.append(model.getUnique());
        sb.append(")");
        db.execSQL(sb.toString());
    }

    public static void clearTable(final SQLiteDatabase db, DBModel model) {
        db.delete(model.getTableName(), null, null);
    }

    public static long extractPKID(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(DBModel.Columns.pkid));
    }
}
