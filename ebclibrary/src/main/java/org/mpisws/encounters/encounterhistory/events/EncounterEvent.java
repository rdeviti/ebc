package org.mpisws.encounters.encounterhistory.events;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.mpisws.encounters.lib.Preferences;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;
import org.mpisws.encounters.encounterhistory.PLocation;
import org.mpisws.helpers.ThreadPoolManager;

import java.io.Serializable;
import java.util.List;

public abstract class EncounterEvent implements Serializable {
    private static final String TAG = EncounterEvent.class.getSimpleName();
    private static final long serialVersionUID = 4915344112998741643L;
    protected final long pkid;
    protected final Long startTime;
    protected final Long lastTimeSeen;
    protected final Long endTime;
    protected final List<RSSIEntry> newRSSIEntries;
    protected final Identifier myNonce;
    protected final Identifier myDHPubKey;
    protected final Identifier myDHKey;
    protected final Identifier receivedNonce;
    protected Identifier secret;

    public EncounterEvent(long pkid, Long startTime, Long lastTimeSeen, Long endTime, List<RSSIEntry> newRSSIEntries,
                          Identifier myNonce, Identifier myDHPubKey, Identifier myDHKey, Identifier receivedNonce, Identifier secret) {
        this.pkid = pkid;
        this.startTime = startTime;
        this.lastTimeSeen = lastTimeSeen;
        this.endTime = endTime;
        this.newRSSIEntries = newRSSIEntries;
        this.myNonce = myNonce;
        this.myDHPubKey = myDHPubKey;
        this.myDHKey = myDHKey;
        this.receivedNonce = receivedNonce;
        this.secret = secret;
    }

    public abstract void broadcast(final Context context);

    public abstract void persistIntoDatabase(Context context);

    public static void insertLocationForEncounters(final Context context) {
        Log.v(TAG, "Inserting location");
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        double lat = location.getLatitude();
                        double longi = location.getLongitude();
                        Log.v(TAG, "Updated location with lat " + lat + " and long " + longi);
                        ThreadPoolManager.getInstance().runTask(()-> {
                            final ContentValues values = new ContentValues();
                            values.put(PLocation.Columns.userHandle, Preferences.getInstance().getUserHandle());
                            values.put(PLocation.Columns.latitude, lat);
                            values.put(PLocation.Columns.longitude, longi);
                            values.put(PLocation.Columns.updatedTimestamp, System.currentTimeMillis());
                            context.getContentResolver().insert(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.locations), values);
                        });

                        // FIREBASE: log location event
                        Bundle params = new Bundle();
                        params.putDouble("latitude", lat);
                        params.putDouble("longitude", longi);
                        //GlobalObjectRegistry.getObject(FirebaseAnalytics.class).logEvent("location", params);
                    } else {
                        Log.v(TAG, "Location null");
                    }
                });
    }
}
