package org.mpisws.encounters.encounterhistory.models;

import org.mpisws.encounters.dbplatform.AbstractMemoryObject;

public class MPrivateMessagingChannel extends AbstractMemoryObject {
    long pkid;
    private byte[] channelName;
    private byte[] sharedSecret;
    private String myTopicHandle;
    private String otherTopicHandle;

    public byte[] getChannelName() {
        return channelName;
    }

    public byte[] getSharedSecret() {
        return sharedSecret;
    }

    public String getMyTopicHandle() {
        return myTopicHandle;
    }

    public String getOtherTopicHandle() {
        return otherTopicHandle;
    }

    public MPrivateMessagingChannel(long pkid, byte[] channelName, byte[] sharedSecret, String myTopicHandle, String otherTopicHandle) {
        super(pkid);
        this.pkid = pkid;
        this.channelName = channelName;
        this.sharedSecret = sharedSecret;
        this.myTopicHandle = myTopicHandle;
        this.otherTopicHandle = otherTopicHandle;
    }
}
