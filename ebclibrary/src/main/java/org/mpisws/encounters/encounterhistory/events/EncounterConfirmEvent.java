package org.mpisws.encounters.encounterhistory.events;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;
import org.mpisws.encounters.encounterhistory.models.MEncounterEntry;

public class EncounterConfirmEvent extends EncounterEvent {
    private static final String TAG = EncounterConfirmEvent.class.getSimpleName();
    private final Identifier myNonce;
    private final Identifier receivedNonce;
    private final Identifier sharedsecret;

    public EncounterConfirmEvent(Identifier myNonce, Identifier receivedNonce, Identifier sharedSecret) {
        super(-1, null, null, null, null, null, null, null, null, null);
        this.receivedNonce = receivedNonce;
        this.sharedsecret = sharedSecret;
        this.myNonce = myNonce;
    }

    @Override
    public void broadcast(Context context) {
        Log.v(TAG, "Broadcasting confirmed encounter");
        final Intent i = new Intent(context, EncounterEventReceiver.class);
        i.putExtra("encounterEvent", this);
        context.sendBroadcast(i);
    }

    @Override
    public void persistIntoDatabase(Context context) {
        if (sharedsecret.getBytes() != null) {
            Identifier eid = Coder.convertSharedSecretToID(secret);
            new EncounterEntriesBridge(context).addItem(
                    new MEncounterEntry(
                            System.currentTimeMillis(), "userHandle", -1,
                            secret, eid,
                            System.currentTimeMillis(), -1,
                            myNonce, null,null, receivedNonce,
                            true,true,
                            null, null));
        }
    }
}