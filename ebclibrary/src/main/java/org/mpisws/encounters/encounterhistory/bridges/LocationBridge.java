package org.mpisws.encounters.encounterhistory.bridges;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.mpisws.database.DBModel;
import org.mpisws.encounters.dbplatform.AbstractEncountersBridge;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;
import org.mpisws.encounters.encounterhistory.models.MLocation;
import org.mpisws.encounters.encounterhistory.PLocation;

import java.util.ArrayList;
import java.util.List;

import static org.mpisws.encounters.encounterhistory.EncounterHistoryAPM.extractPKID;

public class LocationBridge extends AbstractEncountersBridge<MLocation> {
    private static final String TAG = LocationBridge.class.getSimpleName();
    private final Context context;

    public List<MLocation> getLocationsSinceTime(long locationsDBTimestamp) {
        final String[] selectionArgs = new String[] { String.valueOf(locationsDBTimestamp) };
        String whereClause = PLocation.Columns.updatedTimestamp + " > ?";
        String sortOrder = PLocation.Columns.updatedTimestamp + " ASC";
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.locations), null, whereClause, selectionArgs, sortOrder);
        List<MLocation> locs = new ArrayList();
        while (cursor.moveToNext()) {
            locs.add(cursorToItem(cursor));
        }
        cursor.close();
        return locs;
    }

    public List<MLocation> getLocationsBetween(long starttime, long endttime) {
        final String[] selectionArgs = new String[] { String.valueOf(starttime), String.valueOf(endttime) };
        String whereClause = PLocation.Columns.updatedTimestamp + " > ? AND " + PLocation.Columns.updatedTimestamp + " < ?";
        String sortOrder = PLocation.Columns.updatedTimestamp + " ASC";
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.locations), null, whereClause, selectionArgs, sortOrder);
        List<MLocation> locs = new ArrayList();
        while (cursor.moveToNext()) {
            locs.add(cursorToItem(cursor));
        }
        cursor.close();
        return locs;
    }

    public class LocationStamp {
        double latitude;
        double longitude;
        long timestamp;
        LocationStamp(double latitude, double longitude, long timestamp) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.timestamp = timestamp;
        }
    }

    public LocationBridge(final Context context) {
        super(context);
        this.context = context;
    }
    
    public LocationStamp getLocationByEncounterPKID(final long pkid) {
        Log.d(TAG, "Getting location from encounter PKID");
        LocationStamp result = null;
        final Uri uri = ContentUris.withAppendedId(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.locations), pkid);
        final Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToNext()) {
            result = new LocationStamp(
                        EncounterHistoryAPM.locations.extractLatitude(cursor),
                        EncounterHistoryAPM.locations.extractLongitude(cursor),
                        EncounterHistoryAPM.locations.extractTimestamp(cursor));
        }
        cursor.close();
        return result;
    }

    @Override
    protected DBModel getPersistenceModel() {
        return EncounterHistoryAPM.locations;
    }

    @Override
    protected ContentValues itemToContentValues(MLocation item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MLocation cursorToItem(Cursor cursor) {
        final long pkID = extractPKID(cursor);
        return new MLocation(
                pkID,
                PLocation.extractLatitude(cursor),
                PLocation.extractLongitude(cursor),
                PLocation.extractTimestamp(cursor));
    }
}
