package org.mpisws.encounters.encounterhistory.models;

import org.mpisws.encounters.dbplatform.AbstractMemoryObject;

import java.io.Serializable;

import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.lib.time.TimeInterval;

import static org.mpisws.encounters.clients.CloudAgentHelpers.COMPONENTDELIMITER;

public class MEncounterEntry extends AbstractMemoryObject implements Serializable {
    private static final String TAG = MEncounterEntry.class.getSimpleName();
    private static final long serialVersionUID = 7973198346712274576L;
    private final String userHandle ;
    private final long encounterPKID;
    private final Identifier secret;
    private final Identifier encounterID;
    private final boolean postedNonceTopic;
    private final Identifier myNonce;
    private final Identifier myDHPrivKey;
    private final Identifier myDHPubKey;
    private final Identifier receivedNonce;
    private final String myTopicHandle;
    private final long endTime;
    private final long startTime;
    private final boolean shouldPostForLinking;
    private String otherTopicHandle;

    public MEncounterEntry(final long pkid, final String userHandle, final long encounterPKID, final Identifier secret, final Identifier encounterID,
                           final long startTime, final long endTime,
                           Identifier myNonce, Identifier myDHPrivKey, Identifier myDHPubKey, Identifier receivedNonce,
                           boolean postedNonceTopic, boolean shouldPostForLinking,
                           String myTopicHandle, String otherTopicHandle) {
        super(pkid);
        this.userHandle = userHandle;
        this.encounterPKID = encounterPKID;
        this.secret = secret;
        this.encounterID = encounterID;
        this.startTime = startTime;
        this.endTime = endTime;
        this.myNonce = myNonce;
        this.myDHPrivKey = myDHPrivKey;
        this.myDHPubKey = myDHPubKey;
        this.receivedNonce = receivedNonce;
        this.postedNonceTopic = postedNonceTopic;
        this.shouldPostForLinking = shouldPostForLinking;
        this.myTopicHandle = myTopicHandle;
        this.otherTopicHandle = otherTopicHandle;
    }


    public long getEncounterPKID() {
        return encounterPKID;
    }

    public Identifier getSecret() {
        return secret;
    }

    public Identifier getEncounterID() {
        return encounterID;
    }

    public TimeInterval getTimeInterval() {
        return new TimeInterval(startTime, endTime);
    }

    public Identifier getMyNonce() {
        return myNonce;
    }
    public Identifier getReceivedNonce() {
        return receivedNonce;
    }
    public Identifier getMyDHPrivKey() {
        return myDHPrivKey;
    }
    public Identifier getMyDHPubKey() {
        return myDHPubKey;
    }
    public boolean getPostedNonceTopic() {
        return postedNonceTopic;
    }
    public boolean getShouldPostNonceForLinking() {
        return shouldPostForLinking;
    }
    public String getMyTopicHandle() { return myTopicHandle; }
    public String getOtherTopicHandle() { return otherTopicHandle; }
    public void setOtherTopicHandle(String otherTopicHandle) {
        this.otherTopicHandle = otherTopicHandle;
    }
    public String getUserHandle() {
        return this.userHandle;
    }

    public String valsToString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(startTime)).append(COMPONENTDELIMITER);
        sb.append(String.valueOf(endTime)).append(COMPONENTDELIMITER);
        sb.append(secret.toString()).append(COMPONENTDELIMITER);
        sb.append(myTopicHandle);
        return sb.toString();
    }}
