package org.mpisws.encounters.encounterhistory.bridges;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.mpisws.database.DBModel;
import org.mpisws.encounters.dbplatform.AbstractEncountersBridge;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;
import org.mpisws.encounters.encounterhistory.PPrivateMessagingChannels;
import org.mpisws.encounters.encounterhistory.models.MPrivateMessagingChannel;
import org.mpisws.helpers.Identifier;

public class MessagingChannelsBridge extends AbstractEncountersBridge<MPrivateMessagingChannel> {
    private static final String TAG = MessagingChannelsBridge.class.getSimpleName();
    private final ContentResolver contentResolver;
    private final Context context;

    public MessagingChannelsBridge(final Context context) {
        super(context);
        this.contentResolver = context.getContentResolver();
        this.context = context;
    }

    @Override
    protected DBModel getPersistenceModel() {
        return EncounterHistoryAPM.messageChannels;
    }

    @Override
    protected ContentValues itemToContentValues(MPrivateMessagingChannel item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MPrivateMessagingChannel cursorToItem(Cursor cursor) {
        final long pkID = EncounterHistoryAPM.extractPKID(cursor);
        return new MPrivateMessagingChannel(
                pkID,
                PPrivateMessagingChannels.extractChannelName(cursor),
                PPrivateMessagingChannels.extractSharedSecret(cursor),
                PPrivateMessagingChannels.extractMyTopicHandle(cursor),
                PPrivateMessagingChannels.extractOtherTopicHandle(cursor));
    }

    public void insertEntry(Identifier otherPubKey, Identifier sharedSecret, Identifier channelName, String myTopicHandle, String otherTopicHandle) {
        Log.d(TAG, "Inserting entry for channel " + channelName.toString() + ", " + myTopicHandle + ", " + otherTopicHandle);
        final ContentValues values = new ContentValues();
        values.put(PPrivateMessagingChannels.Columns.otherPubKey, otherPubKey.getBytes());
        values.put(PPrivateMessagingChannels.Columns.channelName, channelName.getBytes());
        values.put(PPrivateMessagingChannels.Columns.sharedSecret, sharedSecret.getBytes());
        values.put(PPrivateMessagingChannels.Columns.myTopicHandle, myTopicHandle);
        values.put(PPrivateMessagingChannels.Columns.otherTopicHandle, otherTopicHandle);
        context.getContentResolver().insert(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.messageChannels), values);
    }

    public byte[] getSharedSecretByTopicHandle(final String myTopicHandle) {
        byte[] result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.messageChannels), null,
                PPrivateMessagingChannels.Columns.myTopicHandle + " = ?", new String[] { myTopicHandle }, null);
        if (cursor.moveToNext()) {
            result = EncounterHistoryAPM.messageChannels.extractSharedSecret(cursor);
        }
        cursor.close();
        return result;
    }

    public MPrivateMessagingChannel getEntryFromTopicHandle(final String myTopicHandle) {
        Log.d(TAG, "Getting entry with topic handle " + myTopicHandle);
        MPrivateMessagingChannel result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.messageChannels), null,
                PPrivateMessagingChannels.Columns.myTopicHandle + " = ?", new String[] { myTopicHandle }, null);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }

    public MPrivateMessagingChannel getEntryFromTopicTitle(String topicTitle) {
        Log.d(TAG, "Getting entry with topic title " + topicTitle);
        MPrivateMessagingChannel result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.messageChannels), null,
                "HEX("+ PPrivateMessagingChannels.Columns.channelName+")" + " = ?", new String[] { topicTitle }, null);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }

    public MPrivateMessagingChannel getEntryFromOtherPubKey(String otherPubKey) {
        MPrivateMessagingChannel result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.messageChannels), null,
                "HEX("+ PPrivateMessagingChannels.Columns.otherPubKey +")" + " = ?", new String[] { otherPubKey }, null);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }
}
