package org.mpisws.encounters.encounterhistory.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;

import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;

public class EncounterStartedEvent extends EncounterEvent {
    private static final String TAG = EncounterStartedEvent.class.getSimpleName();
    private static final long serialVersionUID = 3977070185056288814L;

    public EncounterStartedEvent(long pkid, long startTime, Identifier advert, Identifier myNonce, Identifier myDHPubKey, Identifier myDHKey) {
        super(pkid, startTime, startTime, -1L, null, myNonce, myDHPubKey, myDHKey, advert, null);
    }

    @Override
    public void broadcast(Context context) {
        Log.v(TAG, "Broadcasting started encounter");
        final Intent i = new Intent(context, EncounterEventReceiver.class);
        i.putExtra("encounterEvent", this);
        context.sendBroadcast(i);
    }

    @Override
    public void persistIntoDatabase(Context context) {
        Log.d(TAG, "Inserting new EIDSS Entry for new encounter: " + pkid + " " + receivedNonce + " " + myNonce);
        new EncounterEntriesBridge(context).insertEntry(pkid, startTime, receivedNonce, myNonce, myDHKey, myDHPubKey,false,
        GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());

        // FIREBASE: log encounter start event
        Bundle params = new Bundle();
        params.putLong("pkid", pkid);
        params.putLong("time", startTime);
        params.putString("receivedNonce", receivedNonce.toString());
        params.putString("myNonce", myNonce.toString());
        //GlobalObjectRegistry.getObject(FirebaseAnalytics.class).logEvent("encounter_start", params);
    }
}
