package org.mpisws.encounters.encounterhistory.bridges;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.mpisws.database.DBModel;
import org.mpisws.encounters.dbplatform.AbstractEncountersBridge;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;
import org.mpisws.encounters.encounterhistory.PInitiatePrivateMessagingChannels;
import org.mpisws.encounters.encounterhistory.models.MInitiatePrivateMessagingChannel;
import org.mpisws.helpers.Identifier;

public class InitiateMessagingChannelsBridge extends AbstractEncountersBridge<MInitiatePrivateMessagingChannel> {
    private static final String TAG = InitiateMessagingChannelsBridge.class.getSimpleName();
    private final ContentResolver contentResolver;
    private final Context context;

    public InitiateMessagingChannelsBridge(final Context context) {
        super(context);
        this.contentResolver = context.getContentResolver();
        this.context = context;
    }

    @Override
    protected DBModel getPersistenceModel() {
        return EncounterHistoryAPM.messageChannels;
    }

    @Override
    protected ContentValues itemToContentValues(MInitiatePrivateMessagingChannel item) {
        return null;
    }

    @Override
    public MInitiatePrivateMessagingChannel cursorToItem(Cursor cursor) {
        final long pkID = EncounterHistoryAPM.extractPKID(cursor);
        return new MInitiatePrivateMessagingChannel(
                pkID,
                PInitiatePrivateMessagingChannels.extractMyDHPrivKey(cursor),
                PInitiatePrivateMessagingChannels.extractMyDHPubKey(cursor),
                PInitiatePrivateMessagingChannels.extractPubKeyTopicHandle(cursor));
    }

    public void insertEntry(Identifier myDHPrivKey, Identifier myDHPubKey, String pubKeyTopicHandle) {
        final ContentValues values = new ContentValues();
        values.put(PInitiatePrivateMessagingChannels.Columns.myDHPrivKey, myDHPrivKey.getBytes());
        values.put(PInitiatePrivateMessagingChannels.Columns.myDHPubKey, myDHPubKey.getBytes());
        values.put(PInitiatePrivateMessagingChannels.Columns.pubKeyTopicHandle, pubKeyTopicHandle);
        context.getContentResolver().insert(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.initiateMessagingChannels), values);
    }


    public MInitiatePrivateMessagingChannel getEntryWithTopicHandle(final String topicHandle) {
        MInitiatePrivateMessagingChannel result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.initiateMessagingChannels), null,
                PInitiatePrivateMessagingChannels.Columns.pubKeyTopicHandle + " = ?", new String[] { topicHandle }, null);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }

}
