package org.mpisws.encounters.encounterhistory.models;

import org.mpisws.encounters.dbplatform.AbstractMemoryObject;
import org.mpisws.encounters.lib.Preferences;

import java.io.Serializable;
import static org.mpisws.encounters.clients.CloudAgentHelpers.COMPONENTDELIMITER;

public class MLocation extends AbstractMemoryObject implements Serializable {
    private static final String TAG = MLocation.class.getSimpleName();
    private static final long serialVersionUID = 7973198346712274576L;

    private final double latitude;
    private final double longitude;
    private final long timestamp;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public long getTimestamp() {
        return timestamp;
    }


    public MLocation(long pkID, double latitude, double longitude, long timestamp) {
        super(pkID);
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    public String valsToString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(latitude)).append(COMPONENTDELIMITER);
        sb.append(String.valueOf(longitude)).append(COMPONENTDELIMITER);
        sb.append(String.valueOf(timestamp));
        return sb.toString();
    }

 }
