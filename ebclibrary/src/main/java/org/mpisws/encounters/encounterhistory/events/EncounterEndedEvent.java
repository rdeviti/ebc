package org.mpisws.encounters.encounterhistory.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;

import java.util.List;

public class EncounterEndedEvent extends EncounterEvent {
    private static final String TAG = EncounterEndedEvent.class.getSimpleName();
    private static final long serialVersionUID = -6822428069212190684L;

    public EncounterEndedEvent(final long pkid, final long endTime, final List<RSSIEntry> rssiEntries) {
        super(pkid, null, endTime, endTime, rssiEntries, null, null, null, null, null);
    }

    @Override
    public void broadcast(Context context) {
        Log.v(TAG, "Broadcasting ended encounter");
        final Intent i = new Intent(context, EncounterEventReceiver.class);
        i.putExtra("encounterEvent", this);
        context.sendBroadcast(i);
    }

    @Override
    public void persistIntoDatabase(Context context) {
        new EncounterEntriesBridge(context).updateEndTime(pkid, endTime);
        Log.d(TAG, "- END Ongoing Encounter with PKID: " + pkid + " and endtime " + endTime);

        // FIREBASE: Log encounter end event
        Bundle params = new Bundle();
        params.putLong("pkid", pkid);
        params.putLong("time", endTime);
        //GlobalObjectRegistry.getObject(FirebaseAnalytics.class).logEvent("encounter_end", params);
    }
}
