package org.mpisws.encounters.encounterhistory.bridges;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.database.DBEncounterInfo;
import org.mpisws.database.DBModel;
import org.mpisws.database.databaseSchema.DBEncounterEntries;
import org.mpisws.encounters.dbplatform.AbstractEncountersBridge;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;
import org.mpisws.encounters.encounterhistory.PEncounterEntries;
import org.mpisws.encounters.encounterhistory.models.MEncounterEntry;
import org.mpisws.encounters.encounterhistory.models.MLocation;
import org.mpisws.encounters.lib.time.TimeInterval;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EncounterEntriesBridge extends AbstractEncountersBridge<MEncounterEntry> {
    private static final String TAG = EncounterEntriesBridge.class.getSimpleName();
    private final ContentResolver contentResolver;
    private final Context context;

    public static class Methods {
        public static class GetEncountersWithConstraint {
            public static final String name = "getEncountersWithConstraint";
            public static class ParameterKeys {
                public static final String query = "query";
            }
        }
    }

    public EncounterEntriesBridge(final Context context) {
        super(context);
        this.contentResolver = context.getContentResolver();
        this.context = context;
    }

    @Override
    protected DBModel getPersistenceModel() {
        return EncounterHistoryAPM.encounterEntries;
    }

    @Override
    protected ContentValues itemToContentValues(MEncounterEntry item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MEncounterEntry cursorToItem(Cursor cursor) {
        final long pkID = EncounterHistoryAPM.extractPKID(cursor);
        return new MEncounterEntry(
                pkID,
                PEncounterEntries.extractUserHandle(cursor),
                PEncounterEntries.extractEncounterPKID(cursor),
                new Identifier(PEncounterEntries.extractSharedSecret(cursor)),
                new Identifier(PEncounterEntries.extractEncounterID(cursor)),
                PEncounterEntries.extractStartTime(cursor),
                PEncounterEntries.extractEndTime(cursor),
                new Identifier(PEncounterEntries.extractMyNonce(cursor)),
                new Identifier(PEncounterEntries.extractMyDHPrivKey(cursor)),
                new Identifier(PEncounterEntries.extractMyDHPubKey(cursor)),
                new Identifier(PEncounterEntries.extractReceivedNonce(cursor)),
                PEncounterEntries.extractPostedNonceTopic(cursor),
                PEncounterEntries.extractShouldPostForLinking(cursor),
                PEncounterEntries.extractMyTopicHandle(cursor),
                PEncounterEntries.extractOtherTopicHandle(cursor));
    }

    public void insertEntry(long encPKID, long startTime, Identifier receivedNonce, Identifier myNonce,
                                Identifier myDHPrivKey, Identifier myDHPubKey, boolean shouldPostNonceToLink,
                                String userHandle) {
        Log.d(TAG, "Inserting Entry " + encPKID + ", " + receivedNonce + ", " + myNonce);
        if (receivedNonce == null) return;
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.encounterPKID, encPKID);
        values.put(PEncounterEntries.Columns.receivedNonce, receivedNonce.getBytes());
        values.put(PEncounterEntries.Columns.myNonce, myNonce.getBytes());
        values.put(PEncounterEntries.Columns.myDHPrivKey, myDHPrivKey.getBytes());
        values.put(PEncounterEntries.Columns.myDHPubKey, myDHPubKey.getBytes());
        values.put(PEncounterEntries.Columns.postedNonceTopic, false);
        values.put(PEncounterEntries.Columns.timestampStart, startTime);
        values.put(PEncounterEntries.Columns.timestampEnd, -1L);
        values.put(PEncounterEntries.Columns.myTopicHandle, "");
        values.put(PEncounterEntries.Columns.otherTopicHandle, "");
        values.put(PEncounterEntries.Columns.shouldPostNonceToLink, shouldPostNonceToLink);
        values.put(PEncounterEntries.Columns.userHandle, userHandle);
        try {
            context.getContentResolver().insert(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), values);
        } catch (Exception e) {
            Log.d(TAG, "Duplicate insert attempted");
        }
    }

    public void updateEndTime(long pkid, long endTime) {
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.timestampEnd, endTime);
        int updated = context.getContentResolver().update(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries),
                values, PEncounterEntries.Columns.encounterPKID + " = ?", new String[] { String.valueOf(pkid) });
        Log.d(TAG, "Updated " + updated + " entries with pkid " + pkid + " and end time " + endTime);
    }

    public List<MEncounterEntry> getUnconfirmedEIDs() {
        List<MEncounterEntry> result = new LinkedList<>();
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
                PEncounterEntries.Columns.sharedSecret + " IS NULL", null,
                PEncounterEntries.Columns.receivedNonce + " ASC");
        while (cursor.moveToNext()) {
            MEncounterEntry entry = cursorToItem(cursor);
            result.add(entry);
        }
        cursor.close();
        return result;
    }

    public MEncounterEntry getFirstEntryWithReceivedNonce(Identifier receivedNonce) {
        MEncounterEntry result = null;
        String whereClause = "HEX("+ PEncounterEntries.Columns.receivedNonce + ") = ?";
        String sortOrder = PEncounterEntries.Columns.timestampStart + " ASC";
        String[] selectionArgs = new String[] {receivedNonce.toString() };
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
            whereClause, selectionArgs, sortOrder);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }

    public MEncounterEntry getOneEncounterEntryOfReceivedNonce(Identifier receivedNonce) {
        MEncounterEntry result = null;
        String whereClause = "HEX("+ PEncounterEntries.Columns.receivedNonce + ") = ?";
        String sortOrder = PEncounterEntries.Columns.timestampStart + " ASC";
        String[] selectionArgs = new String[] {receivedNonce.toString() };
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
            whereClause, selectionArgs, sortOrder);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        return result;
    }

    public Pair<List<MLocation>, TimeInterval> getEIDLocsAndTimeOfEID(String eid) {
        MEncounterEntry result = null;
        String whereClause = "HEX(" + PEncounterEntries.Columns.encounterID + ") = ?";
        String[] selectionArgs = new String[]{eid};
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
                whereClause, selectionArgs, null);
        if (cursor.moveToNext()) {
            result = cursorToItem(cursor);
        }
        cursor.close();
        if (result != null) {
            List<MLocation> locs = new LocationBridge(context).getLocationsBetween(
                    result.getTimeInterval().getStartL(),
                    result.getTimeInterval().getEndL());
            return new ImmutablePair(locs, result.getTimeInterval());
        } else return null;
    }

    public byte[] getSharedSecretByTopicHandle(final String myTopicHandle) {
        byte[] result = null;
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
                PEncounterEntries.Columns.myTopicHandle + " = ?", new String[] { myTopicHandle }, null);
        if (cursor.moveToNext()) {
            Log.d(TAG, "Found secret " + new Identifier(EncounterHistoryAPM.encounterEntries.extractSharedSecret(cursor)).toString()
                + " with topic handle " + EncounterHistoryAPM.encounterEntries.extractMyTopicHandle(cursor));
            result = EncounterHistoryAPM.encounterEntries.extractSharedSecret(cursor);
        }
        cursor.close();
        return result;
    }

    public List<MEncounterEntry> getConfirmedEntriesAfterTime(long timestamp) {
        final String[] selectionArgs = new String[] { String.valueOf(timestamp) };
        String whereClause = "(" + PEncounterEntries.Columns.timestampEnd + " > ? OR "
                + PEncounterEntries.Columns.timestampEnd + " = -1)"
                + " AND " + PEncounterEntries.Columns.sharedSecret + " IS NOT NULL";
        String sortOrder = PEncounterEntries.Columns.timestampEnd + " ASC";
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries),
                null, whereClause, selectionArgs, sortOrder);
        List<MEncounterEntry> locs = new ArrayList();
        while (cursor.moveToNext()) {
            locs.add(cursorToItem(cursor));

        }
        cursor.close();
        return locs;
    }

    public void updateEncounterEntriesFrom(MEncounterEntry from, MEncounterEntry to) {
        // first, update the start time of all the to_entries to the start time of the from_entry
        final Uri uri1 = EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries);
        final ContentValues values1 = new ContentValues();
        values1.put(PEncounterEntries.Columns.timestampStart, from.getTimeInterval().getStartL());
        final String whereClause1 = PEncounterEntries.Columns.encounterPKID + " = " + String.valueOf(to.getEncounterPKID());
        final int updatedRows1 = context.getContentResolver().update(uri1, values1,  whereClause1, null);
        Utils.myAssert((updatedRows1 > 0));

        // next, update the end times and PKID of the from_entry to that of the to_entry
        final Uri uri2 = EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries);
        final ContentValues values2 = new ContentValues();
        values2.put(PEncounterEntries.Columns.encounterPKID, to.getEncounterPKID());
        values2.put(PEncounterEntries.Columns.timestampEnd, to.getTimeInterval().getEndL());
        final String whereClause2 = PEncounterEntries.Columns.encounterPKID + " = " + String.valueOf(from.getEncounterPKID());
        final int updatedRows2 = context.getContentResolver().update(uri2, values2,  whereClause2, null);
        Utils.myAssert(updatedRows2 > 0);
    }

    public void updateNoncePostedAsTopic(Identifier myNonce) {
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.postedNonceTopic, true);
        final String[] selectionArgs = new String[] { String.valueOf(myNonce) };
        String whereClause = "HEX(" + PEncounterEntries.Columns.myNonce + ") = ?";
        final Uri uri = EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries);
        final int updatedRows = context.getContentResolver().update(uri, values, whereClause, selectionArgs);
    }

    public void updateConfirmedEncounter(MEncounterEntry MEncounterEntry, Identifier secret, String myTopicHandle) {
        Identifier eid = Coder.convertSharedSecretToID(secret);
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.sharedSecret, secret.getBytes());
        values.put(PEncounterEntries.Columns.encounterID, eid.getBytes());
        values.put(DBEncounterEntries.Columns.myTopicHandle, myTopicHandle);
        String whereClause = "HEX(" + PEncounterEntries.Columns.receivedNonce + ") = ? AND HEX(" + PEncounterEntries.Columns.myNonce + ") = ?";
        String[] selectArgs = new String[] {MEncounterEntry.getReceivedNonce().toString(), MEncounterEntry.getMyNonce().toString()};
        int updatedRows = context.getContentResolver().update(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), values, whereClause, selectArgs);
        if (updatedRows != 1) {
            Log.e(TAG, "UPDATED " + updatedRows + " ROWS WITH NONCES " +
                    MEncounterEntry.getReceivedNonce().toString() + " and " + MEncounterEntry.getMyNonce().toString());
        }

        /* FIREBASE: log encounter confirm event
        Bundle params = new Bundle();
        params.putLong("time", timestamp);
        params.putString("receivedNonce", MEncounterEntry.getReceivedNonce().toString());
        params.putString("myNonce", MEncounterEntry.getMyNonce().toString());
        params.putString("encounterID", eid.toString());
        GlobalObjectRegistry.getObject(FirebaseAnalytics.class).logEvent("encounter_confirm", params);*/
    }

    public void updateTopicHandles(Identifier eid, String otherTopicHandle) {
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.otherTopicHandle, otherTopicHandle);
        String whereClause = "HEX(" + PEncounterEntries.Columns.encounterID + ") = ?";
        String[] selectArgs = new String[] {eid.toString()};
        int updatedRows = context.getContentResolver().update(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), values, whereClause, selectArgs);
        if (updatedRows != 1) {
            Log.e(TAG, "UPDATED " + updatedRows + " ROWS WITH EID " + eid.toString());
        }
    }

    public List<MEncounterEntry> getEntriesToPostForLinking() {
        String whereClause = PEncounterEntries.Columns.shouldPostNonceToLink + " = 1";
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(
                EncounterHistoryAPM.encounterEntries), null, whereClause, null, null);
        List<MEncounterEntry> results = new ArrayList();
        while (cursor.moveToNext()) {
            results.add(cursorToItem(cursor));
        }
        cursor.close();
        return results;
    }

    public void updateNoncePostedForLinking(Identifier myNonce) {
        final ContentValues values = new ContentValues();
        values.put(PEncounterEntries.Columns.shouldPostNonceToLink, false);
        final String[] selectionArgs = new String[] { String.valueOf(myNonce) };
        String whereClause = "HEX(" + PEncounterEntries.Columns.myNonce + ") = ?";
        final Uri uri = EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries);
        final int updatedRows = context.getContentResolver().update(uri, values, whereClause, selectionArgs);
    }

    public void finalizeOldEncounters() {
        Log.d(TAG, "Finalizing encounters");
        final ContentValues values = new ContentValues();
        String whereClause = PEncounterEntries.Columns.timestampEnd+ " = ?";
        final String[] selectionArgs = new String[] { String.valueOf(-1) };
        values.put(PEncounterEntries.Columns.timestampEnd, System.currentTimeMillis());
        final Uri uri = EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries);
        final int updatedRows = context.getContentResolver().update(uri, values, whereClause, selectionArgs);
        Log.d(TAG, "Finalized: updated " + String.valueOf(updatedRows));
    }

    public List<DBEncounterInfo> getEncounterInfosFromQuery(String query) {
        Log.d(TAG, "Calling getEncounterInfosFromQuery " + query);
        Bundle bundle = new Bundle();
        bundle.putString(Methods.GetEncountersWithConstraint.ParameterKeys.query, query);
        Bundle result = contentResolver.call(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries),
                Methods.GetEncountersWithConstraint.name, null, bundle);
        if (result == null) {
            return null;
        }
        return (List<DBEncounterInfo>) result.getSerializable("result");
    }

    public void dumpEncounterDatabase() {
        final Cursor cursor = context.getContentResolver().query(EncounterHistoryAPM.getContentURI(EncounterHistoryAPM.encounterEntries), null,
                null, null, null);
        String encounterDBString = "ENCOUNTER DB DUMP\n";
        while (cursor.moveToNext()) {
            MEncounterEntry entry = cursorToItem(cursor);
            if (entry.getSecret() == null || entry.getSecret().getBytes() == null)
                continue;
            encounterDBString += String.format("[%s]: (%d-%d) [%b/%b], (%s, %s)\n",
                    entry.getSecret().toString(),
                    entry.getTimeInterval().getStartL(), entry.getTimeInterval().getEndL(),
                    entry.getPostedNonceTopic(), entry.getShouldPostNonceForLinking(),
                    entry.getMyTopicHandle() == null ? "" : entry.getMyTopicHandle(),
                    entry.getOtherTopicHandle() == null ? "" : entry.getOtherTopicHandle());
        }
        cursor.close();
        Log.d("ENCOUNTER DUMP", encounterDBString);
    }
}
