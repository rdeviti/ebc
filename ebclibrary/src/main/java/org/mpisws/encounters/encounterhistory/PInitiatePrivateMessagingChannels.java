package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;

import org.mpisws.database.databaseSchema.DBInitiatePrivateMessagingChannels;

public class PInitiatePrivateMessagingChannels extends DBInitiatePrivateMessagingChannels {
    private static PInitiatePrivateMessagingChannels instance = new PInitiatePrivateMessagingChannels();
    private PInitiatePrivateMessagingChannels() {
        super();
    };
    public static PInitiatePrivateMessagingChannels getInstance() {return instance;}

    public static String extractPubKeyTopicHandle(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(Columns.pubKeyTopicHandle));
    }

    public static byte[] extractMyDHPrivKey(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.myDHPrivKey));
    }
    public static byte[] extractMyDHPubKey(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.myDHPubKey));
    }
}
