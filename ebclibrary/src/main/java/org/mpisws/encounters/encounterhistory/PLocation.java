package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;

import org.mpisws.database.databaseSchema.DBLocation;

public class PLocation extends DBLocation {
    private static PLocation instance = new PLocation();
    public static PLocation getInstance() {return instance;}
    public PLocation() {
        super();
    }
    public PLocation(long pkid, double latitude, double longitude, long timestamp) {
        super(pkid, latitude, longitude, timestamp);
    }

    public static double extractLatitude(final Cursor cursor) {
        return cursor.getDouble(cursor.getColumnIndexOrThrow(Columns.latitude));
    }

    public static double extractLongitude(final Cursor cursor) {
        return cursor.getDouble(cursor.getColumnIndexOrThrow(Columns.longitude));
    }

    public static long extractTimestamp(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(Columns.updatedTimestamp));
    }
}
