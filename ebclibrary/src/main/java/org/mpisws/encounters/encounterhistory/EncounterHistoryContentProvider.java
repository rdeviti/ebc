package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import org.mpisws.database.DBEncounterInfo;
import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.dbplatform.AggregatePersistenceModel;
import org.mpisws.encounters.dbplatform.ContentProviderBase;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EncounterHistoryContentProvider extends ContentProviderBase {
    private static final String TAG = EncounterHistoryContentProvider.class.getSimpleName();

    @Override
    protected AggregatePersistenceModel getAggregatePersistenceModel() {
        return EncounterHistoryAPM.getInstance();
    }

    @Override
    protected SQLiteOpenHelper getDBOpenHelper() {
        return new EncounterHistoryDBOpenHelper(getContext());
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        if (EncounterEntriesBridge.Methods.GetEncountersWithConstraint.name.equals(method)) {
            final Bundle result = new Bundle();
            if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return null;
            final SQLiteDatabase db = dbHelper.getReadableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
            Cursor cursor = db.rawQuery(extras.getString(EncounterEntriesBridge.Methods.GetEncountersWithConstraint.ParameterKeys.query), null);
            List<DBEncounterInfo> es = new ArrayList<>();

            long minTime = -1;
            long maxTime = -1;
            long encPKID = -1;
            String myTopicHandle = null;
            byte[] curSS = null;
            byte[] eid = null;
            while (cursor.moveToNext()) {
                byte[] newSS = PEncounterEntries.extractSharedSecret(cursor);
                if (curSS == null) {
                    encPKID = PEncounterEntries.extractEncounterPKID(cursor);
                    eid = PEncounterEntries.extractEncounterID(cursor);
                    myTopicHandle = PEncounterEntries.extractMyTopicHandle(cursor);
                    curSS = newSS;
                }
                long newTime = PLocation.extractTimestamp(cursor);
                // just update the time frame if we're just looking at the same encounter entry
                // with a different location
                if (Arrays.equals(newSS, curSS)) {
                    minTime = (minTime == -1 || newTime < minTime) ? newTime : minTime;
                    maxTime = (maxTime == -1 || newTime > maxTime) ? newTime : maxTime;
                } else {
                    Log.d(TAG, "Query: Adding DBEncounterInfo: \n"
                            + "\t pkid: " + encPKID
                            + "\t eid: " + new Identifier(eid).toString()
                            + "\t ss: " + new Identifier(curSS).toString()
                            + "\t myTH: " + myTopicHandle);
                    // TODO
                    es.add(new DBEncounterInfo(encPKID, eid, curSS, minTime, maxTime, myTopicHandle, ""));

                    // reset
                    minTime = -1;
                    maxTime = -1;
                    encPKID = PEncounterEntries.extractEncounterPKID(cursor);
                    eid = PEncounterEntries.extractEncounterID(cursor);
                    myTopicHandle = PEncounterEntries.extractOtherTopicHandle(cursor);
                    curSS = newSS;
                }
            }
            Log.d(TAG, "Found " + es.size() + " encounters matching query");
            result.putSerializable("result", (Serializable) es);
            cursor.close();
            return result;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
