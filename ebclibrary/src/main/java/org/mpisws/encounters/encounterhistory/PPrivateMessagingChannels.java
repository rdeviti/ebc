package org.mpisws.encounters.encounterhistory;

import android.database.Cursor;

import org.mpisws.database.databaseSchema.DBPrivateMessagingChannels;

public class PPrivateMessagingChannels extends DBPrivateMessagingChannels {
    private static PPrivateMessagingChannels instance = new PPrivateMessagingChannels();
    private PPrivateMessagingChannels() {
        super();
    };
    public static PPrivateMessagingChannels getInstance() {return instance;}

    public static String extractMyTopicHandle(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(DBPrivateMessagingChannels.Columns.myTopicHandle));
    }

    public static String extractOtherTopicHandle(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(DBPrivateMessagingChannels.Columns.otherTopicHandle));
    }

    public static byte[] extractSharedSecret(final Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.sharedSecret));
    }
    
    public static byte[] extractChannelName(final Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(Columns.channelName));
    }
}
