package org.mpisws.encounters.encounterformation;

import android.content.Context;
import android.util.Log;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;
import org.mpisws.encounters.encounterhistory.bridges.LocationBridge;
import org.mpisws.encounters.encounterhistory.models.MEncounterEntry;
import org.mpisws.encounters.clients.CommunicationHelpers;
import org.mpisws.encounters.lib.Preferences;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.ThreadPoolManager;
import org.mpisws.helpers.Utils;
import org.mpisws.messaging.EpochLinkMessage;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.mpisws.encounters.encounterformation.EncounterFormationCore.getOngoingEncounters;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mDHKey;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mDHPubKey;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mNonce;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.EPOCH_LINK;

/**
 * Created by tslilyai on 2/21/18.
 */

public class EncounterConfirmationAndEpochLinking {
    private static final String TAG = EncounterConfirmationAndEpochLinking.class.getSimpleName();
    EncounterEntriesBridge encountersBridge;
    LocationBridge locationBridge;
    Context context;

    public EncounterConfirmationAndEpochLinking(Context context) {
        this.context = context;
        encountersBridge = new EncounterEntriesBridge(context);
        locationBridge = new LocationBridge(context);
    }

    public void confirmEncountersViaES() {
        List<String> advertsToConfirm = new ArrayList<>();

        // make sure no ES or DB actions are run on the main thread!
        ThreadPoolManager.getInstance().runTask(() -> {
            // 1) Get all advertised nonces that we have yet to use to form a secret key
            List<MEncounterEntry> unconfirmedEIDs = encountersBridge.getUnconfirmedEIDs();

            // 2) Remember to create a topic for every unposted nonce we advertised.
            //    Note that there can be repeats, so let's make this a set
            // 3) Also remember the associated received oldNonce to look up to get the DH public key
            Set<String> mySeenNonces = new HashSet<>();
            Set<String> recvSeenNonces = new HashSet<>();
            List<Pair<Identifier, Identifier>> advertsToPost = new ArrayList<>();
            for (int i = 0; i < unconfirmedEIDs.size(); i++) {
                MEncounterEntry eid = unconfirmedEIDs.get(i);
                // remember to post this oldNonce (and also which oldNonce we're posting)
                if (!eid.getPostedNonceTopic()) {
                    if (!mySeenNonces.contains(eid.getMyNonce().toString())) {
                        advertsToPost.add(new ImmutablePair(eid.getMyNonce(), eid.getMyDHPubKey()));
                        mySeenNonces.add(eid.getMyNonce().toString());
                    }
                }
                if (!recvSeenNonces.contains(eid.getReceivedNonce().toString())) {
                    advertsToConfirm.add(eid.getReceivedNonce().toString());
                    recvSeenNonces.add(eid.getReceivedNonce().toString());
                }
            }

            // 4) create topics for unposted adverts and update the posted status of the successfully posted adverts
            Log.d(TAG, ": Posting " + advertsToPost.size() + " broadcasted adverts");
            List<String> topicHandles = ESClient.getInstance().createTopics(advertsToPost);

            for (int i = 0; i < advertsToPost.size(); i++) {
                if (topicHandles.get(i) != null) {
                    encountersBridge.updateNoncePostedAsTopic(advertsToPost.get(i).getLeft());
                }
            }

            // 5) get dh pub key of encounters (null if doesn't exist)
            Log.d(TAG, ": Confirming " + advertsToConfirm.size() + " received adverts");
            List<String> confirmPubKeys = ESClient.getInstance().getTopicTexts(advertsToConfirm);

            // 6) calculate the secret key and eids for all encounters for which you got a dh pub key
            List<Pair<Identifier, Identifier>> eidsToCreate = new ArrayList<>();
            List<Identifier> secrets = new ArrayList<>();
            for (int i = 0; i < unconfirmedEIDs.size(); i++) {
                secrets.add(null);
                eidsToCreate.add(null);
            }
            boolean found = false;
            int advertIndex = 0;
            String otherDHPubKey = "";
            String receivedNonce = "";
            for (int i = 0; i < unconfirmedEIDs.size(); i++) {
                MEncounterEntry unconfirmedEID = unconfirmedEIDs.get(i);
                String eidRecvNonce = unconfirmedEID.getReceivedNonce().toString();

                if (receivedNonce.compareTo(eidRecvNonce) != 0 || receivedNonce.compareTo("")==0) {
                    if (advertIndex >= advertsToConfirm.size()) {
                        break;
                    }
                    otherDHPubKey = confirmPubKeys.get(advertIndex);
                    receivedNonce = advertsToConfirm.get(advertIndex);
                    advertIndex++;
                }
                if (otherDHPubKey != null) {
                    found = true;
                    Log.d(TAG, "Computing secret with received nonce " + eidRecvNonce + " with DHPubKey " + otherDHPubKey);
                    secrets.set(i, computeSecretKey(unconfirmedEID.getMyDHPrivKey(), unconfirmedEID.getReceivedNonce(), otherDHPubKey));
                    Identifier eid = Coder.convertSharedSecretToID(secrets.get(i));
                    eidsToCreate.set(i, new ImmutablePair<>(eid, eid));
                }
            }
            if (!found) return;

            // 7) create the successfully determined encounter topics
            topicHandles = ESClient.getInstance().createTopics(eidsToCreate);
            for (int i = 0; i < unconfirmedEIDs.size(); i++) {
                if (secrets.get(i) != null && topicHandles.get(i) != null) {
                    // put the topic handle and secret into the database
                    Log.d(TAG, "Adding ss, topichandle: " + secrets.get(i).toString()+ ", "+topicHandles.get(i) + " to "
                            + unconfirmedEIDs.get(i).getMyNonce().toString() + ", " + unconfirmedEIDs.get(i).getReceivedNonce().toString());
                    encountersBridge.updateConfirmedEncounter(unconfirmedEIDs.get(i), secrets.get(i), topicHandles.get(i));
                }
            }
        });
    }

    private Identifier computeSecretKey(Identifier myDHKey, Identifier advert, String dhPubKey) {
        byte[] secretKey = SDDR_Native.c_computeSecretKeyWithSHA(myDHKey.getBytes(), advert.getBytes(), Utils.hexStringToByteArray(dhPubKey));
        if (secretKey != null) {
            Identifier secretKeyID = new Identifier(secretKey);
            Log.d(TAG, "Got secret key for received advert " + advert.toString() + ": " + secretKeyID.toString());
            return secretKeyID;
        }
        return null;
    }

    public void insertNewNoncesIntoDatabase() {
        // insert new entries with ongoing encounters into database
        Map<String, Pair<Long, Long>> ongoingEnc = getOngoingEncounters();
        ongoingEnc.forEach((k, v) -> {
            ThreadPoolManager.getInstance().runTask(() -> {
                Log.d(TAG, "Inserting new Entry epoch change : " + v.getLeft() + " " + k + " " + mNonce);
                encountersBridge.insertEntry(v.getLeft()/*pkid*/, v.getRight() /*startTime*/,
                        new Identifier(Utils.hexStringToByteArray(k)),
                        mNonce, mDHKey, mDHPubKey, true,
                        GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
            });
        });
    }

    public void postUnpostedNoncesThatNeedLinking() {
        ThreadPoolManager.getInstance().runTask(() -> {
            List<ESMessage> msgsToSend = new ArrayList<>();
            List<MEncounterEntry> toPostEntries = encountersBridge.getEntriesToPostForLinking();
            List<MEncounterEntry> entriesToPostOn = new ArrayList<>(toPostEntries.size());
            List<Identifier> topicHandlesToGet = new ArrayList<>(toPostEntries.size());

            Log.d(TAG, "Need to post " + toPostEntries.size() + " adverts to link!");

            // (1) get all topic handles that we need to
            for (int i = 0; i < toPostEntries.size(); i++) {
                entriesToPostOn.add(null);
            }
            for (int i = 0; i < toPostEntries.size(); i++) {
                entriesToPostOn.add(null);
                MEncounterEntry entry = toPostEntries.get(i);
                // find the first encounter topic associated with this encounter / received nonce
                MEncounterEntry entryToPostOn = encountersBridge.getFirstEntryWithReceivedNonce(entry.getReceivedNonce());
                entriesToPostOn.set(i,entryToPostOn);
                // only post if confirmed
                if (entryToPostOn.getEncounterID() == null || entryToPostOn.getEncounterID().getBytes() == null) {
                    continue;
                }
                if (entryToPostOn.getOtherTopicHandle() == null || entry.getOtherTopicHandle().compareTo("") == 0) {
                    // get the topic handle of the old encounter so we can post on it
                    topicHandlesToGet.add(entryToPostOn.getEncounterID());
                }
            }
            List<String> newTopicHandles = ESClient.getInstance().getTopicHandles(topicHandlesToGet);
            Log.d(TAG, "Getting topic handles of " + newTopicHandles.size() + " encounters");

            // (2) update topic handle information
            int index = 0; // this tracks the position in the "topicsToGet" array
            for (int i = 0; i < toPostEntries.size(); i++) {
                MEncounterEntry entryToPostOn = entriesToPostOn.get(i);
                if (entryToPostOn.getEncounterID() == null || entryToPostOn.getEncounterID().getBytes() == null) {
                    continue;
                }
                if (entryToPostOn.getOtherTopicHandle() == null || entryToPostOn.getOtherTopicHandle().compareTo("") == 0) {
                    if (newTopicHandles.get(index) != "") {
                        // update the entry with the topic handle in the database
                        encountersBridge.updateTopicHandles(topicHandlesToGet.get(index), newTopicHandles.get(index));
                        // update the eid with the topic handle so we know where to send it
                        entryToPostOn.setOtherTopicHandle(newTopicHandles.get(index));
                        Log.d(TAG, "Set topic handle of " + entryToPostOn.toString() + " to " + newTopicHandles.get(index));
                    }
                    index++;
                }
            }

            // (3) encode the messages to send (with its constraints) and form the message contents
            for (int i = 0; i < toPostEntries.size(); i++) {
                MEncounterEntry entry = toPostEntries.get(i);
                MEncounterEntry entryToPostOn = entriesToPostOn.get(i);
                if (entryToPostOn.getOtherTopicHandle() == null || entryToPostOn.getOtherTopicHandle().compareTo("") == 0) {
                    // we failed to find the topic handle
                    continue;
                }
                // we want to post both this advert and the old one
                EpochLinkMessage epochLinkMessage = new EpochLinkMessage.EpochLinkMessageBuilder()
                        .addOldNonce(entryToPostOn.getMyNonce().toString())
                        .addNewNonce(entry.getMyNonce().toString())
                        .build();
                msgsToSend.add(new ESMessage(epochLinkMessage.toSendMessageText(entryToPostOn.getSecret().getBytes()),
                        entryToPostOn.getEncounterID().toString(),
                        entryToPostOn.getOtherTopicHandle(),
                        true, null, true, -1));
                encountersBridge.updateNoncePostedForLinking(entry.getMyNonce());
            }
            ESClient.getInstance().sendMsgs(msgsToSend);
        });
    }

    public void processNoncesForLinking() {
        /*
         * NOTE: WE LINK *FORWARD* SO THAT THE "NEWER" PKID END TIME IS SET CORRECTLY
         */

        ThreadPoolManager.getInstance().runTask(() -> {
            // (1) get the nonce messages
            List<Pair<Identifier, Identifier>> oldNewNoncesToLink = new ArrayList<>();
            String lastReadCursor = Preferences.getInstance().getLatestReadMessageCursor();
            String continuationCursor = null, firstMsgCursor = null;
            boolean done = false;
            int loopRound = 0, maxLoops = 100;

            while(loopRound < maxLoops) {
                Pair<String, List<ReceivedMessageWrapper>> cursorAndMsgs = CommunicationHelpers.getMsgsPage(continuationCursor, context);
                List<ReceivedMessageWrapper> msgs = cursorAndMsgs.getRight();
                continuationCursor = cursorAndMsgs.getLeft();

                Log.d(TAG, "LINK: Loop round " + loopRound + " : found " + msgs.size() + " messages, continuation cursor " + continuationCursor);

                // didn't find any new messages
                if (msgs.size() == 0) {
                    break;
                }

                for (ReceivedMessageWrapper receivedMessageWrapper: msgs) {
                   if (receivedMessageWrapper.getCursor() == null) {
                        continue;
                    }
                    Log.d(TAG, "LINK: Msg cursor: " + receivedMessageWrapper.getCursor());
                    if (firstMsgCursor == null || lastReadCursor == null) {
                        firstMsgCursor = receivedMessageWrapper.getCursor();
                        Preferences.getInstance().setLatestReadMessageCursor(firstMsgCursor);
                        Log.d(TAG, "LINK: Setting latest read cursor to " + firstMsgCursor);
                    }
                    if (receivedMessageWrapper.getCursor().compareTo(lastReadCursor) == 0) {
                        // we're at the end of read messages
                        done = true;
                        break;
                    }
                    if (receivedMessageWrapper.getMsgType() != EPOCH_LINK) {
                        continue;
                    }
                    EpochLinkMessage epochLinkMessage = receivedMessageWrapper.getEpochLinkMessage();
                    String oldNonce = epochLinkMessage.getOldNonce();
                    String newNonce = epochLinkMessage.getNewNonce();
                    oldNewNoncesToLink.add(new ImmutablePair<>(
                            new Identifier(Utils.hexStringToByteArray(oldNonce)),
                            new Identifier(Utils.hexStringToByteArray(newNonce))));
                }
                if (done) {
                    break;
                }
                loopRound++;
            }
            if (oldNewNoncesToLink.size() == 0) return;

            // (2) process the link nonce pairs that we got
            for (Pair<Identifier, Identifier> nonces : oldNewNoncesToLink) {
                Identifier newNonce = nonces.getRight();
                Identifier oldNonce = nonces.getLeft();
                Log.d(TAG, "Got link with new advert : " + newNonce.toString() + " " + oldNonce.toString());

                // GET THE PKIDS CORRESPONDING TO THE ADVERTS
                MEncounterEntry entryNew = encountersBridge.getOneEncounterEntryOfReceivedNonce(newNonce);
                MEncounterEntry entryOld = encountersBridge.getOneEncounterEntryOfReceivedNonce(oldNonce);

                Utils.myAssert(entryOld != null);
                if (entryNew == null) {
                    Log.d(TAG, "New nonce not actually detected by us at any point " + newNonce);
                    continue;
                }

                // UPDATE THE ENCOUNTER TABLE ENTRIES
                encountersBridge.updateEncounterEntriesFrom(entryOld, entryNew);
            }
        });
    }
}
