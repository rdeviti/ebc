package org.mpisws.encounters.encounterformation;

import android.Manifest;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by tslilyai on 10/27/17.
 */

/**
 * SDDR_Core_Service runs in the background and uses an SDDR_Core object
 * to form and store encounters. Our API provides a wrapper around intent calls
 * to this service.
 */
public class EncounterFormationService extends Service {
    private static final String TAG = EncounterFormationService.class.getSimpleName();
    private EncounterFormationCore core;
    private Thread thread;
    public boolean was_destroyed = false;
    private static final int FOREGROUND_SERVICE_ON = 321;

    private void check_and_start_core() {
        if (was_destroyed) {
            was_destroyed = false;
        }
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bta = bluetoothManager.getAdapter();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location permissions required", Toast.LENGTH_SHORT).show();
            Log.v(TAG, "Location permissions not enabled");
            this.stopSelf();
        }

        if (bta == null || !bta.isEnabled()) {
            Toast.makeText(this, "Bluetooth required", Toast.LENGTH_SHORT).show();
            Log.v(TAG, "Bluetooth not enabled");
            this.stopSelf();
        }
        Log.v(TAG, "Bluetooth and location permissions enabled");
        if (core == null) {
            Log.v(TAG, "Starting SDDR_API Core from thread " + Thread.currentThread().getName());
            core = new EncounterFormationCore(this);
            thread = new Thread(core);
            thread.start();
        } else {
            Log.v(TAG, "SDDR_API already running");
        }
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "creating service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if (intent == null) {
            return START_STICKY;
        }

        if (intent.getExtras().containsKey("@string.start_sddr_service")) {
            check_and_start_core();
            return START_STICKY;
        }

        if (intent.getExtras().containsKey("confirmation_es"))
        {
            if (core != null && intent.getBooleanExtra("confirmation_es", false)) {
                Log.d(TAG, "Confirming encounters over ES!");
                core.setESConfirm(true);
            } else if (core != null) {
                core.setESConfirm(false);
            }
            return START_STICKY;
        }

        if (intent.getExtras().containsKey("confirmation_active"))
        {
            if (core != null && intent.getBooleanExtra("confirmation_active", false)) {
                Log.d(TAG, "Starting server to confirm over BT!");
                core.setActiveConfirm(true, getApplicationContext());
            } else if (core != null) {
                core.setActiveConfirm(false, null);
            }
            return START_STICKY;
        }

        if (intent.getExtras().containsKey("stop_sddr_service"))
        {
            if (core != null)
                core.stop();
            core = null;
            return START_NOT_STICKY;
        }
        else {
            Log.v(TAG, "Unknown intent");
            return START_STICKY;
        }
    }

    public void onDestroy() {
        Log.v(TAG, "Destroying service");
        if (core != null) {
            core.stop();
            core = null;
        }
        was_destroyed = true;
    }

    @Override
    public IBinder onBind(Intent intent) { return null; }
}
