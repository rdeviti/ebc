package org.mpisws.encounters.encounterformation;

/**
 * Created by tslilyai on 10/16/17.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.encounters.lib.Preferences;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;
import org.mpisws.encounters.encounterhistory.events.EncounterConfirmEvent;
import org.mpisws.helpers.Utils;

import java.util.HashMap;
import java.util.Map;

import static org.mpisws.encounters.EncounterBasedCommunication.CHANGE_EPOCH_TIME;
import static org.mpisws.encounters.encounterformation.GattServerClient.confirmEvents;

/**
 * SDDR_Core implements the core functionality of the SDDR protocol. It is started and called by the
 * SDDR_Core_Service. The functionalities it provides are:
 * - running discovery
 * - processing and storing encounter information in persistent storage
 * - running epoch changes and confirmation
 */
public class EncounterFormationCore implements Runnable {
    private static final String TAG = EncounterFormationCore.class.getSimpleName();
    protected static Identifier mDHPubKey;
    protected static Identifier mNonce;
    protected static Identifier mDHKey;
    protected static Map<String, Pair<Long, Long>> ongoingEncounters = new HashMap<>();
    protected EncounterConfirmationAndEpochLinking encounterConfirmationAndEpochLinking;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private Advertiser mAdvertiser;
    private ScannerProcessor mScannerProcessor;
    private long changeEpochTime;
    protected Context mService;
    protected Context appContext;
    private GattServerClient gattServerClient = new GattServerClient();
    private boolean confirmEncountersES = false;
    private boolean confirmEncountersBLE = false;

    EncounterFormationCore(EncounterFormationService service) {
        this.mService = service;
    }

    private void initialize() {
        Looper.prepare();
        if (SDDR_Native.c_RadioPtr <= 0) {
            SDDR_Native.c_mallocRadio();
        }
        Log.v(TAG, "Initialize SDDRCore on thread " + Thread.currentThread().getName());
        this.bluetoothManager = (BluetoothManager) mService.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mAdvertiser = new Advertiser();
        mScannerProcessor = new ScannerProcessor();

        mAdvertiser.initialize(mBluetoothAdapter);
        mScannerProcessor.initialize(mBluetoothAdapter, this);

        changeEpochTime = System.currentTimeMillis() + CHANGE_EPOCH_TIME;
        encounterConfirmationAndEpochLinking = new EncounterConfirmationAndEpochLinking(mService);

        // deal with login credentials here
        Preferences preferences;
        if ((preferences = GlobalObjectRegistry.getObject(Preferences.class)) == null) {
            preferences = new Preferences(mService);
            GlobalObjectRegistry.addObject(preferences);
        }
        // try to get ES Credentials
        Log.d(TAG, "UserHandle, Auth: " + preferences.getUserHandle() + ", " + preferences.getAuthorizationToken());
        ESClient.getInstance().setESCredentials(preferences.getUserHandle(), preferences.getAuthorizationToken());

        new EncounterEntriesBridge(mService).finalizeOldEncounters();
    }

    public void run() {
        initialize();
        Log.v(TAG, "Running core on thread " + Thread.currentThread().getName());
        startAdvertisingAndUpdateAdvert();
        mScannerProcessor.startScanning();
    }

    public void stop() {
        if (SDDR_Native.c_RadioPtr > 0)
            SDDR_Native.c_freeRadio();
        mAdvertiser.stopAdvertising();
        mScannerProcessor.stopScanning();
    }

    private void startAdvertisingAndUpdateAdvert() {
        mDHPubKey = new Identifier(SDDR_Native.c_getAdvertDHPubKey());
        mDHKey = new Identifier(SDDR_Native.c_getAdvertDHKey());
        mNonce = new Identifier(SDDR_Native.c_getMyAdvert());
        mAdvertiser.setAdData(mNonce.getBytes());
        mAdvertiser.resetAdvertiser();
    }

    protected void setESConfirm(boolean conn) {
        confirmEncountersES = conn;
    }

    protected void setActiveConfirm(boolean conn, Context appcontext) {
        if (gattServerClient == null || mAdvertiser == null) return;
        confirmEncountersBLE = conn;
        if (conn == true) {
            this.appContext = appcontext;
            gattServerClient.initialize(bluetoothManager, mService);
            mAdvertiser.setConnectable(true);
        } else {
            mAdvertiser.setConnectable(false);
            gattServerClient.dropConnections();
        }
    }

    public void postScanProcessing() {
        if (changeEpochTime < System.currentTimeMillis()) {
            Log.d(TAG, "CHANGING EPOCH, Confirmation with ES? " + confirmEncountersES + " Confirmation with BLE? " + confirmEncountersBLE);

            SDDR_Native.c_changeEpoch();
            changeEpochTime += CHANGE_EPOCH_TIME;
            startAdvertisingAndUpdateAdvert();

            // add database entries for our new nonce pairs with all currently receiving adverts
            encounterConfirmationAndEpochLinking.insertNewNoncesIntoDatabase();
            // post our nonces on the appropriate "old nonce" encounter topic (the "oldest" topic for this encounter)
            encounterConfirmationAndEpochLinking.postUnpostedNoncesThatNeedLinking();
            // retrieve and link using the new nonces we had posted on our encounter topics
            encounterConfirmationAndEpochLinking.processNoncesForLinking();

            // try to confirm encounters using the network
            if (confirmEncountersES && ESClient.getInstance().isSignedIn() && isNetworkAvailable()) {
                encounterConfirmationAndEpochLinking.confirmEncountersViaES();
            }
            // we can also confirm encounters using BLE
            else if (confirmEncountersBLE) {
                new Handler(appContext.getMainLooper()).post(() -> mScannerProcessor.startScanningActive(10000));
                if (confirmEvents != null) {
                    EncounterConfirmEvent ce = confirmEvents.poll();
                    while (ce != null) {
                        ce.broadcast(mService);
                        ce = confirmEvents.poll();
                    }
                }
            }

        }
    }

    public static Map<String, Pair<Long, Long>> getOngoingEncounters() {
        return ongoingEncounters;
    }

    public GattServerClient getGattServerClient() {
        return gattServerClient;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mService.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
