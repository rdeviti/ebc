package org.mpisws.encounters.encounterformation;

/**
 * Created by tslilyai on 10/18/17.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.ParcelUuid;
import android.util.Log;

import org.mpisws.helpers.Utils;

import java.nio.ByteBuffer;
import java.util.UUID;

import static android.bluetooth.le.AdvertiseSettings.ADVERTISE_MODE_LOW_POWER;

/**
 * Manages BLE Advertising.
 */
public class Advertiser {
    public static final int TOTAL_LENGTH = 31;
    public static final int SHA1_LENGTH = 20;
    public static final int PUUID_LENGTH = 16;
    public static final int NONCE_LENGTH_IN_ADVERT = SHA1_LENGTH - PUUID_LENGTH ;

    private static final String TAG = Advertiser.class.getSimpleName();
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;
    private AdvertiseCallback mAdvertiseCallback;
    private AdvertiseSettings mAdvertiseSettings;
    private UUID mUUID;
    private boolean connectable;
    public static byte[] mPUUID = new byte[PUUID_LENGTH];
    public static byte[] mAdData = new byte[NONCE_LENGTH_IN_ADVERT];

    public void initialize(BluetoothAdapter btAdapter) {
        mBluetoothLeAdvertiser = btAdapter.getBluetoothLeAdvertiser();
        connectable = false;
    }

    public void setConnectable(boolean connect) {
        this.connectable = connect;
    }

    public void setAdData(byte[] newData) {
        int amountToIncludeInPUUID = PUUID_LENGTH ;
        Utils.myAssert(newData.length == SHA1_LENGTH);

        // copy what data can fit into the puuid slot
        System.arraycopy(newData, 0, mPUUID, 0, amountToIncludeInPUUID);
        System.arraycopy(newData, amountToIncludeInPUUID, mAdData, 0, SHA1_LENGTH - amountToIncludeInPUUID);

        ByteBuffer bb = ByteBuffer.wrap(mPUUID);
        long high = bb.getLong();
        long low = bb.getLong();
        mUUID = new UUID(high, low);
        Log.v(TAG, "New Advert " + Utils.getHexString(mPUUID) + Utils.getHexString(mAdData));
    }

    public void resetAdvertiser() {
        if (mAdvertiseCallback != null)
            mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
        startAdvertising();
    }

    public void startAdvertising() {
        if (mAdvertiseCallback == null) {
            mAdvertiseCallback = new SDDRAdvertiseCallback();
            mAdvertiseSettings = buildAdvertiseSettings();
        }
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser.startAdvertising(
                    mAdvertiseSettings,
                    buildAdvertiseData(),
                    mAdvertiseCallback
            );
        }
    }

    /**
     * Stops BLE Advertising.
     */
    public void stopAdvertising() {
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
            mAdvertiseCallback = null;
        }
    }

    /**
     * Returns an AdvertiseData object which includes the Service UUID and Device Name.
     */
    private AdvertiseData buildAdvertiseData() {

        /**
         * Note: There is a strict limit of 31 Bytes on packets sent over BLE Advertisements.
         *  This includes everything put into AdvertiseData including UUIDs, device info, &
         *  arbitrary service or manufacturer data.
         *  Attempting to send packets over this limit will result in a failure with error code
         *  AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE. Catch this error in the
         *  onStartFailure() method of an AdvertiseCallback implementation.
         */
        AdvertiseData.Builder dataBuilder = new AdvertiseData.Builder();
        dataBuilder.setIncludeDeviceName(false);
        dataBuilder.setIncludeTxPowerLevel(false);
        dataBuilder.addServiceData(new ParcelUuid(mUUID), mAdData);
        return dataBuilder.build();
    }

    /**
     * Returns an AdvertiseSetParameters object
     */
    private AdvertiseSettings buildAdvertiseSettings() {
        AdvertiseSettings.Builder settingsBuilder = new AdvertiseSettings.Builder();
        settingsBuilder.setConnectable(connectable);
        settingsBuilder.setAdvertiseMode(ADVERTISE_MODE_LOW_POWER);//ADVERTISE_MODE_LOW_LATENCY);
        settingsBuilder.setTimeout(0);
        return settingsBuilder.build();
    }

    /**
     * Custom callback after Advertising succeeds or fails to start.
     */
    private class SDDRAdvertiseCallback extends AdvertiseCallback {
        @Override
        public void onStartFailure(int errorCode) {
            super.onStartFailure(errorCode);
            if (errorCode == ADVERTISE_FAILED_DATA_TOO_LARGE) {
                Log.v(TAG, "Advertising failed: Data too large!");
            }
            else Log.v(TAG, "Advertising failed: Unknown " + errorCode);
        }

        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
            Log.v(TAG, "Advertising successfully started");
        }
    }
}
