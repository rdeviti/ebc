package org.mpisws.encounters.encounterformation;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.Pair;

import org.mpisws.helpers.Identifier;
import org.mpisws.encounters.encounterhistory.events.EncounterConfirmEvent;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.bluetooth.BluetoothGattCharacteristic.PERMISSION_READ;
import static android.bluetooth.BluetoothGattCharacteristic.PERMISSION_WRITE;
import static android.bluetooth.BluetoothGattCharacteristic.PROPERTY_READ;
import static android.bluetooth.BluetoothGattCharacteristic.PROPERTY_WRITE;
import static android.bluetooth.BluetoothGattService.SERVICE_TYPE_PRIMARY;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;

/**
 * Created by tslilyai on 1/16/18.
 */

public class GattServerClient extends BluetoothGattServerCallback {
    public static final int DHPUBKEY_LENGTH = 29;
    public static final UUID SERVICE_UUID = UUID
            .fromString("9fbc9d38-49fa-4063-bf59-b87b808ad211");
    public static final UUID CHARACTERISTIC_DHKEY_UUID = UUID
            .fromString("b3eede1a-5e06-4c85-a0e7-a844407befa2");
    public static final UUID CHARACTERISTIC_MESSAGE_UUID = UUID
            .fromString("82cbd8c1-fb1a-4c3d-86f5-bfe72367586e");
    private static final String TAG = GattServerClient.class.getSimpleName();
    private Map<String, BluetoothGatt> mActiveGatt; // Protected by synchronized(mActiveGatt)
    private Queue<Pair<BluetoothDevice, GattClientCallback>> mFutureGatt; // Protected by synchronized(mActiveGatt)
    private Queue<Pair<BluetoothDevice, GattClientCallbackForMessaging>> mFutureGattForMsging; // Protected by synchronized(mActiveGatt)
    private BluetoothGattServer mGattServer;
    private BluetoothManager btmanager;
    private Context mService;
    public static ConcurrentLinkedQueue<Pair<Identifier, Identifier>> receivedSDDRAddrsAndDHPubKeys = new ConcurrentLinkedQueue();
    public static ConcurrentLinkedQueue<EncounterConfirmEvent> confirmEvents;

    protected GattServerClient() {
        mActiveGatt = new HashMap<>();
        mFutureGatt = new ArrayDeque();
        mFutureGattForMsging = new ArrayDeque();
        confirmEvents = new ConcurrentLinkedQueue<>();
    };

    private static final int MAX_CONCURRENT_GATT = 7; // TODO: Find where this was specified in Android framework

    public void initialize(BluetoothManager btmanager, Context context) {
        mService = context;
        this.btmanager = btmanager;

        mGattServer = btmanager.openGattServer(mService, this);
        if (mGattServer != null) {
            BluetoothGattService service = new BluetoothGattService(SERVICE_UUID, SERVICE_TYPE_PRIMARY);
            BluetoothGattCharacteristic DHKeyChar = new BluetoothGattCharacteristic(CHARACTERISTIC_DHKEY_UUID, PROPERTY_READ | PROPERTY_WRITE, PERMISSION_READ | PERMISSION_WRITE);
            service.addCharacteristic(DHKeyChar);

            mGattServer.addService(service);
        } else {
            Log.e(TAG, "Failed to start the GATT server");
        }
    }

    @Override
    public void onConnectionStateChange(BluetoothDevice dev, int status, int newState) {
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            Log.v(TAG, "SERVER Connected to device " + dev.getAddress());
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            Log.v(TAG, "SERVER Disconnected from device " + dev.getAddress() + " (" + status + ")");
        }
    }

    public void connectToDevice(BluetoothDevice dev, Long pkid, Identifier advert) {
        String deviceAddress = dev.getAddress();
        Log.v(TAG, "BT address to connect: " + deviceAddress);
        synchronized (mActiveGatt) {
            GattClientCallback callback = new GattClientCallback(pkid, advert);
            if (mActiveGatt.size() < MAX_CONCURRENT_GATT) {
                mActiveGatt.put(deviceAddress, dev.connectGatt(mService, false, callback));
            } else {
                mFutureGatt.add(new Pair<>(dev, callback));
            }
        }
    }

    public void reset() {
        if (mGattServer != null)
            mGattServer.close();
        initialize(btmanager, mService);
        dropConnections();
    }

    public void dropConnections() {
        synchronized (mActiveGatt) {
            //for (BluetoothGatt gatt : mActiveGatt.values()) {
                //gatt.disconnect();
                //gatt.close();
            //}
            //mActiveGatt.clear();
            mFutureGatt.clear();
            mFutureGattForMsging.clear();
        }
    }

    public void sendMsgToDevice(BluetoothDevice dev, String msg) {
        String deviceAddress = dev.getAddress();
        Log.v(TAG, "BT address to msg : " + deviceAddress);
        synchronized (mActiveGatt) {
            GattClientCallbackForMessaging callback = new GattClientCallbackForMessaging(msg);
            if (mActiveGatt.size() < MAX_CONCURRENT_GATT) {
                mActiveGatt.put(deviceAddress, dev.connectGatt(mService, false, callback));
            } else {
                mFutureGattForMsging.add(new Pair<>(dev, callback));
            }
        }
    }

    private void onPeerDHKeyReceived(Identifier advert, Identifier peerDHPubKey, boolean checkAdvert) {
        Log.d(TAG, "Received DH key " + peerDHPubKey.toString());
        Identifier secretKeyID;
        if (checkAdvert) {
            secretKeyID = new Identifier(SDDR_Native.c_computeSecretKeyWithSHA(EncounterFormationCore.mDHKey.getBytes(), advert.getBytes(), peerDHPubKey.getBytes()));
        } else {
            secretKeyID = new Identifier(SDDR_Native.c_computeSecretKey(EncounterFormationCore.mDHKey.getBytes(), peerDHPubKey.getBytes()));
        }
        // TODO this is an update not an insert, but encounter might not exist yet
        confirmEvents.add(new EncounterConfirmEvent(EncounterFormationCore.mDHKey, advert, secretKeyID));
    }

    /* ACTING AS A SERVER */
    @Override
    public void onCharacteristicReadRequest(BluetoothDevice device,
                                            int requestId, int offset,
                                            BluetoothGattCharacteristic characteristic) {
        if (CHARACTERISTIC_DHKEY_UUID.equals(characteristic.getUuid())) {
            if (EncounterFormationCore.mDHPubKey != null) {
                Log.d(TAG, "Sending DH key " + EncounterFormationCore.mDHPubKey.toString() + " in response!");

                byte[] value = EncounterFormationCore.mDHPubKey.getBytes();
                mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, value);
            }
        }
    }

    @Override
    public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattCharacteristic characteristic,
                                             boolean preparedWrite, boolean responseNeeded,
                                             int offset, byte[] value) {
        if (CHARACTERISTIC_DHKEY_UUID.equals(characteristic.getUuid())) {
            Identifier peerDHPubKey = new Identifier(Arrays.copyOfRange(value, 0, DHPUBKEY_LENGTH));
            Log.d(TAG, "Someone wrote their key down for me :) " + device.getAddress() + " " + peerDHPubKey.toString());
            onPeerDHKeyReceived(null, peerDHPubKey, false);
        } else if (CHARACTERISTIC_MESSAGE_UUID.equals(characteristic.getUuid())) {
            Log.d(TAG, "Received message " + value.toString());
        }
        mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, null);
    }

    /* ACTING AS A CLIENT */
    // TODO: Should perform our actions from callbacks in a Handler thread potentially
    // (See slide 29 in https://droidcon.de/sites/global.droidcon.cod.newthinking.net/files/media/documents/practical_bluetooth_le_on_android_0.pdf)
    private class GattClientCallback extends BluetoothGattCallback implements Handler.Callback {
        private static final int MSG_DISCOVER_SERVICES = 0;
        private static final int MSG_READ_DHKEY = 1;
        private static final int MSG_WRITE_DONE = 2;
        private static final int MSG_CHANGE_MTU = 3;
        private long pkid;
        private Identifier advert;
        private Handler bleHandler;
        private byte[] DHKey;

        public GattClientCallback(long pkid, Identifier advert) {
            this.pkid = pkid;
            this.advert = advert;
            HandlerThread handlerThread = new HandlerThread("BLE-Worker");
            handlerThread.start();
            bleHandler = new Handler(handlerThread.getLooper(), this);
        }

        public void dispose() {
            bleHandler.removeCallbacksAndMessages(null);
            bleHandler.getLooper().quit();
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            BluetoothDevice dev = gatt.getDevice();

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.v(TAG, "Connected to device " + dev.getAddress());
                // Step 0: Discover the services to look for the SDDR service (wait for onServicesDiscovered)
                bleHandler.obtainMessage(MSG_DISCOVER_SERVICES, gatt).sendToTarget();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.v(TAG, "Disconnected from device " + dev.getAddress() + " (" + status + ")");
                close(gatt);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            // Step 1: Change the MTU so we can send everything
            bleHandler.obtainMessage(MSG_CHANGE_MTU, gatt).sendToTarget();
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            if (status != GATT_SUCCESS) {
                disconnect(gatt);
                Log.d(TAG, "Failed to change mtu");
                return;
            }
            super.onMtuChanged(gatt, mtu, status);
            Log.d(TAG, "Mtu changed to " + mtu);
            // Step 3: Perform the read
            bleHandler.obtainMessage(MSG_READ_DHKEY, gatt).sendToTarget();
        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status != GATT_SUCCESS) {
                disconnect(gatt);
                return;
            }

            // Step 4: Write our DHKEY value to the remote peer (wait for onCharacteristicWrite)
            bleHandler.post(() -> {
                byte[] data = characteristic.getValue();
                if (data != null) {
                    Identifier peerDHPubKey = new Identifier(Arrays.copyOf(data, DHPUBKEY_LENGTH));
                    onPeerDHKeyReceived(advert, peerDHPubKey, true);
                }

                this.DHKey = new byte[DHPUBKEY_LENGTH];
                System.arraycopy(EncounterFormationCore.mDHPubKey.getBytes(), 0, DHKey, 0, DHPUBKEY_LENGTH);
                Log.d(TAG, "Writing: " + DHKey.length + " " + new Identifier(DHKey).toString());

                characteristic.setValue(Arrays.copyOfRange(DHKey, 0, DHPUBKEY_LENGTH));
                if (gatt != null) gatt.writeCharacteristic(characteristic);
            });
       }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            // Step 5: Disconnect since we are done the protocol (wait for N/A)
            // (It is also possible the write failed, but we would disconnect anyway)
            Log.d(TAG, "Wrote all my DHKey!");
            bleHandler.obtainMessage(MSG_WRITE_DONE, gatt).sendToTarget();

        }

        private void disconnect(BluetoothGatt gatt) {
            BluetoothDevice dev = gatt.getDevice();
            Log.v(TAG, "Disconnecting from device " + dev.getAddress());
            gatt.disconnect();
        }

        private void close(BluetoothGatt gatt) {
            BluetoothDevice dev = gatt.getDevice();
            Log.v(TAG, "Closing device " + dev.getAddress());
            gatt.close();

            synchronized (mActiveGatt) {
                mActiveGatt.remove(dev.getAddress());
                if (mFutureGatt.size() > 0) {
                    Pair<BluetoothDevice, GattClientCallback> futureInfo = mFutureGatt.remove();
                    mActiveGatt.put(futureInfo.first.getAddress(), futureInfo.first.connectGatt(mService, false, futureInfo.second));
                }
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            BluetoothGatt gatt = (BluetoothGatt) msg.obj;
            switch (msg.what) {
                case MSG_DISCOVER_SERVICES:
                    gatt.discoverServices();
                    break;
                case MSG_CHANGE_MTU:
                    gatt.requestMtu(DHPUBKEY_LENGTH+10);
                    break;
                case MSG_READ_DHKEY:
                    for (BluetoothGattService service : gatt.getServices()) {
                        if (service.getUuid().compareTo(SERVICE_UUID) == 0) {
                            // Step 1: Read the DHKEY value from the remote peer (wait for onCharacteristicRead)
                            // TODO: Potentially need to wait until all devices report that services were discovered?
                            // (See bold text in 2nd answer of https://stackoverflow.com/questions/21237093/)
                            if (service.getCharacteristic(CHARACTERISTIC_DHKEY_UUID) == null) {
                                disconnect(gatt);
                            } else {
                                gatt.readCharacteristic(service.getCharacteristic(CHARACTERISTIC_DHKEY_UUID));
                            }
                        }
                    }
                    break;
                case MSG_WRITE_DONE:
                    disconnect(gatt);
            }
            return true;
        }
    }

    private class GattClientCallbackForMessaging extends BluetoothGattCallback implements Handler.Callback {
        private static final int MSG_DISCOVER_SERVICES = 0;
        private static final int MSG_SERVICES_DISCOVERED = 1;
        private static final int MSG_WRITE_DONE = 2;
        private Handler bleHandler;
        private String msg;

        public GattClientCallbackForMessaging(String msg) {
            HandlerThread handlerThread = new HandlerThread("BLE-Worker");
            handlerThread.start();
            bleHandler = new Handler(handlerThread.getLooper(), this);
            this.msg = msg;
        }

        public void dispose() {
            bleHandler.removeCallbacksAndMessages(null);
            bleHandler.getLooper().quit();
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            BluetoothDevice dev = gatt.getDevice();

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.v(TAG, "Connected to device " + dev.getAddress());
                // Step -1: Discover the services to look for the SDDR service (wait for onServicesDiscovered)
                bleHandler.post(() -> {
                    if (gatt != null)
                        gatt.requestMtu(msg.length() + 10);
                });
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.v(TAG, "Disconnected from device " + dev.getAddress() + " (" + status + ")");
                close(gatt);
            }
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int newState) {
            super.onMtuChanged(gatt, mtu, newState);
            Log.d(TAG, "Mtu changed to " + mtu + " " + newState);
            if (newState != STATE_CONNECTED) {
                Log.d(TAG, "UHOH");
            }
            // Step 0: Discover the services to look for the SDDR service (wait for onServicesDiscovered)
            bleHandler.obtainMessage(MSG_DISCOVER_SERVICES, gatt).sendToTarget();
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            bleHandler.obtainMessage(MSG_SERVICES_DISCOVERED, gatt).sendToTarget();
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            // Step 3: Disconnect since we are done the protocol (wait for N/A)
            // (It is also possible the write failed, but we would disconnect anyway)
            Log.d(TAG, "Wrote my message!");
            bleHandler.obtainMessage(MSG_WRITE_DONE, gatt).sendToTarget();
        }

        private void disconnect(BluetoothGatt gatt) {
            if (gatt != null) {
                BluetoothDevice dev = gatt.getDevice();
                Log.v(TAG, "Disconnecting from device " + dev.getAddress());
                gatt.disconnect();
            }
        }

        private void close(BluetoothGatt gatt) {
            if (gatt != null) {
                BluetoothDevice dev = gatt.getDevice();
                Log.v(TAG, "Closing device " + dev.getAddress());
                gatt.close();

                synchronized (mActiveGatt) {
                    mActiveGatt.remove(dev.getAddress());
                    if (mFutureGattForMsging.size() > 0) {
                        Pair<BluetoothDevice, GattClientCallbackForMessaging> futureInfo = mFutureGattForMsging.remove();
                        mActiveGatt.put(futureInfo.first.getAddress(), futureInfo.first.connectGatt(mService, false, futureInfo.second));
                    }
                }
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            BluetoothGatt gatt = (BluetoothGatt) msg.obj;
            if (gatt == null) {
                return true;
            }
            switch (msg.what) {
                case MSG_DISCOVER_SERVICES:
                    gatt.discoverServices();
                    break;
                case MSG_SERVICES_DISCOVERED:
                    for (BluetoothGattService service : gatt.getServices()) {
                        if (service.getUuid().compareTo(SERVICE_UUID) == 0) {
                            // Step 1: Read the DHKEY value from the remote peer (wait for onCharacteristicRead)
                            // TODO: Potentially need to wait until all devices report that services were discovered?
                            // (See bold text in 2nd answer of https://stackoverflow.com/questions/21237093/)
                            if (service.getCharacteristic(CHARACTERISTIC_MESSAGE_UUID) == null) {
                                disconnect(gatt);
                            } else {
                                BluetoothGattCharacteristic c = service.getCharacteristic(CHARACTERISTIC_MESSAGE_UUID);
                                c.setValue(this.msg);
                                gatt.writeCharacteristic(c);
                            }
                        }
                    }
                    break;
                case MSG_WRITE_DONE:
                    disconnect(gatt);
            }
            return true;
        }
    }
}
