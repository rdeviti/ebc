package org.mpisws.encounters.encounterformation;

import java.util.ArrayList;

/**
 * Created by tslilyai on 10/29/17.
 */

/**
 * SDDR_Native exposes the interface of the
 * C++ JNI code used by SDDR_Core and the Scanner class.
 */
public class SDDR_Native {
    static { System.loadLibrary("c_SDDRRadio"); }
    static public native void c_mallocRadio();
    static public native void c_freeRadio();
    static public native byte[] c_getMyAdvert();
    static public native void c_changeEpoch();
    static public native long c_processScanResult(int rssi, byte[] advert, long time);
    static public native void c_preDiscovery();
    static public native void c_postDiscovery();
    static public native byte[] c_getAdvertDHKey();
    static public native byte[] c_getAdvertDHPubKey();
    static public native byte[] c_computeSecretKeyWithSHA(byte[] myDHKey, byte[] sha1OtherDHKey, byte[] otherDHKey);
    static public native byte[] c_computeSecretKey(byte[] myDHKey, byte[] otherDHKey);
    static public native void c_changeMessagingDHKey();
    static public native byte[] c_getNewMessagingDHKey();
    static public native byte[] c_getNewMessagingDHPubKey();
    static public native int c_sendToClient(String data);

    static public long c_RadioPtr;
    public static ArrayList<byte[]> c_EncounterMsgs = new ArrayList<>();
}