package org.mpisws.encounters.encounterformation;

/**
 * Created by tslilyai on 10/18/17.
 *
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;
import android.os.ParcelUuid;
import android.os.SystemClock;
import android.util.Log;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.Utils;
import org.mpisws.encounters.encounterhistory.events.EncounterEndedEvent;
import org.mpisws.encounters.encounterhistory.events.EncounterEvent;
import org.mpisws.encounters.encounterhistory.events.EncounterStartedEvent;
import org.mpisws.encounters.encounterhistory.events.RSSIEntry;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_POWER;
import static org.mpisws.encounters.EncounterBasedCommunication.SCAN_BATCH_INTERVAL;
import static org.mpisws.encounters.encounterformation.Advertiser.NONCE_LENGTH_IN_ADVERT;
import static org.mpisws.encounters.encounterformation.Advertiser.PUUID_LENGTH;
import static org.mpisws.encounters.encounterformation.Advertiser.SHA1_LENGTH;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mDHKey;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mDHPubKey;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.mNonce;
import static org.mpisws.encounters.encounterformation.EncounterFormationCore.ongoingEncounters;

/**
 * Scans for Bluetooth Low Energy Advertisements matching a filter and displays them to the user.
 */
public class ScannerProcessor {
    private static final String TAG = ScannerProcessor.class.getSimpleName();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanCallback mScanCallback;
    private EncounterFormationCore core;
    private boolean activeConnections;
    private Handler handler;
    private Context context;

    public ScannerProcessor() {
        this.activeConnections = false;
        this.handler = new Handler();
    }

    public void initialize(BluetoothAdapter btAdapter, EncounterFormationCore core) {
        this.mBluetoothAdapter = btAdapter;
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        this.core = core;
        this.context = core.mService;
    }

    public void startScanningActive(long activeScanInterval) {
        // stop everything
        stopScanning();
        core.getGattServerClient().dropConnections();

        // set things up for this active scan
        SDDR_Native.c_preDiscovery();
        activeConnections = true;
        startScanning();

        // this determines how long the active connections / scanning will go on for
        handler.postDelayed(() -> {
            activeConnections = false;
            stopScanning();

            // process all encounter stuff
            SDDR_Native.c_postDiscovery();
            processEncounters();
            core.postScanProcessing();

            // start normal batch scanning again
            startScanning();
        }, activeScanInterval);
    }

    /**
     * Start scanning for BLE Advertisements (and stop after a set period of time).
     */
    public void startScanning() {
        if (mScanCallback == null) {
            mScanCallback = new SDDRScanCallback();
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
        } else {
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
        }
        Log.v(TAG, "Starting Scanning on thread " + Thread.currentThread().getName());
    }

    /**
     * Stop scanning for BLE Advertisements.
     */
    public void stopScanning() {
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    /**
     * Filter our scans so we only discover SDDR_API devices
     */
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();
        return scanFilters;
    }

    /**
     * Return a {@link ScanSettings} object (default settings for now)
     */
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(SCAN_MODE_LOW_POWER);
        if (!activeConnections)
            builder.setReportDelay(SCAN_BATCH_INTERVAL);
        return builder.build();
    }

    /**
     * Custom ScanCallback object. Calls the native function to process discoveries for encounters.
     */
    private class SDDRScanCallback extends ScanCallback {
        private void processResult(ScanResult result) {
            String addr = result.getDevice().getAddress();
            ScanRecord record = result.getScanRecord();
            if (record == null) {
                Log.v(TAG, "No scan record");
                return;
            } else {
                Map<ParcelUuid, byte[]> datamap = record.getServiceData();
                if (datamap == null) {
                    return;
                }
                for (ParcelUuid pu : datamap.keySet()) {
                    ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
                    bb.putLong(pu.getUuid().getMostSignificantBits());
                    bb.putLong(pu.getUuid().getLeastSignificantBits());
                    // the puuid
                    byte[] datahead = bb.array();

                    byte[] datatail = datamap.get(pu);
                    Utils.myAssert(datahead.length == PUUID_LENGTH);
                    if (datatail.length < NONCE_LENGTH_IN_ADVERT) {
                        Log.d(TAG, "Not SDDR---wrong advert length " + new Identifier(datatail).toString());
                        continue;
                    }

                    // get the new advertised nonce
                    byte[] newnonce = new byte[SHA1_LENGTH];
                    // get part of new nonce from PUUID
                    System.arraycopy(Arrays.copyOfRange(datahead, 0, PUUID_LENGTH), 0, newnonce, 0, PUUID_LENGTH);
                    System.arraycopy(datatail, 0, newnonce, PUUID_LENGTH, NONCE_LENGTH_IN_ADVERT);
                    // get the rssi
                    int rssi = result.getRssi();
                    // process the scan result. if this is a new device, pkid is not null
                    long pkid = SDDR_Native.c_processScanResult(rssi, newnonce,
                            System.currentTimeMillis() -
                                    SystemClock.elapsedRealtime() +
                                    result.getTimestampNanos() / 1000000);

                    Log.d(TAG, "Processing SDDR_API scanresult with data " + Utils.getHexString(datahead) + Utils.getHexString(datatail) + ":\n"
                            + "newNonce " + Utils.getHexString(newnonce) + ", "
                            + " devAddr " + addr);

                    if (activeConnections) {
                        // TODO keep list of devices connected in this epoch
                        core.getGattServerClient().connectToDevice(result.getDevice(), pkid, new Identifier(newnonce));
                    }
                }
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.v(TAG, results.size() + " Batch Results Found on thread " + Thread.currentThread().getName());

            SDDR_Native.c_preDiscovery();
            for (ScanResult result : results) {
                processResult(result);
            }
            SDDR_Native.c_postDiscovery();
            processEncounters();
            core.postScanProcessing();
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            processResult(result);
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.v(TAG, "Scanning failed: " + errorCode);
        }
    }

    private void processEncounters() {
        List<Long> pkids = new ArrayList<>();
        for (Iterator<byte[]> iterator = SDDR_Native.c_EncounterMsgs.iterator(); iterator.hasNext(); ) {
            byte[] msg = iterator.next();
            final SDDR_Proto.Event event;

            try {
                event = SDDR_Proto.Event.parseFrom(msg);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                return;
            }

            Utils.myAssert(event.hasEncounterEvent());
            final SDDR_Proto.Event.EncounterEvent subEvent = event.getEncounterEvent();
            final SDDR_Proto.Event.EncounterEvent.EventType type = subEvent.getType();
            final long time = subEvent.getTime();
            final long pkid = subEvent.getPkid();
            pkids.add(pkid);
            final String address = subEvent.getAddress();
            final List<SDDR_Proto.Event.EncounterEvent.RSSIEvent> rssiEventsPB = subEvent.getRssiEventsList();
            final List<ByteString> advertsPB = subEvent.getSharedSecretsList();

            // Transforming the lists into the appropriate Java structures for EncounterEvent
            final List<RSSIEntry> rssiEvents = new LinkedList<>();
            for (SDDR_Proto.Event.EncounterEvent.RSSIEvent rssiEvent : rssiEventsPB) {
                rssiEvents.add(new RSSIEntry(rssiEvent.getTime(), rssiEvent.getRssi()));
            }

            Identifier receivedNonce = null;
            // we don't have to track devices' adverts if we're trying to connect to them now
            if (advertsPB.size() > 0) {
                // TODO hack there really is only ever one nonce per encounter
                receivedNonce = new Identifier(advertsPB.get(0).toByteArray());
            }

            EncounterEvent encEvent;
            switch (type) {
                case UnconfirmedStart:
                    encEvent = new EncounterStartedEvent(pkid, time, receivedNonce, mNonce, mDHPubKey, mDHKey);
                    Log.v(TAG, "[EncounterEvent] Tentative encounter started at " + time);
                    ongoingEncounters.put(receivedNonce.toString(), new ImmutablePair<>(pkid, time));
                    break;
                case End:
                    encEvent = new EncounterEndedEvent(pkid, time, rssiEvents);
                    Log.v(TAG, "[EncounterEvent] Encounter Ended at " + time);
                    ongoingEncounters.remove(receivedNonce.toString());
                    break;
                default:
                    encEvent = null;
            }

            Log.v(TAG, "\tPKID = " + pkid + ", Address = " + address);

            if (encEvent != null) encEvent.broadcast(core.mService);
            iterator.remove();
        }
        EncounterEvent.insertLocationForEncounters(core.mService);

    }
}
