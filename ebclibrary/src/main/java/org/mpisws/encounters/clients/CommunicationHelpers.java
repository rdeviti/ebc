package org.mpisws.encounters.clients;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.encounters.encounterformation.SDDR_Native;
import org.mpisws.encounters.encounterhistory.PPrivateMessagingChannels;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;
import org.mpisws.encounters.encounterhistory.bridges.InitiateMessagingChannelsBridge;
import org.mpisws.encounters.encounterhistory.bridges.MessagingChannelsBridge;
import org.mpisws.encounters.encounterhistory.models.MInitiatePrivateMessagingChannel;
import org.mpisws.encounters.encounterhistory.models.MPrivateMessagingChannel;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Identifier;
import org.mpisws.messaging.EncounterForwardingMessage;
import org.mpisws.messaging.InitiatePrivateMessagingMessage;
import org.mpisws.messaging.PrivateMessage;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.util.ArrayList;
import java.util.List;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;
import static org.mpisws.helpers.Utils.hexStringToByteArray;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.NONE;

public class CommunicationHelpers {
    private static final String TAG = CommunicationHelpers.class.getSimpleName();

    public static String createNewPublicKeyTopic(Context context) {
        /*
         * (1) create a new private / public DH key pair
         */
        SDDR_Native.c_changeMessagingDHKey();
        Identifier publicKey = new Identifier(SDDR_Native.c_getNewMessagingDHPubKey());
        Identifier privateKey = new Identifier(SDDR_Native.c_getNewMessagingDHKey());

        /*
         * (2) create a topic with the public key as the title. this is where people will post
         */
        String replyTopicHandle = ESClient.getInstance().createOneTopic(new ImmutablePair<>(publicKey, publicKey));
        /*
         * (3) store this public / private key pair in the database
         */
        if (replyTopicHandle != null) {
            new InitiateMessagingChannelsBridge(context).insertEntry(privateKey, publicKey, replyTopicHandle);
        }
        Log.d(TAG, "Created topic for new public key " + publicKey.toString()
        + "\nwith private dh key " + privateKey.toString());
        return replyTopicHandle;
    }

    public static void postReplyToPrivateMessage(
            String replyMessageText,
            ReceivedMessageWrapper receivedMessageWrapper,
            Context context) {
        MessagingChannelsBridge messagingChannelsBridge = new MessagingChannelsBridge(context);
        MPrivateMessagingChannel mPrivateMessagingChannel = messagingChannelsBridge.getEntryFromTopicHandle(receivedMessageWrapper.getTopicHandle());
        getMessagingChannelTopicHandleAndPostReply(replyMessageText, mPrivateMessagingChannel, messagingChannelsBridge);
    }

    public static void createKeyAndPostReplyToEncounterForwardingMessage (
            String replyMessageText,
            EncounterForwardingMessage encounterForwardingMessage,
            Context context)
    {
        MessagingChannelsBridge messagingChannelsBridge = new MessagingChannelsBridge(context);
        String otherPubKey = ESClient.getInstance().getOneTopicTitle(encounterForwardingMessage.getReplyTopicHandle());

        if (otherPubKey == null) {
            Log.d(TAG, "Couldn't find public key topic");
            return;
        }

        // see if we've replied to this public key before; if so, follow the reply procedure
        MPrivateMessagingChannel mPrivateMessagingChannel = messagingChannelsBridge.getEntryFromOtherPubKey(otherPubKey);
        if (mPrivateMessagingChannel != null) {
            getMessagingChannelTopicHandleAndPostReply(
                    replyMessageText,
                    mPrivateMessagingChannel,
                    messagingChannelsBridge);
            return;
        }

        // we never replied to this public key before. create a new public/private key pair
        if (encounterForwardingMessage.getReplyTopicHandle().compareTo("") == 0) {
            Log.d(TAG, "Tried to reply to EF message with replies disabled");
            return;
        }

        // create priv-pub key for this
        SDDR_Native.c_changeMessagingDHKey();
        Identifier myPublicKey = new Identifier(SDDR_Native.c_getNewMessagingDHPubKey());
        Identifier myPrivateKey = new Identifier(SDDR_Native.c_getNewMessagingDHKey());

        // compute secret key
        byte[] secret = SDDR_Native.c_computeSecretKey(myPrivateKey.getBytes(), hexStringToByteArray(otherPubKey));
        if (secret == null) return;
        Identifier secretId = new Identifier(secret);
        Log.d(TAG, "Creating private messaging channel with shared secret " + secretId.toString());
        Identifier channelName = Coder.convertSharedSecretToID(secretId);

        // create my side of the channel
        String myTopicHandle = ESClient.getInstance().createOneTopic(new ImmutablePair<>(channelName, channelName));

        // quit if we failed :(
        if (myTopicHandle == null) {
            Log.d(TAG, "Failed to create topic to reply to EF Message with pub key " + otherPubKey);
            return;
        }

        // if success, save everything in the database (we don't have the other topic handle yet, that's fine)
        messagingChannelsBridge.insertEntry(new Identifier(hexStringToByteArray(otherPubKey)),
                secretId, channelName, myTopicHandle, null);

        // post the initial message and our message on our channel
        String encryptedMessageTextOriginal = new PrivateMessage.MessageChannelMessageBuilder()
                .addMessage(encounterForwardingMessage.getMsgText())
                .build()
                .toSendMessageText(secret);

        // post our message on our channel
        String encryptedMessageTextReply = new PrivateMessage.MessageChannelMessageBuilder()
                .addMessage(replyMessageText)
                .build()
                .toSendMessageText(secret);
        List<ESMessage> messagesToSend = new ArrayList<>(2);
        messagesToSend.add(new ESMessage(encryptedMessageTextOriginal, channelName.toString(), myTopicHandle, true, null, true, -1));
        messagesToSend.add(new ESMessage(encryptedMessageTextReply, channelName.toString(), myTopicHandle, true, null, true, -1));

        ESClient.getInstance().sendMsgs(messagesToSend);

        // post our public key on their channel (note that we need to do this last, since
        // they might query for our message as soon as they receive this post)
        String publicKeyText = new InitiatePrivateMessagingMessage.InitiateMessageChannelMessageBuilder()
                .addPublicKey(myPublicKey)
                .build()
                .toSendMessageText();
        Log.d(TAG, "Posting initiate encounter forwarding message " + publicKeyText);
        ESClient.getInstance().sendOneMessage(publicKeyText, encounterForwardingMessage.getReplyTopicHandle());
    }

    public static void getMessagingChannelTopicHandleAndPostReply(
            String replyText,
            MPrivateMessagingChannel mPrivateMessagingChannel,
            MessagingChannelsBridge messagingChannelsBridge)
    {
        if (mPrivateMessagingChannel == null) return;

        // get the other topic handle if it exists
        String otherTopicHandle = mPrivateMessagingChannel.getOtherTopicHandle();
        if (otherTopicHandle == null || otherTopicHandle.compareTo("") ==0) {
            otherTopicHandle = ESClient.getInstance().getOneTopicHandle(new Identifier(mPrivateMessagingChannel.getChannelName()));
        }
        if (otherTopicHandle != null) {
            // if found, update our database entry
            ContentValues values = new ContentValues();
            values.put(PPrivateMessagingChannels.Columns.otherTopicHandle, otherTopicHandle);
            messagingChannelsBridge.updateFromContentValues(mPrivateMessagingChannel.getPKID(), values);
        }

        String encryptedPrivateMessageText = new PrivateMessage.MessageChannelMessageBuilder()
                .addMessage(replyText)
                .build()
                .toSendMessageText(mPrivateMessagingChannel.getSharedSecret());
        Log.d(TAG, "Posting reply to private message " + replyText + ", encrypted " + encryptedPrivateMessageText);

        // post our message on their channel if we found the topic handle; if not, post on our channel
        // (they'll have to get the whole channel messages to receive this one)
        ESClient.getInstance().sendOneMessage(encryptedPrivateMessageText,
                otherTopicHandle == null ? mPrivateMessagingChannel.getMyTopicHandle() : otherTopicHandle);
    }

    public static List<ReceivedMessageWrapper> createPrivateChannelAndGetReply(ReceivedMessageWrapper msgWrapper, Context context) {
        // (1) create shared secret
        InitiatePrivateMessagingMessage initiateMessageChannelMessage = msgWrapper.getInitiateMessageChannelMessage();

        MInitiatePrivateMessagingChannel mInitiatePrivateMessagingChannel =
                new InitiateMessagingChannelsBridge(context).getEntryWithTopicHandle(msgWrapper.getTopicHandle());
        byte[] dhKey = mInitiatePrivateMessagingChannel.getMyDHPrivKey();
        byte[] secret = SDDR_Native.c_computeSecretKey(dhKey, initiateMessageChannelMessage.getPublicKey().getBytes());
        if (secret == null) return null;
        Identifier secretId = new Identifier(secret);
        Log.d(TAG, "Creating private messaging channel with shared secret " + secretId.toString());
        Identifier channelName = Coder.convertSharedSecretToID(secretId);

        // (2) create my part of the communication channel
        String myTopicHandle = ESClient.getInstance().createOneTopic(new ImmutablePair<>(channelName, channelName));

        // (3) query for their part of the communication channel
        String otherTopicHandle = ESClient.getInstance().getOneTopicHandle(channelName);

        // just skip this if we failed to actually create a topic or if their channel doesn't exist
        if (myTopicHandle == null || otherTopicHandle == null) {
            return null;
        }

        // (4) insert into database
        new MessagingChannelsBridge(context).insertEntry(new Identifier(mInitiatePrivateMessagingChannel.getMyDHPubKey()),
                secretId, channelName, myTopicHandle, otherTopicHandle);

        // (5) get the message they posted for us to read (on their channel)
        List<ESMessage> postedMessages = ESClient.getInstance().getMessagesOnTopic(otherTopicHandle);
        // because we don't set the topic title when we retrieve messages using the topic handle..
        for (ESMessage msg : postedMessages) {
            msg.setTopicText(channelName.toString());
        }
        List<ReceivedMessageWrapper> receivedMessages = new ArrayList<>();
        if (postedMessages.size() >= 1) {
            for (ESMessage msg : postedMessages) {
                Log.d(TAG, "Got reply message on channel " + channelName.toString() + ": " + msg.getMsgText());
                // HACK: we need to simulate that the message was posted on our topic so we reply to the
                // right channel
                msg.setTopicHandle(myTopicHandle);
                receivedMessages.add(new ReceivedMessageWrapper(msg, secretId.getBytes()));
            }
            return receivedMessages;
        } else {
            return null;
        }
    }


    public static ImmutablePair<String, List<ReceivedMessageWrapper>> getUnreadMsgsPage(String cursor, Context context) {
        Pair<String, List<ESMessage>> cursorAndMsgs = ESClient.getInstance().getUnreadPageMessages(cursor, PAGE_SIZE);
        List receivedMessageWrappers = new ArrayList(cursorAndMsgs.getRight().size());
        for (ESMessage msg : cursorAndMsgs.getRight()) {
            if (msg == null || msg.getMsgText() == null) {
                continue;
            }
            // try to get secret from either the encounters or private messaging channels
            byte[] secret = new EncounterEntriesBridge(context).getSharedSecretByTopicHandle(msg.getTopicHandle());
            Log.d(TAG, "Found EEB secret with topic handle " + msg.getTopicHandle() +  ", "
                    + ((secret == null) ? "" : new Identifier(secret).toString()));
            if (secret == null) {
                secret = new MessagingChannelsBridge(context).getSharedSecretByTopicHandle(msg.getTopicHandle());
                Log.d(TAG, "Found EEB secret with topic handle " + msg.getTopicHandle() +  ", "
                    + ((secret == null) ? "" : new Identifier(secret).toString()));
            }
            if (msg.getMsgText() != null && (secret != null || msg.getMsgText().contains(InitiatePrivateMessagingMessage.FLAG))) {
                ReceivedMessageWrapper receivedMessageWrapper = new ReceivedMessageWrapper(msg, secret);
                if (receivedMessageWrapper.getMsgType() == NONE)
                    continue;
                receivedMessageWrappers.add(receivedMessageWrapper);
            }
        }
        return new ImmutablePair<String, List<ReceivedMessageWrapper>>(cursorAndMsgs.getLeft(), receivedMessageWrappers);
    }

    public static ImmutablePair<String, List<ReceivedMessageWrapper>> getMsgsPage(String cursor, Context context) {
        Pair<String, List<ESMessage>> cursorAndMsgs = ESClient.getInstance().getPageMessages(cursor, PAGE_SIZE);
        List receivedMessageWrappers = new ArrayList(cursorAndMsgs.getRight().size());
        for (ESMessage msg : cursorAndMsgs.getRight()) {
            if (msg == null || msg.getMsgText() == null) {
                continue;
            }
            // try to get secret from either the encounters or private messaging channels
            byte[] secret = new EncounterEntriesBridge(context).getSharedSecretByTopicHandle(msg.getTopicHandle());
            Log.d(TAG, "Found EEB secret with topic handle " + msg.getTopicHandle() +  ", "
                    + ((secret == null) ? "" : new Identifier(secret).toString()));
            if (secret == null) {
                secret = new MessagingChannelsBridge(context).getSharedSecretByTopicHandle(msg.getTopicHandle());
                Log.d(TAG, "Found EEB secret with topic handle " + msg.getTopicHandle() +  ", "
                    + ((secret == null) ? "" : new Identifier(secret).toString()));
            }
            if (msg.getMsgText() != null && (secret != null || msg.getMsgText().contains(InitiatePrivateMessagingMessage.FLAG))) {
                ReceivedMessageWrapper receivedMessageWrapper = new ReceivedMessageWrapper(msg, secret);
                if (receivedMessageWrapper.getMsgType() == NONE)
                    continue;
                receivedMessageWrappers.add(receivedMessageWrapper);
            }
        }
        return new ImmutablePair<String, List<ReceivedMessageWrapper>>(cursorAndMsgs.getLeft(), receivedMessageWrappers);
    }

}
