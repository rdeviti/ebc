package org.mpisws.encounters.clients;

import android.util.Log;

import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.encounters.lib.Preferences;

import static org.mpisws.encounters.EncounterBasedCommunication.IUserAccountClient;
import static org.mpisws.encounters.EncounterBasedCommunication.isSignedIn;

/**
 * Created by tslilyai on 2/17/18.
 */

public class UserAccountClient implements IUserAccountClient {
    private static final String TAG = UserAccountClient.class.getSimpleName();
    private String ebCKey;

    public UserAccountClient(){
        this.ebCKey = Preferences.getInstance().getEbCKey();
    }

    public boolean loginAccountWithGoogle(String googletoken) {
        ESClient.getInstance().registerGoogleUser(googletoken);

        // try to sign into ES
        Preferences preferences = GlobalObjectRegistry.getObject(Preferences.class);
        if (isSignedIn()) { Log.d(TAG, "Is already signed in!"); return true;}
        ESClient.getInstance().setESCredentials(preferences.getUserHandle(), preferences.getAuthorizationToken());
        if (!isSignedIn()) {
            ESClient.getInstance().signIn();
        }

        // set ES credentials
        String userhandle = ESClient.getInstance().getCredentials().getUserHandle();
        String auth = ESClient.getInstance().getCredentials().getAuth();
        Log.d(TAG, "Storing user handle, auth: " + userhandle + ", " + auth);
        preferences.setUserHandle(userhandle);
        preferences.setAuthorizationToken(auth);

        // set EbC per-user key (for encryption)
        // TODO for right now just based on ES credentials
        if (ebCKey == null) {
            ebCKey = userhandle+auth;
            Preferences.getInstance().setEbCKey(ebCKey);
        }
        return isSignedIn();
    }

    public void signOut() {
        if (!isSignedIn()) return;
        ESClient.getInstance().signOut();
        Preferences.getInstance().setEbCKey(null);
        Preferences.getInstance().setUserHandle(null);
        Preferences.getInstance().setAuthorizationToken(null);
        // TODO sign out agent here too?
    }

    public void deleteAccount() {
        if (!isSignedIn()) return;
        ESClient.getInstance().deleteAccount();
        Preferences.getInstance().setEbCKey(null);
        Preferences.getInstance().setAuthorizationToken(null);
        Preferences.getInstance().setUserHandle(null);
        // TODO delete agent client account?
    }
}
