package org.mpisws.encounters.clients;

import android.util.Log;

import org.mpisws.encounters.encounterformation.SDDR_Native;
import org.mpisws.encounters.lib.Preferences;
import org.mpisws.messaging.EncounterForwardingMessage;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

/**
 * Created by tslilyai on 2/17/18.
 */

public class CloudAgentHelpers {
    final static String TAG = CloudAgentHelpers.class.getSimpleName();
    final static String AUTHDELIMITER = "AUTHDELIMITER";
    final static String ENCDELIMITER = "ENCDELIMITER";
    final static String LISTDELIMITER =";";
    final static String MESSAGEDELIMITER ="MESSAGEDELIMITER";
    public final static String COMPONENTDELIMITER = ",";

    public static void main(String[] args) {
        List<String> secretStrs = new ArrayList<>();
        List<String> locStrs = new ArrayList<>();
        secretStrs.add("HELLO");
        locStrs.add("WORLD");
        issueUploadNewEncountersRequest(secretStrs,locStrs);
        exit(0);
    }

    public static int issueSetCredentialsRequest(String userHandle, String auth) {
        String str = String.format("%s%s%s", userHandle, AUTHDELIMITER, auth);
        Log.d(TAG, "Uploading auth: " + str);
        return SDDR_Native.c_sendToClient(str);
    }

    public static int issueUploadNewEncountersRequest(
            List<String> secretStrs,
            List<String> locationStrs) {
        String str = String.format("%s%s%s%s%s",
                Preferences.getInstance().getUserHandle(),
                ENCDELIMITER,
                secretStrs == null? "" : String.join(LISTDELIMITER, secretStrs),
                ENCDELIMITER,
                locationStrs == null? "" : String.join(LISTDELIMITER, locationStrs));
        Log.d(TAG, "Uploading data: " + str);
        return SDDR_Native.c_sendToClient(str);
    }

    public static int sendEncounterForwardingMessage(EncounterForwardingMessage efm) {
        StringBuilder s = new StringBuilder();
        s.append(Preferences.getInstance().getUserHandle()).append(MESSAGEDELIMITER);
        s.append(efm.getFwdingConstraint().getHopCount()).append(MESSAGEDELIMITER);
        s.append(efm.getFwdingConstraint().getFanoutLimit()).append(MESSAGEDELIMITER);
        s.append(efm.getFwdingConstraint().getExpirationTime()).append(MESSAGEDELIMITER);
        s.append(efm.getEncounterConstraint().useCausalityConstraint()).append(MESSAGEDELIMITER);
        s.append(efm.getEncounterConstraint().duration).append(MESSAGEDELIMITER);
        s.append(efm.getEncounterConstraint().toQueryString()).append(MESSAGEDELIMITER);
        s.append(efm.getMsgText());
        Log.d(TAG, "Issuing efc: " + s.toString());
        return SDDR_Native.c_sendToClient(s.toString());
    }
}
