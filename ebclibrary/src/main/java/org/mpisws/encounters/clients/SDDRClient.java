package org.mpisws.encounters.clients;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.database.DBEncounterInfo;
import org.mpisws.encounters.encounterhistory.bridges.EncounterEntriesBridge;
import org.mpisws.encounters.lib.time.TimeInterval;
import org.mpisws.helpers.ThreadPoolManager;
import org.mpisws.helpers.Utils;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.encounters.EncounterBasedCommunication;
import org.mpisws.encounters.encounterformation.EncounterFormationService;
import org.mpisws.encounters.encounterhistory.bridges.LocationBridge;
import org.mpisws.encounters.encounterhistory.models.MLocation;
import org.mpisws.encounters.encounterhistory.models.MEncounterEntry;
import org.mpisws.encounters.lib.Preferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tslilyai on 11/6/17.
 */

public class SDDRClient implements EncounterBasedCommunication.ISDDRClient {
    private static final String TAG = SDDRClient.class.getSimpleName();
    private Context context;

    public SDDRClient(Context context){
        this.context = context;
    }

    @Override
    public void startFormingEncounters() {
        Intent serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("@string.start_sddr_service", 0);
        context.startService(serviceIntent);
        Log.d(TAG, "Started encounters service");
    }

    @Override
    public void stopFormingEncounters() {
        Intent serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("stop_sddr_service", 0);
        context.startService(serviceIntent);
        Log.d(TAG, "Stopped encounters service");
    }

    @Override
    public void confirmAndCommunicateUsingNetwork() {
        Intent serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_active", false);
        context.startService(serviceIntent);

        serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_es", true);
        context.startService(serviceIntent);
    }

    @Override
    public void confirmAndCommunicateUsingBLE() {
        Intent serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_active", true);
        context.startService(serviceIntent);

        serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_es", false);
        context.startService(serviceIntent);
    }

    @Override
    public void disableConfirmation() {
         Intent serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_active", false);
        context.startService(serviceIntent);

        serviceIntent = new Intent(context, EncounterFormationService.class);
        serviceIntent.putExtra("confirmation_es", false);
        context.startService(serviceIntent);
    }

    @Override
    public Pair<List<MLocation>, TimeInterval> getEncounterInfoFromEID(String encounterID) {
        return new EncounterEntriesBridge(context).getEIDLocsAndTimeOfEID(encounterID);
    }

    @Override
    public List<DBEncounterInfo> getEncountersWithConstraint(EncounterQueryConstraint constraint) {
        if (constraint == null) { return null; }
        return new EncounterEntriesBridge(context).getEncounterInfosFromQuery(constraint.toQueryString());
    }

    @Override
    public void updateDatabaseOnAgent() {
        ThreadPoolManager.getInstance().runTask(() ->
        {
            Log.d(TAG, "Issuing credentials");
            int ret = CloudAgentHelpers.issueSetCredentialsRequest(Preferences.getInstance().getUserHandle(), Preferences.getInstance().getAuthorizationToken());
            Log.d(TAG, "Issued Credentials: " + ret);

            // get all locations and encounters that have completed past the given time stamp (as well as ongoing encounters)
            List<MLocation> locations = new LocationBridge(context).getLocationsSinceTime(Preferences.getInstance().getDBTimestamp());
            List<MEncounterEntry> encounterEntries = new EncounterEntriesBridge(context).getConfirmedEntriesAfterTime(
                    Preferences.getInstance().getDBTimestamp()
            );

            if (encounterEntries.size() == 0) {
                Log.d(TAG, "No encounters to upload");
                return;
            }
            List<String> locationStrs = new ArrayList<>();
            List<String> entryStrs = new ArrayList<>();
            for (MLocation location : locations) {
                locationStrs.add(location.valsToString());
            }
            for (MEncounterEntry entry : encounterEntries) {
                entryStrs.add(entry.valsToString());
            }
            List<List<String>> locBatches = Utils.getBatches(locationStrs);
            List<List<String>> entryBatches = Utils.getBatches(entryStrs);
            Log.d(TAG, "Uploading " + locBatches.size() + " locBatches and " + entryBatches.size() + " entryBatches");
            int lastEntryBatch = 0, lastLocBatch = 0;
            while (lastEntryBatch < entryBatches.size()) {
                if (lastEntryBatch < locBatches.size()) {
                    ret = CloudAgentHelpers.issueUploadNewEncountersRequest(entryBatches.get(lastEntryBatch), locBatches.get(lastEntryBatch));
                    Log.d(TAG, "Uploading batch " + lastEntryBatch + " returned " + ret);
                    if (ret == 10/* I/O Error */ || ret == -1 /* could not connect */) {
                        break;
                    }
                    lastLocBatch++;
                    lastEntryBatch++;
                } else {
                    ret = CloudAgentHelpers.issueUploadNewEncountersRequest(entryBatches.get(lastEntryBatch), null);
                    lastEntryBatch++;
                }
            }
            while (lastLocBatch < locBatches.size()) {
                ret = CloudAgentHelpers.issueUploadNewEncountersRequest(entryBatches.get(lastEntryBatch), locBatches.get(lastEntryBatch));
                Log.d(TAG, "Uploading batch " + lastEntryBatch + " returned " + ret);
                if (ret == 10/* I/O Error */ || ret == -1 /* could not connect */) {
                    break;
                }
                lastLocBatch++;
            }
            Log.d(TAG, "Uploaded " + lastEntryBatch + " encounter batches and " + lastLocBatch + " location batches: " + ret);

            // update the checkpoints to the most recently added encounter/loc/secret if we successfully posted
            // make sure that we don't update the timestamps beyond what succeeded
            int lastLocBatchIndex = (lastLocBatch+1)*Utils.BATCH_SIZE;
            long newLocTimeStamp = lastLocBatch == 0
                    ? Preferences.getInstance().getDBTimestamp()
                    : locations.get(
                            lastLocBatchIndex > locations.size() ? locations.size()-1 : lastLocBatchIndex-1
                        ).getTimestamp();
            Preferences.getInstance().setTimestamp(newLocTimeStamp);
        });
    }
}