package org.mpisws.encounters.clients;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.microsoft.embeddedsocial.autorest.models.Reason;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.database.DBEncounterInfo;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.encounters.EncounterBasedCommunication;
import org.mpisws.encounters.encounterhistory.PPrivateMessagingChannels;
import org.mpisws.encounters.encounterhistory.bridges.MessagingChannelsBridge;
import org.mpisws.encounters.encounterhistory.models.MPrivateMessagingChannel;
import org.mpisws.encounters.lib.Preferences;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.ThreadPoolManager;
import org.mpisws.helpers.Utils;
import org.mpisws.messaging.EncounterForwardingMessage;
import org.mpisws.messaging.ReceivedMessageWrapper;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.ForwardingConstraint;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.EPOCH_LINK;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.INITIATE_PRIVATE_MESSAGE;

/**
 * Created by tslilyai on 11/6/17.
 */
public class CommunicationClient implements EncounterBasedCommunication.ICommunicationClient {
    private static final String TAG = CommunicationClient.class.getSimpleName();
    private static final int loopLimit = 100;
    private Context context;

    public CommunicationClient(Context context) {
        this.context = context;
        Preferences preferences = GlobalObjectRegistry.getObject(Preferences.class);
        ESClient.getInstance().setESCredentials(preferences.getUserHandle(), preferences.getAuthorizationToken());
    }

    @Override
    public void blockEncounters(EncounterQueryConstraint constraint) {
        ThreadPoolManager.getInstance().runTask(() -> {
            List<DBEncounterInfo> eids = GlobalObjectRegistry.getObject(EncounterBasedCommunication.class)
                    .getSDDRClient().getEncountersWithConstraint(constraint);
            List<String> ids = new ArrayList<>(eids.size());
            for (DBEncounterInfo eid : eids) {
                ids.add(eid.getMyTopicHandle());
            }
            ESClient.getInstance().blockUsers(ids);
        });
    }

    @Override
    public void sendDirectMessageToEncounters(String msgText, EncounterQueryConstraint encounterConstraint, boolean enableReplies) {
        sendMultiHopMessageToEncounters(msgText, encounterConstraint, null, enableReplies);
    }

    @Override
    public void sendMultiHopMessageToEncounters(String msgText, EncounterQueryConstraint encounterConstraint, ForwardingConstraint forwardingConstraint, boolean enableReplies) {
        ThreadPoolManager.getInstance().runTask(() -> {

            // construct the message to send
            String replyTopicHandle = null;
            if (enableReplies) {
                replyTopicHandle = CommunicationHelpers.createNewPublicKeyTopic(context);
            }

            final EncounterForwardingMessage encounterForwardingMessage = new EncounterForwardingMessage.EncounterForwardingMessageBuilder()
                    .addMessageText(msgText)
                    .addEncounterConstraint(encounterConstraint)
                    .addForwardingConstraint(forwardingConstraint)
                    .addReplyTopicHandle(replyTopicHandle)
                    .build();

            // TODO currently ignoring expiration time?
            CloudAgentHelpers.sendEncounterForwardingMessage(encounterForwardingMessage);
        });
    }

    @Override
    public void getUnreadMsgs(ESClient.GetReceivedMessagesCallback callback) {
        ThreadPoolManager.getInstance().runTask(() -> {
            List<ReceivedMessageWrapper> unreadMsgs = new LinkedList<>();

            String newestCursor = null;
            int numLoops = 0;
            String continuationCursor = null;
            Pair<String, List<ReceivedMessageWrapper>> nextCursor_newMsgs;

            boolean done = false;
            while (numLoops < loopLimit) {
                nextCursor_newMsgs = CommunicationHelpers.getUnreadMsgsPage(continuationCursor, context);
                for (ReceivedMessageWrapper msgWrapper : nextCursor_newMsgs.getRight()) {
                    continuationCursor = msgWrapper.getCursor();
                    if (newestCursor == null) {
                        newestCursor = msgWrapper.getCursor();
                        Log.d(TAG, "Read notifs cursor to be set to " + newestCursor);
                    }
                    // skip all epoch linking messages
                    if (msgWrapper.getMsgType() == EPOCH_LINK) {
                        Log.d(TAG, "Skipping epoch link message");
                        continue;
                    }
                    // initiate private messaging! swap out this message for the actual reply message
                    if (msgWrapper.getMsgType() == INITIATE_PRIVATE_MESSAGE) {
                        Log.d(TAG, "Found initiate private message on my public key " + msgWrapper.getEncounterID());
                        List<ReceivedMessageWrapper> replyMessages = CommunicationHelpers.createPrivateChannelAndGetReply(msgWrapper, context);
                        if (replyMessages != null) unreadMsgs.addAll(replyMessages);
                    } else {
                        // add the message to the list of unread messages
                        Log.d(TAG, "Found message of type " + msgWrapper.getMsgType());
                        unreadMsgs.add(msgWrapper);
                    }
                }
                if (nextCursor_newMsgs.getRight().size() == 0) {
                    break;
                }
                if (done) break;
                numLoops++;
            }
            if (newestCursor != null) ESClient.getInstance().markPriorMessagesRead(newestCursor);
            if (callback != null) {
                callback.processReceivedMessages(unreadMsgs);
            }
        });
    }

    @Override
    public void replyToMessage(String replyMessageText, ReceivedMessageWrapper receivedMessageWrapper) {
        ThreadPoolManager.getInstance().runTask(() -> {
            switch (receivedMessageWrapper.getMsgType()) {
                case EPOCH_LINK:
                    break;
                case INITIATE_PRIVATE_MESSAGE:
                    break;
                case ENCOUNTER_FORWARDING_MESSAGE: {
                    EncounterForwardingMessage encounterForwardingMessage = receivedMessageWrapper.getEncounterForwardingMessage();
                    CommunicationHelpers.createKeyAndPostReplyToEncounterForwardingMessage(
                            replyMessageText, encounterForwardingMessage, context);
                    break;
                }
                case PRIVATE_MESSAGE: {
                    CommunicationHelpers.postReplyToPrivateMessage(replyMessageText, receivedMessageWrapper, context);
                    break;
                }
            }
        });
    }

    @Override
    public void getConversationOnPrivateChannel(String topicTitle, ESClient.GetReceivedMessagesCallback callback) {
        MessagingChannelsBridge messagingChannelsBridge = new MessagingChannelsBridge(context);
        MPrivateMessagingChannel mPrivateMessagingChannel = messagingChannelsBridge.getEntryFromTopicTitle(topicTitle);

        ThreadPoolManager.getInstance().runTask(() -> {
            String otherTopicHandle = mPrivateMessagingChannel.getOtherTopicHandle();
            if (otherTopicHandle == null || otherTopicHandle.compareTo("") == 0) {
                otherTopicHandle = ESClient.getInstance().getOneTopicHandle(
                        new Identifier(Utils.hexStringToByteArray(topicTitle)));
                if (otherTopicHandle == null || otherTopicHandle.compareTo("") == 0) {
                    return;
                }
                ContentValues values = new ContentValues();
                values.put(PPrivateMessagingChannels.Columns.otherTopicHandle, otherTopicHandle);
                messagingChannelsBridge.updateFromContentValues(mPrivateMessagingChannel.getPKID(), values);
            }

            List<ESMessage> messagesToMe = ESClient.getInstance().getMessagesOnTopic(mPrivateMessagingChannel.getMyTopicHandle());
            List<ESMessage> messagesFromMe = ESClient.getInstance().getMessagesOnTopic(otherTopicHandle);
            List<ReceivedMessageWrapper> recMessages = new ArrayList<>();
            for (ESMessage msg : messagesToMe) {
                recMessages.add(new ReceivedMessageWrapper(msg, mPrivateMessagingChannel.getSharedSecret()));
            }
            for (ESMessage msg : messagesFromMe) {
                recMessages.add(new ReceivedMessageWrapper(msg, mPrivateMessagingChannel.getSharedSecret()));
            }

            // sort by oldest to newest
            recMessages.sort((m1, m2) -> (int) (m1.getTimestamp() - m2.getTimestamp()));
            callback.processReceivedMessages(recMessages);
        });
    }

    @Override
    public void reportMsg(String cursor, Reason reason) {
        ESClient.getInstance().reportMsg(cursor, reason);
    }
}