package org.mpisws.encounters.dbplatform;

public interface JavaItemFilter<T> {
    public boolean isNeeded(T item);
}
