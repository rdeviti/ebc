package org.mpisws.encounters.dbplatform;

import org.mpisws.database.DBModel;

public class MatchType {

    private DBModel model;
    private boolean row;

    public MatchType(DBModel model, boolean row) {
        this.model = model;
        this.row = row;
    }

    public DBModel getModel() {
        return model;
    }

    public boolean isRow() {
        return row;
    }
}
