package org.mpisws.encounters.dbplatform;

import android.net.Uri;

import org.mpisws.database.DBModel;

import java.util.List;

/**
 *
 * @author verdelyi
 */
public abstract class AggregatePersistenceModel {

    public abstract String getContentProviderAuthority();

    public abstract List<DBModel> getPersistenceModels();
    
    public Uri getRootContentURI() {
        return Uri.parse("content://" + getContentProviderAuthority());
    }
}
