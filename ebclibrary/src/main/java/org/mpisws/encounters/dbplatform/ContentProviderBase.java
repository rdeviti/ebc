package org.mpisws.encounters.dbplatform;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import org.mpisws.database.DBModel;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.encounters.encounterhistory.EncounterHistoryAPM;

import java.util.ArrayList;

public abstract class ContentProviderBase extends ContentProvider {
    private final String TAG = ContentProviderBase.class.getSimpleName();
    private final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    protected SQLiteOpenHelper dbHelper;

    protected abstract AggregatePersistenceModel getAggregatePersistenceModel();
    protected abstract net.sqlcipher.database.SQLiteOpenHelper getDBOpenHelper();

    private int modelToCode(final DBModel model, final boolean isRow) {
        if (isRow) {
            return getAggregatePersistenceModel().getPersistenceModels().indexOf(model)+1000;
        } else {
            return getAggregatePersistenceModel().getPersistenceModels().indexOf(model)+2000;
        }
    }

    private MatchType codeToModel(final int code) {
        if (code == -1) {
            throw new IllegalArgumentException("Probably no match in matcher");
        } else if (code < 1500) {
            return new MatchType(getAggregatePersistenceModel().getPersistenceModels().get(code-1000), true);
        } else if (code > 1500) {
            return new MatchType(getAggregatePersistenceModel().getPersistenceModels().get(code-2000), false);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public synchronized boolean onCreate() {
        dbHelper = getDBOpenHelper();
        for (DBModel model : getAggregatePersistenceModel().getPersistenceModels()) {
            matcher.addURI(getAggregatePersistenceModel().getContentProviderAuthority(),
                    model.getTableName(), modelToCode(model, false));
            matcher.addURI(getAggregatePersistenceModel().getContentProviderAuthority(),
                    model.getTableName() + "/#", modelToCode(model, true));
        }
        //SQLiteDatabase.loadLibs(getContext());
        return true;
    }

    @Override
    public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return null;
        final MatchType matchType = codeToModel(matcher.match(uri));
        final SQLiteDatabase db = dbHelper.getReadableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        final String realselection = matchType.isRow() ? addPKIDConditionToWhereClause(selection, uri.getLastPathSegment()) : selection;
        final Cursor c = db.query(
                matchType.getModel().getTableName(), projection, realselection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri); // TODO ???
        return c;
    }

    @Override
    public synchronized String getType(Uri uri) {
        throw new UnsupportedOperationException();
    }

    @Override
    public synchronized Uri insert(Uri uri, ContentValues values) {
        if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return null;
        final SQLiteDatabase db = dbHelper.getWritableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        final MatchType matchType = codeToModel(matcher.match(uri));
        final DBModel dmb = matchType.getModel();
        try {
            final long rowID = db.insertOrThrow(dmb.getTableName(), null, values);
            if (rowID == -1) {
                throw new SQLException("Insert failed");
            }
            final Uri insertedRowURI = ContentUris.withAppendedId(EncounterHistoryAPM.getContentURI(dmb), rowID);
            getContext().getContentResolver().notifyChange(insertedRowURI, null);
            return insertedRowURI;
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    public String addPKIDConditionToWhereClause(final String where, final String pkidString) { // TODO add to whereArgs instead
        final StringBuilder whereSB = new StringBuilder();
        if (where != null) {
            whereSB.append("(").append(where).append(") AND ");
        }
        whereSB.append("(" + DBModel.Columns.pkid + " = ").append(pkidString).append(")");
        return whereSB.toString();
    }

    @Override
    public synchronized int delete(Uri uri, String where, String[] whereArgs) {
        //Log.d(TAG, "Delete starting: " + uri);
        if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return -1;
        final SQLiteDatabase db = dbHelper.getWritableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        final MatchType matchType = codeToModel(matcher.match(uri));
        final String realWhere = matchType.isRow() ? addPKIDConditionToWhereClause(where, uri.getLastPathSegment()) : where;
        final int count = db.delete(matchType.getModel().getTableName(), realWhere, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        //Log.d(TAG, "Delete done");
        return count;
    }

    @Override
    public synchronized int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return -1;
        Log.d(TAG, "User handle is " + GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        final SQLiteDatabase db = dbHelper.getWritableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        final MatchType matchType = codeToModel(matcher.match(uri));
        final String realselection = matchType.isRow() ? addPKIDConditionToWhereClause(selection, uri.getLastPathSegment()) : selection;
        final int count = db.update(matchType.getModel().getTableName(), values, realselection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    
    @Override
    public synchronized ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final ContentProviderResult[] results = super.applyBatch(operations); // TODO ?!
        return results;
    }

}
