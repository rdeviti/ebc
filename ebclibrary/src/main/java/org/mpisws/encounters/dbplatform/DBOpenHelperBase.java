package org.mpisws.encounters.dbplatform;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import org.mpisws.database.DBModel;
import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;

import static org.mpisws.encounters.encounterhistory.EncounterHistoryAPM.createTable;
import static org.mpisws.encounters.encounterhistory.EncounterHistoryAPM.drop;

public abstract class DBOpenHelperBase extends SQLiteOpenHelper {
    private final static String TAG = DBOpenHelperBase.class.getSimpleName();

    public DBOpenHelperBase(final Context context, final String databaseName, final int databaseVersion) {
        super(context, databaseName, null, databaseVersion);
    }

    protected abstract AggregatePersistenceModel getAggregatePersistenceModel();

    public void recreateDatabase() {
        Log.v(TAG, "RecreateDatabase");
        if (GlobalObjectRegistry.getObject(ESCredentials.class) == null) return;
        final SQLiteDatabase db = getWritableDatabase(GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle());
        try {
            for (DBModel model : getAggregatePersistenceModel().getPersistenceModels()) {
                drop(db, model);
            }
        } catch (final SQLException ex) {
            Log.v(getClass().getSimpleName(), "(Drop table failed)", ex);
        }
        onCreate(db);
        db.close();
    }
    
    private void createTables(final SQLiteDatabase db) {
        for (DBModel model : getAggregatePersistenceModel().getPersistenceModels()) {
            createTable(db, model);
        }
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        Log.v(TAG, "OnCreate");
        createTables(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        Log.v(TAG, "OnOpen: creating tables if they don't exist");
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
