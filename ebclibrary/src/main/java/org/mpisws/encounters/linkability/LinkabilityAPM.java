package org.mpisws.encounters.linkability;

import android.database.Cursor;
import android.net.Uri;

import org.mpisws.database.DBModel;
import org.mpisws.encounters.dbplatform.AggregatePersistenceModel;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author verdelyi
 */
public class LinkabilityAPM extends AggregatePersistenceModel {

    public static final PLinkabilityEntries linkabilityEntries = new PLinkabilityEntries();
    public static final List<DBModel> models = new LinkedList<>();
    private static final LinkabilityAPM instance = new LinkabilityAPM();

    static {
        models.add(linkabilityEntries);
    }

    private LinkabilityAPM() {
    }

    public String getContentProviderAuthority() {
        return "org.mpisws.sddrservice.linkability";
    }

    public List<DBModel> getPersistenceModels() {
        return Collections.unmodifiableList(models);
    }

    public static LinkabilityAPM getInstance() {
        return instance;
    }

    public static Uri getContentURI(DBModel model) {
        return Uri.parse("content://" + LinkabilityAPM.getInstance().getContentProviderAuthority() + "/" + model.getTableName());
    }

    public static long extractPKID(final Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(DBModel.Columns.pkid));
    }
}
