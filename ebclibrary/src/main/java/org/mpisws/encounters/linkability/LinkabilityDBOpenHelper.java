package org.mpisws.encounters.linkability;

import android.content.Context;

import org.mpisws.encounters.dbplatform.AggregatePersistenceModel;
import org.mpisws.encounters.dbplatform.DBOpenHelperBase;

/**
 *
 * @author verdelyi
 */
public class LinkabilityDBOpenHelper extends DBOpenHelperBase {

    private static final String DATABASE_NAME = "linkability.db";
    private static final int DATABASE_VERSION = 1;

    public LinkabilityDBOpenHelper(final Context context) {
        super(context, DATABASE_NAME, DATABASE_VERSION);
    }

    @Override
    protected AggregatePersistenceModel getAggregatePersistenceModel() {
        return LinkabilityAPM.getInstance();
    }
}