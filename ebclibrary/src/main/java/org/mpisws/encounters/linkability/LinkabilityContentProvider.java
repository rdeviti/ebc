package org.mpisws.encounters.linkability;

import android.os.Bundle;

import net.sqlcipher.database.SQLiteOpenHelper;

import org.mpisws.encounters.dbplatform.AggregatePersistenceModel;
import org.mpisws.encounters.dbplatform.ContentProviderBase;

/**
 * 
 * @author verdelyi
 */
public class LinkabilityContentProvider extends ContentProviderBase {

    @Override
    protected AggregatePersistenceModel getAggregatePersistenceModel() {
        return LinkabilityAPM.getInstance();
    }

    @Override
    protected SQLiteOpenHelper getDBOpenHelper() {
        return new LinkabilityDBOpenHelper(getContext());
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        // TODO do we need this?
        return Bundle.EMPTY;
    }
}
