package org.mpisws.encounters.lib;

import android.text.TextUtils;

/**
 * Created by tslilyai on 3/1/18.
 */
/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for license information.
 */

@SuppressWarnings("FieldCanBeLocal")
public final class Options {

    private Application application = null;

    private Options() {}

    public void verify() {
        checkValueIsNotNull("application", application);
        checkValueIsNotNull("application.ebcKey", application.ebcKey);
        checkValueIsNotEmpty("application.ebcKey", application.ebcKey);
    }

    private void throwInvalidConfigException(String message) {
        throw new IllegalArgumentException("Invalid EmbeddedSocial configuration file: " + message);
    }

    private void checkValueIsNotEmpty(String name, String value) {
        if (TextUtils.isEmpty(value)) {
            throw new IllegalArgumentException("field \"" + name + " must be not empty");
        }
    }

    private void checkValueIsNotNull(String name, Object value) {
        if (value == null) {
            throwInvalidConfigException("field \"" + name + "\" is missed");
        }
    }

    public String getEbCKey() {
        return application.ebcKey;
    }

    public void setEbCKey(String ebcKey) {
        application.ebcKey = ebcKey;
    }

    /**
     * General application's options.
     */
    public static class Application {
        public String ebcKey = null;
    }
}
