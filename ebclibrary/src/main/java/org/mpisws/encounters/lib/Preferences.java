/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for license information.
 */

package org.mpisws.encounters.lib;

import android.content.Context;
import android.content.SharedPreferences;

import org.mpisws.helpers.GlobalObjectRegistry;

/**
 * Contains application preferences and config values. Encapsulates the work with SharedPreferences in the app.
 */
public class Preferences {

	private static final String PREF_FILE_NAME = "com.microsoft.embeddedsocial.pref";
	private static final String USER_HANDLE = "userhandle";
	private static final String AUTHORIZATION = "authorization";
	private static final String encEntryDBTimestamp = "encEntrys";
	private static final String ebCKey = "ebCKey";
	private static final String latestReadMessageCursor = "lrmc";

	private final SharedPreferences sharedPreferences;

	public Preferences(Context context) {
		sharedPreferences = context.getApplicationContext()
			.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
	}

	private SharedPreferences.Editor editor() {
		return sharedPreferences.edit();
	}

	public static Preferences getInstance() {
		return GlobalObjectRegistry.getObject(Preferences.class);
	}
	/**
	 * Stores a generates app instance handle.
	 */
	public String getUserHandle() {
		return sharedPreferences.getString(USER_HANDLE, null);
	}

	public void setUserHandle(String userHandle) {
		editor().putString(USER_HANDLE, userHandle).apply();
	}

	public String getAuthorizationToken() {
		return sharedPreferences.getString(AUTHORIZATION, null);
	}

	public void setAuthorizationToken(String authorizationToken) {
		editor().putString(AUTHORIZATION, authorizationToken).apply();
	}


    public long getDBTimestamp() {
        return sharedPreferences.getLong(encEntryDBTimestamp, -1);
    }
    public void setTimestamp(long timestamp) {
		editor().putLong(encEntryDBTimestamp, timestamp).apply();
    }

    public void setEbCKey(String ebCKey) {
		editor().putString(this.ebCKey, ebCKey).apply();
    }

    public String getEbCKey() {
		return sharedPreferences.getString(this.ebCKey, null);
    }

    public String getLatestReadMessageCursor() {
		return sharedPreferences.getString(this.latestReadMessageCursor, "");
    }
    public void setLatestReadMessageCursor(String latestReadMessageCursor) {
        editor().putString(this.latestReadMessageCursor, latestReadMessageCursor).apply();
    }
}