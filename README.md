# Table of contents
* [Javadocs](/docs)
* [The exampleapp module](https://gitlab.rts.mpi-sws.org/encounters/ebc/tree/master/exampleapp) demonstrates the below configuration, initialization, and usage instructions
* [Motivation: What is Encounter-based Communication?](#encounter-based-communication)
* [Necessary Setup](#necessary-setup)
    * [Initializing the EbC Agent ](#initializing-the-ebc-agent)
    * [EbC User Accounts](#ebc-user-accounts)
* [Usage](#usage)
    * [Encounter Formation and Confirmation](#encounter-formation-and-confirmation)
    * [Direct Communication over Encounters](#direct-communication-over-encounters)

******

# Encounter-Based Communication
<h4 align="center">Add secure, encounter-based communication to your Android applications.</h4>

This Android library implements _Encounter-based Communication_, a communication paradigm that is based on the notion of an _encounter_, or a period of co-location. EbC enables users to follow up after a meeting; share commentary, advice, or content about an attended event or a
visited place; identify people with shared interests at a large
event; confirm that they have met at a given place and time;
and use local services or control their present physical environment. EbC enables this type of communication securely,
without requiring participants to exchange contact details or reveal personal information.
More formally, EbC provides the following 4 properties:

1. Implicit naming: Communication is possible without the need to pair devices a priori or manually exchange contact details, identifiers, or keys.
2. Persistent addressing: Communication is possible both during and after a period of co-location.
3. Privacy: Parties may de-anonymize, re-identify, or track users/devices only if explicitly permitted.
4. Security: Users can authenticate communication partners as people or resources they have encountered at a specific time and place; message exchanges should be confidential, with message integrity protected.

The EbCLibrary module exposes an interface to control encounter discovery and send/receive direct messages to/from these encounters. Message recipients are selected by a number of _constraints_, including time, place, and length of the encounter.

## Necessary Setup

### Integration

In Android Studio, add the following in `build.gradle`
```
repositories {
    maven { url "https://dl.bintray.com/tslilyai/EncounterBasedCommunication" }
}
dependencies {
    compile 'org.mpisws:ebclibrary:0.1.0.2'
}
```

Alternatively, the source code can be downloaded and included as a separate module in an Android project.

EbC requires that the device have BLE capabilities and Bluetooth enabled. Furthermore, the user must allow access to location services. This requires the application to request Bluetooth and location access upon start (see `MainActivity.java` in the `testapp` module for an example). In your `AndroidManifest`, you must also include the following permissions:
```
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-feature android:name="android.hardware.bluetooth_le" />
```

### Initializing the EbC Agent

The EncounterBasedCommunication agent is initialized with the application context and a special config file (placed in `res/raw`). Please email `ebc@mpi-sws.org` to receive this file, which contains a key necessary to authenticate the application as an EbC application with the backend service. Initialization looks as follows:

```java
EncounterBasedCommunication ebc = EncounterBasedCommunication.getInstance();
ebc.initialize(getApplicationContext(), R.raw.ebc_config);
```

### EbC User Accounts 
In order to use any EbC functionality, the application must have the user sign into their EbC account. Each EbC account is tied to a Google account: the application must acquire a Google OAuth token for the user, which is passed as an argument to the EbC call `loginAccountWithGoogle(googletoken)`. A typical example of this workflow looks as follows:

```java
/* 
 * Log in the user; this procedure will also create an account for the user if her account does not yet exist.
 */
public void loginUser() {
    String googletoken = GoogleToken.getToken(); // application must implement Google token acquisition
    if (googletoken != null) {
        ebc.getUserAccountClient().loginAccountWithGoogle(googletoken);
    }
}

/* 
 * Log out of an EbC account and prevent access to any ebc functionality.
 * The user must then provide another googletoken the next time she wishes to log in.
 */
ebc.getUserAccountClient().signOut();

/* 
 * Delete an EbC account 
 * Note that this does *not* delete the encounter database stored on the device (as the database is device, and not user, specific).
 */ 
ebc.getUserAccountClient().deleteAccount();
```

Per-user EbC account credentials are used to encrypt the encounter database on the phone and hold the user accountable for any messages sent to encounters using the EbC library. Due to the per-device nature of the encounter database, it is expected that only one account use the application for every device.

_Note: a `Security Exception` will be thrown if any EbC actions are attempted when the user is not logged in or the EbC instance is not initialized._

## Usage
_After initializing an EbC agent and logging into a user account_, the application can access its various components to control encounter formation (`IEncounterFormationClient`) and handle direct messaging (`ICommunicationClient`).

These two clients are accessed via calls to
```java
ebc.getSDDRClient();
ebc.getCommunicationClient();
```

### Encounter Formation and Confirmation
Once a user is logged in, she can begin to form and confirm encounters (allowing her to later communicate over these encounters). A typical workflow is as follows:

```java
// start the encounter formation service running in the background
ebc.getSDDRClient().startFormingEncounters();

if (device_has_network) {
    // confirm all unconfirmed encounters (opening communication channels for these encounters)
    // over the network. This requires WiFi or data to succeed.
    ebc.getSDDRClient().confirmAndCommunicateUsingNetwork();
} else if (want_to_communicate_over_bluetooth) {
    // confirm all unconfirmed encounters (opening communication channels for these encounters)
    // over a Bluetooth connection. This requires the other end of the encounter to be within range
    // and be available to connect over Bluetooth.
    ebc.getSDDRClient().confirmAndCommunicateUsingBLE();
} else {
    // all encounters will remain unconfirmed (and un-communicable) in 
    // the device's database. Encounter confirmation over the network 
    // should be initialiated when network access becomes available to
    // enable communication with prior encounters.
}

....

// if application wishes to stop encounter confirmation 
// (over either the network or BLE)
ebc.getSDDRClient().disableConfirmation();

// when application wishes to stop encounter formation
ebc.getSDDRClient().stopFormingEncounters();
```

### Direct Communication over Encounters
Logged-in users can communicate over all confirmed encounters in the device's encounter database.
A typical communication workflow to send messages is as follows:

```java
/* The EbC CommunicationClient allows you to send messages to your encounters, specified by time/place or path 
 */ 

/* EXAMPLE: SENDING TO ENCOUNTERS WITHIN A SPECIFIED SPACE-TIME REGION */
EncounterQueryConstraint encounterConstraintSpaceTime = new SpaceTimeIntersectConstraint(
    CURRENT_TIME_MS() - MIN_TO_MS(100) /*starttime*/, 
    CURRENT_TIME_MS() /*endtime*/, 
    MIN_TO_MS(5) /*overlap duration*/, 
    new SpaceRegion(12003 /*latitude*/, 4000 /*longitude*/, 100 /*radius in meters*/),
    null /*non-null values constrains to encounters overlapping with the specified time value*/);

/* EXAMPLE: SENDING TO ENCOUNTERS THAT OCCURRED ON YOUR PATH (TRAJECTORY) */
EncounterQueryConstraint encounterConstraintTrajectory = new TrajectoryConstraint(
                        System.currentTimeMillis()-DAYS2MS(1), System.currentTimeMillis(),
                        MIN2MS(10), 
                        null // don't factor in causality,
                        false);

// Note that this will require the network to be available, as messages through encounters 
// are sent via a secure cloud service
getEbC(this).getCommunicationClient().sendDirectMessageToEncounters("msg_text", encounterConstraintTrajectory, true);
getEbC(this).getCommunicationClient().sendMultiHopMessageToEncounters("msg_text", encounterConstraintSpaceTime, null, false);
```

You can also get all your unread messages from various encounters (processed asynchronously). Note that
there are two types of messages, ones sent via encounters, and others sent via 
private channels you have opened up with other users. These private channels are created when you
respond to a message received over an encounter. Other types of messages are used as metadata only,
and should be ignored:
```java
Client.GetReceivedMessagesCallback callback = new ESClient.GetReceivedMessagesCallback() {
	@Override
	public void processReceivedMessages(List<ReceivedMessageWrapper> unreadMsgs) {
		for (ReceivedMessageWrapper msg : unreadMsgs) {
			String msgText;
			switch (msg.getMsgType()) {
				case ENCOUNTER_FORWARDING_MESSAGE:
					EncounterForwardingMessage encounterForwardingMessage = msg.getEncounterForwardingMessage();
					// you can read the message
					msgText = encounterForwardingMessage.getMsgText();
					break;
				case PRIVATE_MESSAGE:
					PrivateMessage privateMessage = msg.getPrivateMessage();
					msgText = privateMessage.getMsgText();
					break;
				default: // skip non-messaging type messages used as metadata 
					msgText = "";
					break;
			}
			// you can get the topic text, i.e. the EncounterID to which this message corresponds
			String whichEncounter = msg.getEncounterID();
			// you can also report offensive text
			if (isOffensive(msgText)) {
				getEbC(getApplicationContext()).getCommunicationClient().reportMsg(msg.getCursor(), Reason.OFFENSIVECONTENT);
			}
		}
	}
};
getEbC(this).getCommunicationClient().getUnreadMsgs(callback);
```

```java
/* You can also block certain encounters from messaging you by specifying a constraint on which encounters to block 
 */
ebc.getCommunicationClient().blockEncounters(encounterConstraintSpaceTime);

/* 
 * And report received messages as spam or unwanted
 */
List<EbCMsgContents> unreadMsgs = ebc.getCommunicationClient().getAllUnreadMsgs();
for (EbCMsgContents esMessage : unreadMsgs) {
    if (is_offensive(esMessage.getMsgText())) {
        ebc.getCommunicationClient().reportMsg(esMessage, Reason.OFFENSIVECONTENT);
    }
}
```
