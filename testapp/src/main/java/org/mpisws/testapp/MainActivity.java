package org.mpisws.testapp;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.encounters.EncounterBasedCommunication;
import org.mpisws.messaging.ReceivedMessageWrapper;
import org.mpisws.testapp.googleauth.GoogleNativeAuthenticator;
import org.mpisws.testapp.googleauth.GoogleToken;
import org.mpisws.testapp.simulator.Alarm;
import org.mpisws.testapp.simulator.SimulationClientBT;
import org.mpisws.testapp.simulator.SimulationServerBT;
import org.mpisws.testapp.simulator.NullWakelock;

import java.util.List;

import static org.mpisws.encounters.EncounterBasedCommunication.REQUEST_ACCESS_FINE_LOCATION;
import static org.mpisws.encounters.EncounterBasedCommunication.REQUEST_ENABLE_BT;
import static org.mpisws.encounters.EncounterBasedCommunication.isSignedIn;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static EncounterBasedCommunication ebc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ebc = EncounterBasedCommunication.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!requestBT() || !request_permissions()) {
            Intent mStartActivity = new Intent(this, MainActivity.class);
            int mPendingIntentId = 123456;
            PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager mgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
            System.exit(0);
        }
        findViewById(R.id.getGoogleToken).setOnClickListener(this);
        findViewById(R.id.signIn).setOnClickListener(this);
        findViewById(R.id.signOut).setOnClickListener(this);
        findViewById(R.id.deleteAccount).setOnClickListener(this);
        findViewById(R.id.testSendMessages).setOnClickListener(this);
        findViewById(R.id.testReceiveMessages).setOnClickListener(this);
        findViewById(R.id.nullWakelock).setOnClickListener(this);
        findViewById(R.id.simulateEncounterFormationAndConfirmationClient).setOnClickListener(this);
        findViewById(R.id.simulateEncounterFormationAndConfirmationServer).setOnClickListener(this);
        findViewById(R.id.testEndToEndES).setOnClickListener(this);
        findViewById(R.id.testEndToEndBLE).setOnClickListener(this);
        findViewById(R.id.testWithoutConfirmation).setOnClickListener(this);

        ebc.initialize(this, R.raw.ebc_config);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /***************************** USER STUFF **********************/
            case R.id.getGoogleToken:
                if (isSignedIn() || GoogleToken.getToken() != null)
                    return;
                GoogleNativeAuthenticator GNA = new GoogleNativeAuthenticator(GoogleNativeAuthenticator.AuthenticationMode.SIGN_IN_ONLY, this);
                GNA.makeAuthRequest();
                break;
            case R.id.signIn:
                String googletoken = GoogleToken.getToken();
                if (!isSignedIn() && googletoken != null) {
                    ebc.getUserAccountClient().loginAccountWithGoogle(googletoken);
                    // remove temporary google token
                    GoogleToken.setToken(null);
                }
                break;
            case R.id.signOut:
                ebc.getUserAccountClient().signOut();
                break;
            case R.id.deleteAccount:
                ebc.getUserAccountClient().deleteAccount();
                break;


            /***************************** ES AND SDDR TESTS **********************/
            case R.id.nullWakelock:
                //if (!isSignedIn()) throw new SecurityException("Not signed in");
                startService(new Intent(MainActivity.this, NullWakelock.class));
                Log.d(TAG, "Starting null wakelock test!");
                break;
            case R.id.simulateEncounterFormationAndConfirmationClient:
                if (!isSignedIn()) throw new SecurityException("Not signed in");
                new SimulationClientBT(this).startClient();
                break;
            case R.id.simulateEncounterFormationAndConfirmationServer:
                if (!isSignedIn()) throw new SecurityException("Not signed in");
                Log.d(TAG, "Starting simulation server!");
                new SimulationServerBT(this).startServer();
                break;
            case R.id.testSendMessages:
                ebc.getSDDRClient().updateDatabaseOnAgent();
                ebc.getCommunicationClient().sendMultiHopMessageToEncounters("hello world", null, null, true);
                break;
            case R.id.testReceiveMessages:
                ESClient.GetReceivedMessagesCallback callback = new ESClient.GetReceivedMessagesCallback() {
                    @Override
                    public void processReceivedMessages(List<ReceivedMessageWrapper> msgs) {
                         for (ReceivedMessageWrapper msg : msgs) {
                            System.out.print(msg.getTopicHandle() + ", ");
                            System.out.print(msg.getMsgType());
                            System.out.println("\n");
                        }
                    }
                };
                ebc.getCommunicationClient().getUnreadMsgs(callback);
                break;
            case R.id.testEndToEndES:
                ebc.getSDDRClient().startFormingEncounters();
                ebc.getSDDRClient().confirmAndCommunicateUsingNetwork();
                break;
            case R.id.testWithoutConfirmation:
                ESClient.getInstance().setESCredentials("user", "fakeauth");
                ebc.getSDDRClient().startFormingEncounters();
                break;
            case R.id.testEndToEndBLE:
                ebc.getSDDRClient().startFormingEncounters();
                ebc.getSDDRClient().confirmAndCommunicateUsingBLE();
                break;
            default:
                return;
        }
    }


    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            Log.v(TAG, "Bluetooth not enabled");
            Toast.makeText(this, "Exiting encountersService: Bluetooth required", Toast.LENGTH_SHORT).show();
            finishAndRemoveTask();
        } else {
            Toast.makeText(this, "Bad Response", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean request_permissions() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ACCESS_FINE_LOCATION);
                    }
                };
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("encountersService requires location access for Bluetooth protocol; please grant and then restart the application (Note to self: this should be handled better by the actual application using the library)")
                        .setPositiveButton("OK", listener)
                        .setNegativeButton("Cancel", null)
                        .create()
                        .show();
                return false;
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_ACCESS_FINE_LOCATION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    // permission denied!
                    Log.v(TAG, "No access to fine location");
                    Toast.makeText(this, "Exiting encountersService: Location access required", Toast.LENGTH_SHORT).show();
                    finishAndRemoveTask();
                }
                return;
            }
        }
    }

    private boolean requestBT() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter btadapter = bluetoothManager.getAdapter();
        if(btadapter == null||!btadapter.isEnabled())
        {
            Log.v(TAG, "Bluetooth not enabled");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return false;
        }
        return true;
    }
}
