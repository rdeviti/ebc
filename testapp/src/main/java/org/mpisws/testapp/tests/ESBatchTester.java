package org.mpisws.testapp.tests;

import android.os.Handler;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.helpers.Identifier;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.util.concurrent.Executors.newCachedThreadPool;

/**
 * Created by tslilyai on 2/26/18.
 */

public class ESBatchTester {
    private int count;
    private List<Identifier> topicTitles;
    private List<String> topicTitleStrs;
    List<Pair<Identifier, Identifier>> titlePairs;

    public ESBatchTester(int count) {
        this.count = count;
        topicTitles = new ArrayList<>(count);
        topicTitleStrs = new ArrayList<>(count);
        titlePairs = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Identifier name = new Identifier(String.valueOf(System.currentTimeMillis() + i * i).getBytes());
            topicTitles.add(name);
            topicTitleStrs.add(name.toString());
        }
        for (Identifier topic : topicTitles) {
            titlePairs.add(new MutablePair<>(topic, topic));
        }
    }

    public void simulateConfirmations() {
        List<Pair<Identifier, Identifier>> titlePairs2 = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Identifier id = new Identifier(String.valueOf(System.currentTimeMillis() + i * i).getBytes());
            titlePairs2.add(new MutablePair<>(id, id));
        }
        new Thread(() -> {
            List<String> succeeded = ESClient.getInstance().createTopics(titlePairs);
            System.out.println("Posted adverts: " + succeeded.toString());
            List<String> confirmPubKeys = ESClient.getInstance().getTopicTexts(topicTitleStrs);
            System.out.println("Got pub keys: " + confirmPubKeys.toString());
            succeeded = ESClient.getInstance().createTopics(titlePairs2);
            System.out.println("Done: " + succeeded.toString());
        }).start();
    }

    public void runBatchTest() {
        /* findAndCreateTopics uses an executorService (like newCachedThreadPool() to
            execute a runnable. This runnable does the batching logic / latch logic for batching.
            The ESClient thread waits on the runnable thread to finish, at which point it
            continues.

            My question is: why do the 2nd and 3rd methods below freeze (the runnable
            thread never seems to continue after issueBatch() is called, because the callbacks
            registered for the batch call are never invoked and therefore the latch in the
            runnable never decrements to 0)
         */
        // THIS WORKS
        /*newCachedThreadPool().execute(() -> {
            List<String> successes = ESClient.getInstance().getTopicTexts(topicTitleStrs);//ESClient.getInstance().findAndCreateTopics(titlePairs, false);
            System.out.println("DONE: " + successes.toString());
        });*/
        // THIS FREEZES (NO CALLBACKS INVOKED EVEN WHEN BATCHCLIENT RETURNS RESPONSES)
        new Handler().post(() -> {
            List<String> successes = ESClient.getInstance().getTopicTexts(topicTitleStrs);//ESClient.getInstance().findAndCreateTopics(titlePairs, false);
            System.out.println("DONE: " + successes.toString());
        });
        // THIS ALSO FREEZES
        //List<String> successes = ESClient.getInstance().getTopicTexts(topicTitleStrs);//ESClient.getInstance().findAndCreateTopics(titlePairs, false);
        //System.out.println("DONE: " + successes.toString());
    }

    public void runSimulateESOnlyTest() {
        simulatePostAdverts();
        simulateGetDHPubKeys();
        simulatePostEncounterTopics();
        simulatePostLinkAdvert();
        simulateGetLinkMsg();
    }

    private void simulatePostAdverts() {
        ESClient.getInstance().createTopics(titlePairs);
        System.out.println("POST ADVERTS: " +  "**************************TESTING************************");
    }

    private void simulateGetDHPubKeys() {
        List<String> confirmPubKeys = ESClient.getInstance().getTopicTexts(topicTitleStrs);
        System.out.println("GET DHPUBKEY: " +  "**************************TESTING************************" + String.valueOf(confirmPubKeys.size()));
    }

    private void simulatePostEncounterTopics() {
        List<Pair<Identifier, Identifier>> info = new LinkedList<>();
        byte[] encounterIDBytes = (System.currentTimeMillis() + "Hello World").getBytes();
        for (int i = 0; i < topicTitles.size(); i++) {
            info.add(new ImmutablePair<>(new Identifier(encounterIDBytes), new Identifier(encounterIDBytes)));
        }
        ESClient.getInstance().createTopics(info);
        System.out.println("POST ETOPICS : " +  "**************************TESTING************************" + info.size() + " ENCOUNTER TOPICS");
    }

    private void simulatePostLinkAdvert() {
        List<ESMessage> msgsToPost = new LinkedList<>();
        for (Identifier title : topicTitles) {
            msgsToPost.add(new ESMessage(title.toString(), title.toString(), null, true, null, true, -1));
        }
        ESClient.getInstance().sendMsgs(msgsToPost);
        System.out.println("POST LINK MSG: " +  "**************************TESTING************************" + "SENT " + msgsToPost.size() + " messages");
    }

    private void simulateGetLinkMsg() {
        // TODO right now just call this function a bunch of times (since it doesn't actually retrieve messages)
        for (int i = 0; i < topicTitles.size(); i++) {
            Pair<String, List<ESMessage>> msgs = ESClient.getInstance().getPageMessages(null, 20);
            System.out.println("GET LINK MSG: " +  "**************************TESTING************************" + "Got " + msgs.getRight().size() + " messages");
        }
    }
}
