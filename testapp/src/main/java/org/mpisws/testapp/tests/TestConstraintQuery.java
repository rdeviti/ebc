package org.mpisws.testapp.tests;

import org.mpisws.messaging.constraints.AllEncountersConstraint;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.SpaceTimeIntersectConstraint;
import org.mpisws.messaging.constraints.TrajectoryConstraint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tslilyai on 2/23/18.
 */

public class TestConstraintQuery {
    static EncounterQueryConstraint.SpaceRegion spaceRegion = new EncounterQueryConstraint.SpaceRegion(23849230, 23849320, 1000);
    static long starttime = 10000;
    static long overlapduration = 10000;

    public static void main(String[] args) {
        for (EncounterQueryConstraint constraint : getConstraints()) {
            System.out.println(constraint.toQueryString());
        }
    }

   public static List<EncounterQueryConstraint> getConstraints (){
       List<EncounterQueryConstraint> constraints = new ArrayList<>();
       constraints.add(new SpaceTimeIntersectConstraint(starttime, starttime+1000, overlapduration, spaceRegion, true));
       constraints.add(new AllEncountersConstraint());
       return constraints;
   }
}
