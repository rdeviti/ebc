package org.mpisws.testapp.simulator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import org.mpisws.encounters.encounterformation.SDDR_Native;
import org.mpisws.helpers.Identifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class SimulationClientBT {
    Context context;
    protected static final int NUM_SIMULATED_DEVICES = 10;
    protected static final int NUM_SIMULATED_EPOCHS = 10;

    private static final String TAG = SimulationClientBT.class.getSimpleName();
    private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private final BluetoothAdapter mAdapter;
    static private SimulatorEncounterFormationCore core;

    protected static List<Identifier> mDHPubKeys = new ArrayList<>(NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> mDHNonces = new ArrayList<>(NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> mDHFullKeys = new ArrayList<>(NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> otherDHPubKeys = new ArrayList<>(NUM_SIMULATED_DEVICES*NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> otherDHNonces = new ArrayList<>(NUM_SIMULATED_DEVICES*NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> otherDHFullKeys = new ArrayList<>(NUM_SIMULATED_DEVICES*NUM_SIMULATED_EPOCHS);
    protected static List<Identifier> mSharedSecrets = new ArrayList<>(NUM_SIMULATED_DEVICES*NUM_SIMULATED_EPOCHS);
    protected static int CURRENT_EPOCH = 0;

    public SimulationClientBT(Context context) {
        this.context = context;
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(mReceiver, filter);
        core = new SimulatorEncounterFormationCore(context);
        initializeSimulatedAdverts();
    }

    public void startClient() {
        mAdapter.cancelDiscovery();
        Log.d(TAG, "Starting discovery");
        mAdapter.startDiscovery();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.d(TAG, "Device found: " + device.getName() + "; MAC " + device.getAddress());
                if (device.getName() != null && device.getName().contains("Xperia")) {
                    connect(device);
                    context.unregisterReceiver(mReceiver);
                    mAdapter.cancelDiscovery();
                }
            }
        }
    };

    private void connect(BluetoothDevice device) {
        new Thread( () -> {
            BluetoothSocket sock;
            try {
                sock = device.createRfcommSocketToServiceRecord(MY_UUID);
                sock.connect();
                Log.d(TAG, "Connected, sending DH keys over socket");
                sendDHKeysOverSocket(sock);
                sock.close();
            } catch (IOException e) {
                Log.d(TAG, "Exception: " + e);
            }
        }).start();
    }

    private void initializeSimulatedAdverts() {
        for (int i = 0; i < NUM_SIMULATED_EPOCHS*NUM_SIMULATED_DEVICES; i++) {
            SDDR_Native.c_changeEpoch();
            otherDHPubKeys.add(new Identifier(SDDR_Native.c_getAdvertDHPubKey()));
            otherDHFullKeys.add(new Identifier(SDDR_Native.c_getAdvertDHKey()));
            otherDHNonces.add(new Identifier(SDDR_Native.c_getMyAdvert()));
        }
        for (int i = 0; i < NUM_SIMULATED_EPOCHS; i++) {
            SDDR_Native.c_changeEpoch();
            mDHPubKeys.add(new Identifier(SDDR_Native.c_getAdvertDHPubKey()));
            mDHFullKeys.add(new Identifier(SDDR_Native.c_getAdvertDHKey()));
            mDHNonces.add(new Identifier(SDDR_Native.c_getMyAdvert()));

            for (int j = 0; j < NUM_SIMULATED_DEVICES; j++) {
                Identifier sharedSecret = new Identifier(SDDR_Native.c_computeSecretKeyWithSHA(
                        otherDHFullKeys.get(j+(NUM_SIMULATED_DEVICES*i)).getBytes(),
                        mDHNonces.get(i).getBytes(),
                        mDHPubKeys.get(i).getBytes()));
                mSharedSecrets.add(sharedSecret);
            }
        }
    }

    private void sendDHKeysOverSocket(BluetoothSocket sock) {

        try {

            if (sock == null) return;
            OutputStream os = sock.getOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(os);

            out.writeObject(mSharedSecrets);
            out.writeObject(otherDHNonces);
            out.writeObject(otherDHPubKeys);
            out.flush();
            Log.d(TAG, "Sent data over socket!");

            BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            String line = in.readLine();
            Log.d(TAG, "Got response: " + line);

            in.close();
            out.close();
            os.close();
            sock.close();

        } catch (IOException e) {
            System.out.println("SimulationClient: General I/O exception: " + e.getMessage());
            e.printStackTrace();
        }

        // run the simulator core
        Log.d(TAG, "Running simulator core!");
        new Thread(core).start();
    }
}
