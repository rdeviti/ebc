package org.mpisws.testapp.simulator;

/**
 * Created by tslilyai on 10/18/17.
 *
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.util.Log;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mpisws.encounters.encounterformation.SDDR_Native;
import org.mpisws.encounters.encounterformation.SDDR_Proto;
import org.mpisws.encounters.encounterhistory.events.EncounterEndedEvent;
import org.mpisws.encounters.encounterhistory.events.EncounterEvent;
import org.mpisws.encounters.encounterhistory.events.EncounterStartedEvent;
import org.mpisws.encounters.encounterhistory.events.RSSIEntry;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_POWER;
import static org.mpisws.encounters.EncounterBasedCommunication.SCAN_BATCH_INTERVAL;
import static org.mpisws.testapp.simulator.SimulationClientBT.CURRENT_EPOCH;
import static org.mpisws.testapp.simulator.SimulationClientBT.NUM_SIMULATED_DEVICES;
import static org.mpisws.testapp.simulator.SimulationClientBT.otherDHNonces;
import static org.mpisws.testapp.simulator.SimulatorEncounterFormationCore.mDHFullKey;
import static org.mpisws.testapp.simulator.SimulatorEncounterFormationCore.mDHPubKey;
import static org.mpisws.testapp.simulator.SimulatorEncounterFormationCore.mNonce;
import static org.mpisws.testapp.simulator.SimulatorEncounterFormationCore.ongoingEncounters;

/**
 * Scans for Bluetooth Low Energy Advertisements matching a filter and displays them to the user.
 */
public class SimulatorScannerProcessor {
    private static final String TAG = SimulatorScannerProcessor.class.getSimpleName();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanCallback mScanCallback;
    private SimulatorEncounterFormationCore core;

    public SimulatorScannerProcessor() { }

    public void initialize(BluetoothAdapter btAdapter, SimulatorEncounterFormationCore core) {
        this.mBluetoothAdapter = btAdapter;
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        this.core = core;
    }

    /**
     * Start scanning for BLE Advertisements (and stop after a set period of time).
     */
    public void startScanning() {
        if (mScanCallback == null) {
            mScanCallback = new SDDRScanCallback();
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
        } else {
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
        }
        Log.v(TAG, "Starting Scanning on thread " + Thread.currentThread().getName());
    }

    /**
     * Stop scanning for BLE Advertisements.
     */
    public void stopScanning() {
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    /**
     * Filter our scans so we only discover SDDR_API devices
     */
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();
        return scanFilters;
    }

    /**
     * Return a {@link ScanSettings} object (default settings for now)
     */
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(SCAN_MODE_LOW_POWER);
        builder.setReportDelay(SCAN_BATCH_INTERVAL);
        return builder.build();
    }

    /**
     * Custom ScanCallback object. Calls the native function to process discoveries for encounters.
     */
    private class SDDRScanCallback extends ScanCallback {
        private void simulateReceivingResults() {
            Log.d(TAG, "CURRENT EPOCH: " + CURRENT_EPOCH);
            // get all "new" nonces you should've "received"
            for (int i = 0; i < NUM_SIMULATED_DEVICES; i++) {
                // for each new nonce, call processScanResult
                byte[] newnonce = otherDHNonces.get(i+(NUM_SIMULATED_DEVICES*CURRENT_EPOCH)).getBytes();
                long pkid = SDDR_Native.c_processScanResult(-1, newnonce, System.currentTimeMillis());
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.v(TAG, results.size() + " Batch Results Found on thread " + Thread.currentThread().getName());

            SDDR_Native.c_preDiscovery();
            simulateReceivingResults();
            SDDR_Native.c_postDiscovery();
            processEncounters();
            core.postScanProcessing();
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            Log.v(TAG, "Some scan result");
            SDDR_Native.c_preDiscovery();
            simulateReceivingResults();
            SDDR_Native.c_postDiscovery();
            processEncounters();
            core.postScanProcessing();
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.v(TAG, "Scanning failed: " + errorCode);
        }
    }

    private void processEncounters() {
        List<Long> pkids = new ArrayList<>();
        for (Iterator<byte[]> iterator = SDDR_Native.c_EncounterMsgs.iterator(); iterator.hasNext(); ) {
            byte[] msg = iterator.next();
            final SDDR_Proto.Event event;

            try {
                event = SDDR_Proto.Event.parseFrom(msg);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                return;
            }

            Utils.myAssert(event.hasEncounterEvent());
            final SDDR_Proto.Event.EncounterEvent subEvent = event.getEncounterEvent();
            final SDDR_Proto.Event.EncounterEvent.EventType type = subEvent.getType();
            final long time = subEvent.getTime();
            final long pkid = subEvent.getPkid();
            pkids.add(pkid);
            final String address = subEvent.getAddress();
            final List<SDDR_Proto.Event.EncounterEvent.RSSIEvent> rssiEventsPB = subEvent.getRssiEventsList();
            final List<ByteString> advertsPB = subEvent.getSharedSecretsList();

            // Transforming the lists into the appropriate Java structures for EncounterEvent
            final List<RSSIEntry> rssiEvents = new LinkedList<>();
            for (SDDR_Proto.Event.EncounterEvent.RSSIEvent rssiEvent : rssiEventsPB) {
                rssiEvents.add(new RSSIEntry(rssiEvent.getTime(), rssiEvent.getRssi()));
            }

            Identifier receivedNonce = null;
            // we don't have to track devices' adverts if we're trying to connect to them now
            if (advertsPB.size() > 0) {
                // TODO hack there really is only ever one nonce per encounter
                receivedNonce = new Identifier(advertsPB.get(0).toByteArray());
            }

            EncounterEvent encEvent;
            switch (type) {
                case UnconfirmedStart:
                    encEvent = new EncounterStartedEvent(pkid, time, receivedNonce, mNonce, mDHPubKey, mDHFullKey);
                    Log.v(TAG, "[EncounterEvent] Tentative encounter started at " + time);
                    ongoingEncounters.put(receivedNonce.toString(), new ImmutablePair<>(pkid, time));
                    break;
                case End:
                    encEvent = new EncounterEndedEvent(pkid, time, rssiEvents);
                    Log.v(TAG, "[EncounterEvent] Encounter Ended at " + time);
                    ongoingEncounters.remove(receivedNonce.toString());
                    break;
                default:
                    encEvent = null;
            }

            Log.v(TAG, "\tPKID = " + pkid + ", Address = " + address);

            if (encEvent != null) encEvent.broadcast(core.mService);
            iterator.remove();
        }
        EncounterEvent.insertLocationForEncounters(core.mService);
    }
}
