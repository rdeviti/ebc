package org.mpisws.testapp.simulator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Identifier;
import org.mpisws.messaging.EpochLinkMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mpisws.encounters.EncounterBasedCommunication.CHANGE_EPOCH_TIME;
import static org.mpisws.helpers.Utils.SHA1;
import static org.mpisws.testapp.simulator.SimulationClientBT.NUM_SIMULATED_DEVICES;
import static org.mpisws.testapp.simulator.SimulationClientBT.NUM_SIMULATED_EPOCHS;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class SimulationServerBT {
    private static final String TAG = SimulationServerBT.class.getSimpleName();
    private static final String NAME = "SimulationServerBT";
    private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private final BluetoothAdapter mAdapter;
    private Context context;

    private int currentEpoch = 0;
    private List<Identifier> nonces;
    private List<Identifier> sharedSecrets;
    private List<Identifier> pubKeys;

    /**
     */
    public SimulationServerBT(Context context) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        this.context = context;
    }

    public void startServer() {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        context.startActivity(discoverableIntent);

        new Thread( () -> {
            Log.d(TAG, "start");
            BluetoothSocket socket;
            // Create a new listening server socket
            try {
                BluetoothServerSocket serversocket = mAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
                Log.d(TAG, "accepting");
                socket = serversocket.accept();
                Log.d(TAG, "accepted");
                serversocket.close();
                // If a connection was accepted
                if (socket != null) {
                    ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                    sharedSecrets = (List<Identifier>)in.readObject();
                    nonces = (List<Identifier>)in.readObject();
                    pubKeys = (List<Identifier>)in.readObject();
                    if (sharedSecrets.size() != nonces.size() || nonces.size() != pubKeys.size()) {
                        return;
                    }
                    PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
                    pw.println("Finished");

                    pw.close();
                    in.close();
                    socket.close();
                    processData();
                } else {
                    Log.d(TAG, "Socket was null");
                }
            } catch (IOException | ClassNotFoundException e) {
                Log.e(TAG, "IO exception trying to accept and read " + e.getMessage());
            }
        }).start();
    }
    private void processData() {
        final Handler handler = new Handler(Looper.getMainLooper());
        // Create all topics you'll ever have to create
        List<Pair<Identifier, Identifier>> topicsToCreate = new ArrayList<>();
        for (int i=0; i < nonces.size(); i++) {
            topicsToCreate.add(new ImmutablePair<>(nonces.get(i), pubKeys.get(i)));
        }
        for (Identifier ss : sharedSecrets) {
            topicsToCreate.add(new ImmutablePair<>(Coder.convertSharedSecretToID(ss), Coder.convertSharedSecretToID(ss)));
        }
        Log.d(TAG, "Creating nonce and shared secret topics: " + topicsToCreate.size());
        ESClient.getInstance().createTopics(topicsToCreate);

        // Every "epoch" or so try to post link messages to the prior "epoch" ss for each "device"
        // Let's try and post to the prior 3 epochs
        handler.postDelayed(new Runnable() {
            public void run() {
                new Thread(() -> {
                    Log.d(TAG, "Runnable!");
                    int lowEpoch = currentEpoch > 3 ? currentEpoch - 3 : 0;
                    List<Identifier> sses = sharedSecrets.subList(lowEpoch * NUM_SIMULATED_DEVICES, currentEpoch * NUM_SIMULATED_DEVICES);
                    List<Identifier> eids = new ArrayList<>(sses.size());
                    for (Identifier ss : sses) {
                        eids.add(Coder.convertSharedSecretToID(ss));
                    }
                    List<String> topicHandles = ESClient.getInstance().getTopicHandles(eids);

                    List<ESMessage> msgsToSend = new ArrayList<>();
                    for (int i = 0; i < NUM_SIMULATED_DEVICES; i++) {
                        for (int j = currentEpoch; j > lowEpoch; j--) {
                            int oldIndex = ((j - 1 - lowEpoch) * NUM_SIMULATED_DEVICES) + i;
                            int newIndex = ((j - lowEpoch) * NUM_SIMULATED_DEVICES) + i;
                            if (topicHandles.get(oldIndex) == null || topicHandles.get(oldIndex).compareTo("") == 0) {
                                continue;
                            }
                            EpochLinkMessage epochLinkMessage = new EpochLinkMessage.EpochLinkMessageBuilder()
                                    .addOldNonce(nonces.get((lowEpoch * NUM_SIMULATED_DEVICES) + oldIndex).toString())
                                    .addNewNonce(nonces.get((lowEpoch * NUM_SIMULATED_DEVICES) + newIndex).toString())
                                    .build();
                            msgsToSend.add(new ESMessage(epochLinkMessage.toSendMessageText(sses.get(oldIndex).getBytes()),
                                    eids.get(oldIndex).toString(),
                                    topicHandles.get(oldIndex),
                                    true, null, true, -1));
                        }
                    }
                    ESClient.getInstance().sendMsgs(msgsToSend);
                    currentEpoch++;
                    // heh hack
                    if (currentEpoch == NUM_SIMULATED_EPOCHS) currentEpoch--;
                    handler.postDelayed(this, CHANGE_EPOCH_TIME);
                }).start();
            }
        }, CHANGE_EPOCH_TIME);
    }
}
