package org.mpisws.testapp.simulator;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Alarm extends BroadcastReceiver {
    public static final long WAKE_INTERVAL = (long) (15*60000);
    private static final String TAG = Alarm.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "BroadcastReceiver: onReceive(): " + System.currentTimeMillis());
        AlarmManager am =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        long ts = System.currentTimeMillis();
        long triggerAtMillis = (long) (ts + WAKE_INTERVAL);
        try {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pi);
            Log.d(TAG, "BroadcastReceiver: onReceive(): " + ts + "->" + triggerAtMillis);
        } catch (NullPointerException e) {
            Log.e(TAG, "BroadcastReceiver: onReceive() Exception" + System.currentTimeMillis() + e.getMessage());
        }
    }

    public void setAlarm(Context context)
    {
        AlarmManager am =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        long ts = System.currentTimeMillis();
        long triggerAtMillis = (long) (ts + WAKE_INTERVAL);
        Log.d(TAG, "BroadcastReceiver: setAlarm(): " + ts + "->" + triggerAtMillis);
        try {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pi);
            Log.d(TAG, "BroadcastReceiver: setAlarm(): " + ts + "->" + triggerAtMillis);
        } catch (NullPointerException e) {
            Log.e(TAG, "BroadcastReceiver: setAlarm() Exception" + System.currentTimeMillis() + e.getMessage());
        }
    }
}
