package org.mpisws.testapp.simulator;

/**
 * Created by tslilyai on 10/16/17.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Looper;
import android.util.Log;

import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.encounters.encounterformation.Advertiser;
import org.mpisws.helpers.Identifier;

import java.util.HashMap;
import java.util.Map;

import static org.mpisws.encounters.EncounterBasedCommunication.CHANGE_EPOCH_TIME;
import static org.mpisws.testapp.simulator.SimulationClientBT.NUM_SIMULATED_EPOCHS;
import static org.mpisws.testapp.simulator.SimulationClientBT.CURRENT_EPOCH;
import static org.mpisws.testapp.simulator.SimulationClientBT.mDHFullKeys;
import static org.mpisws.testapp.simulator.SimulationClientBT.mDHNonces;
import static org.mpisws.testapp.simulator.SimulationClientBT.mDHPubKeys;

/**
 * SDDR_Core implements the core functionality of the SDDR protocol. It is started and called by the
 * SDDR_Core_Service. The functionalities it provides are:
 * - running discovery
 * - processing and storing encounter information in persistent storage
 * - running epoch changes and confirmation
 */
public class SimulatorEncounterFormationCore implements Runnable {
    private static final String TAG = SimulatorEncounterFormationCore.class.getSimpleName();
    protected static Identifier mDHPubKey;
    protected static Identifier mNonce;
    protected static Identifier mDHFullKey;
    protected static Map<String, Pair<Long, Long>> ongoingEncounters = new HashMap<>();
    protected SimulatorEncounterConfirmationAndEpochLinking encounterConfirmationAndEpochLinking;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private Advertiser mAdvertiser;
    private SimulatorScannerProcessor mScannerProcessor;
    private long changeEpochTime;
    protected Context mService;

    public SimulatorEncounterFormationCore(Context context) {
        this.mService = context;
    }

    private void initialize() {
        Looper.prepare();
        Log.v(TAG, "Initialize SDDRCore on thread " + Thread.currentThread().getName());
        this.bluetoothManager = (BluetoothManager) mService.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mAdvertiser = new Advertiser();
        mScannerProcessor = new SimulatorScannerProcessor();

        mAdvertiser.initialize(mBluetoothAdapter);
        mScannerProcessor.initialize(mBluetoothAdapter, this);

        changeEpochTime = System.currentTimeMillis() + CHANGE_EPOCH_TIME;
        encounterConfirmationAndEpochLinking = new SimulatorEncounterConfirmationAndEpochLinking(mService);
    }

    public void run() {
        initialize();
        Log.v(TAG, "Running core on thread " + Thread.currentThread().getName());
        startAdvertisingAndUpdateAdvert();
        mScannerProcessor.startScanning();
    }

    public void stop() {
        mAdvertiser.stopAdvertising();
        mScannerProcessor.stopScanning();
    }

    private void startAdvertisingAndUpdateAdvert() {
        mDHPubKey = mDHPubKeys.get(CURRENT_EPOCH);
        mDHFullKey = mDHFullKeys.get(CURRENT_EPOCH);
        mNonce = mDHNonces.get(CURRENT_EPOCH);
        mAdvertiser.setAdData(mNonce.getBytes());
        mAdvertiser.resetAdvertiser();
    }

    public void postScanProcessing() {
        if (changeEpochTime < System.currentTimeMillis()) {
            Log.d(TAG, "CHANGING EPOCH");
            CURRENT_EPOCH++;
            if (CURRENT_EPOCH >= NUM_SIMULATED_EPOCHS) {
                // hack
                CURRENT_EPOCH--;
            }
            changeEpochTime += CHANGE_EPOCH_TIME;
            startAdvertisingAndUpdateAdvert();

            // add database entries for our new nonce pairs with all currently receiving adverts
            encounterConfirmationAndEpochLinking.insertNewNoncesIntoDatabase();
            // post our nonces on the appropriate "old nonce" encounter topic (the "oldest" topic for this encounter)
            encounterConfirmationAndEpochLinking.postUnpostedNoncesThatNeedLinking();
            // retrieve and link using the new nonces we had posted on our encounter topics
            encounterConfirmationAndEpochLinking.processNoncesForLinking();
            // because we're simulating, do the whole ES stuff
            encounterConfirmationAndEpochLinking.confirmEncountersViaES();
        }
    }

    public static Map<String, Pair<Long, Long>> getOngoingEncounters() {
        return ongoingEncounters;
    }
}
