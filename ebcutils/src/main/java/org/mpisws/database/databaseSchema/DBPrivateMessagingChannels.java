package org.mpisws.database.databaseSchema;

import org.mpisws.database.DBColumn;
import org.mpisws.database.DBModel;
import org.mpisws.database.DBValue;

import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import static org.mpisws.database.DBValue.TYPE.BYTES;
import static org.mpisws.database.DBValue.TYPE.LONG;
import static org.mpisws.database.DBValue.TYPE.STRING;

public class DBPrivateMessagingChannels extends DBModel {
    private long pkid;

    // set after confirmation
    private byte[] channelName;
    private byte[] sharedSecret;
    private byte[] otherPubKey;
    private String myTopicHandle;
    private String otherTopicHandle;

    private static DBPrivateMessagingChannels instance = new DBPrivateMessagingChannels();

    protected DBPrivateMessagingChannels(){};

    public DBPrivateMessagingChannels(long pkid, byte[] otherPubKey, byte[] sharedSecret, byte[] channelName,
                                      String myTopicHandle, String otherTopicHandle) {
        this.pkid = pkid;
        this.otherPubKey = otherPubKey;
        this.channelName = channelName;
        this.sharedSecret = sharedSecret;
        this.myTopicHandle = myTopicHandle;
        this.otherTopicHandle = otherTopicHandle;
    }

    public static DBPrivateMessagingChannels getInstance() {
        return instance;
    }

    public class Columns extends DBModel.Columns {
        public static final String sharedSecret = "ss";
        public static final String channelName = "channelName";
        public static final String myTopicHandle = "myTopicHandle";
        public static final String otherTopicHandle = "otherTopicHandle";
        public static final String otherPubKey = "otherPubKey";
    }

    @Override
    public String getTableName() {
        return DBPrivateMessagingChannels.class.getSimpleName();
    }

    @Override
    public List<DBColumn> getColumns() {
        final List<DBColumn> columns = new LinkedList<DBColumn>();
        columns.add(new DBColumn(Columns.pkid, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        columns.add(new DBColumn(Columns.sharedSecret, "BLOB"));
        columns.add(new DBColumn(Columns.channelName, "BLOB"));
        columns.add(new DBColumn(Columns.otherPubKey, "BLOB"));
        columns.add(new DBColumn(Columns.myTopicHandle, "CLOB"));
        columns.add(new DBColumn(Columns.otherTopicHandle, "CLOB"));
        return columns;
    }

    @Override
    public List<DBValue> getValues() {
        final List<DBValue> values = new LinkedList<>();
        values.add(new DBValue(Columns.pkid, LONG, this.pkid));
        values.add(new DBValue(Columns.otherPubKey, BYTES, this.otherPubKey));
        values.add(new DBValue(Columns.sharedSecret, BYTES, this.sharedSecret));
        values.add(new DBValue(Columns.channelName, BYTES, this.sharedSecret));
        values.add(new DBValue(Columns.myTopicHandle, STRING, this.myTopicHandle));
        values.add(new DBValue(Columns.otherTopicHandle, STRING, this.otherTopicHandle));
        return values;
    }

    public static String valsToString(Long pkid, byte[] otherPubKey, byte[] sharedSecret, byte[] channelName,
                                      String myTopicHandle, String otherTopicHandle)  {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(pkid)).append("\n");
        sb.append(Base64.getEncoder().encodeToString(otherPubKey)).append("\n");
        sb.append(Base64.getEncoder().encodeToString(sharedSecret)).append("\n");
        sb.append(Base64.getEncoder().encodeToString(channelName)).append("\n");
        sb.append(myTopicHandle).append("\n");
        sb.append(otherTopicHandle).append("\n");
        return sb.toString();
    }

    public static DBPrivateMessagingChannels fromString(String values) {
        String[] vals = values.split("\n");
        return new DBPrivateMessagingChannels(
                Long.parseLong(vals[0]),
                Base64.getDecoder().decode(vals[1]),
                Base64.getDecoder().decode(vals[2]),
                Base64.getDecoder().decode(vals[3]),
                vals[4],
                vals[5]);
    }
}
