package org.mpisws.database.databaseSchema;

import org.mpisws.database.DBColumn;
import org.mpisws.database.DBModel;
import org.mpisws.database.DBValue;

import java.util.LinkedList;
import java.util.List;

import static org.mpisws.database.DBValue.TYPE.DOUBLE;
import static org.mpisws.database.DBValue.TYPE.LONG;

public class DBLocation extends DBModel {
    long pkid;
    double latitude;
    double longitude;
    long timestamp;
    private static DBLocation instance = new DBLocation();

    protected DBLocation(){};
    public static DBLocation getInstance(){ return instance; };

    public DBLocation(long pkid, double latitude, double longitude, long timestamp) {
        this.pkid = pkid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    public class Columns extends DBModel.Columns {
        public static final String userHandle = "userHandle";
        public static final String latitude = "loclat";
        public static final String longitude = "loclong";
        public static final String updatedTimestamp = "loctime";
    }

    @Override
    public String getTableName() {
        return DBLocation.class.getSimpleName();
    }

    @Override
    public List<DBColumn> getColumns() {
        final List<DBColumn> columns = new LinkedList<DBColumn>();
        columns.add(new DBColumn(Columns.pkid, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        columns.add(new DBColumn(Columns.userHandle, "CLOB"));
        columns.add(new DBColumn(Columns.latitude, "REAL"));
        columns.add(new DBColumn(Columns.longitude, "REAL"));
        columns.add(new DBColumn(Columns.updatedTimestamp, "INTEGER"));
        return columns;
    }

    @Override
    public List<DBValue> getValues() {
        final List<DBValue> values = new LinkedList<>();
        values.add(new DBValue(Columns.pkid, LONG, this.pkid));
        values.add(new DBValue(Columns.latitude, DOUBLE, this.latitude));
        values.add(new DBValue(Columns.longitude, DOUBLE, this.longitude));
        values.add(new DBValue(Columns.updatedTimestamp, LONG, this.timestamp));
        return values;
    }

   /*public static DBLocation fromString(String values) {
        String[] vals = values.split("\n");
        // TODO
        return new DBLocation(
                -1,//Long.parseLong(vals[0]),
                Double.parseDouble(vals[1]),
                Double.parseDouble(vals[2]),
                Long.parseLong(vals[3]));
    }*/
}
