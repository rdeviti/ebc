package org.mpisws.database.databaseSchema;

/**
 * Created by tslilyai on 2/19/18.
 */
import org.mpisws.database.DBColumn;
import org.mpisws.database.DBModel;
import org.mpisws.database.DBValue;

import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import static org.mpisws.database.DBValue.TYPE.BOOL;
import static org.mpisws.database.DBValue.TYPE.BYTES;
import static org.mpisws.database.DBValue.TYPE.LONG;
import static org.mpisws.database.DBValue.TYPE.STRING;

public class DBEncounterEntries extends DBModel {
    private long pkid;
    private long encounterPKID; // links entries to one encounter
    private long timestamp_start;
    private long timestamp_end;
    private String userHandle;

    // to confirm, we need to post our public DH key
    private boolean postedNonceTopic;

    // to link, we need to post this nonce to an older entry's confirmed topic
    private boolean shouldPostNonceToLink;

    // set after confirmation
    private byte[] encounterID;
    private byte[] sharedSecret;
    private String myTopicHandle;
    private String otherTopicHandle;

    // my-nonce + receivedNonce = one unique entry
    private byte[] receivedNonce;
    private byte[] myNonce;
    private byte[] myDHPrivKey;
    private byte[] myDHPubKey;

    private static DBEncounterEntries instance = new DBEncounterEntries();

    protected DBEncounterEntries(){};

    public DBEncounterEntries(long pkid, long encounterPKID, long timestamp_start, long timestamp_end,
                              byte[] sharedSecret, byte[] encounterID,
                              byte[] myNonce, byte[] myDHPrivKey, byte[]myDHPubKey, byte[] receivedNonce,
                              boolean postedNonceTopic, boolean shouldPostNonceToLink,
                              String myTopicHandle, String otherTopicHandle, String userHandle) {
        this.pkid = pkid;
        this.encounterPKID = encounterPKID;
        this.sharedSecret = sharedSecret;
        this.encounterID = encounterID;
        this.timestamp_start = timestamp_start;
        this.timestamp_end = timestamp_end;
        this.myNonce = myNonce;
        this.myDHPrivKey = myDHPrivKey;
        this.myDHPubKey = myDHPubKey;
        this.receivedNonce = receivedNonce;
        this.postedNonceTopic = postedNonceTopic;
        this.myTopicHandle = myTopicHandle;
        this.otherTopicHandle = otherTopicHandle;
        this.shouldPostNonceToLink = shouldPostNonceToLink;
        this.userHandle = userHandle;
    }

    public static DBEncounterEntries getInstance() {
        return instance;
    }

    public class Columns extends DBModel.Columns {
        public static final String encounterPKID = "encPKID";
        public static final String sharedSecret = "ss";
        public static final String encounterID = "encID";
        public static final String timestampStart = "timestart";
        public static final String timestampEnd = "timeend";
        public static final String myNonce = "myNonce";
        public static final String myDHPrivKey = "mydhprivkey";
        public static final String myDHPubKey = "mydhpubkey";
        public static final String receivedNonce = "recievedNonce";
        public static final String postedNonceTopic = "postedNonceTopic";
        public static final String myTopicHandle = "myTopicHandle";
        public static final String otherTopicHandle = "otherTopicHandle";
        public static final String shouldPostNonceToLink = "shouldPostNonceToLink";
        public static final String userHandle = "userHandle";
    }

    @Override
    public String getTableName() {
        return DBEncounterEntries.class.getSimpleName();
    }
    @Override
    public List<DBColumn> getColumns() {
        final List<DBColumn> columns = new LinkedList<DBColumn>();
        columns.add(new DBColumn(Columns.pkid, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        columns.add(new DBColumn(Columns.encounterPKID, "INTEGER"));
        columns.add(new DBColumn(Columns.sharedSecret, "BLOB"));
        columns.add(new DBColumn(Columns.encounterID, "BLOB"));
        columns.add(new DBColumn(Columns.timestampStart, "INTEGER"));
        columns.add(new DBColumn(Columns.timestampEnd, "INTEGER"));
        columns.add(new DBColumn(Columns.myNonce, "BLOB"));
        columns.add(new DBColumn(Columns.myDHPrivKey, "BLOB"));
        columns.add(new DBColumn(Columns.myDHPubKey, "BLOB"));
        columns.add(new DBColumn(Columns.receivedNonce, "BLOB"));
        columns.add(new DBColumn(Columns.postedNonceTopic, "INTEGER"));
        columns.add(new DBColumn(Columns.myTopicHandle, "CLOB"));
        columns.add(new DBColumn(Columns.otherTopicHandle, "CLOB"));
        columns.add(new DBColumn(Columns.shouldPostNonceToLink, "INTEGER"));
        columns.add(new DBColumn(Columns.userHandle, "CLOB"));
        return columns;
    }

    @Override
    public List<DBValue> getValues() {
        final List<DBValue> values = new LinkedList<>();
        values.add(new DBValue(Columns.pkid, LONG, this.pkid));
        values.add(new DBValue(Columns.encounterPKID, LONG, this.encounterPKID));
        values.add(new DBValue(Columns.timestampStart, LONG, this.timestamp_start));
        values.add(new DBValue(Columns.timestampEnd, LONG, this.timestamp_end));
        values.add(new DBValue(Columns.sharedSecret, BYTES, this.sharedSecret));
        values.add(new DBValue(Columns.encounterID, BYTES, this.encounterID));
        values.add(new DBValue(Columns.myNonce, BYTES, this.myNonce));
        values.add(new DBValue(Columns.myDHPrivKey, BYTES, this.myDHPrivKey));
        values.add(new DBValue(Columns.myDHPubKey, BYTES, this.myDHPubKey));
        values.add(new DBValue(Columns.receivedNonce, BYTES, this.receivedNonce));
        values.add(new DBValue(Columns.postedNonceTopic, BOOL, this.postedNonceTopic));
        values.add(new DBValue(Columns.shouldPostNonceToLink, BOOL, this.shouldPostNonceToLink));
        values.add(new DBValue(Columns.myTopicHandle, STRING, this.myTopicHandle));
        values.add(new DBValue(Columns.otherTopicHandle, STRING, this.otherTopicHandle));
        return values;
    }

    @Override
    public String getUnique() {
        return ", UNIQUE(" + Columns.myNonce + "," + Columns.receivedNonce + ")";
    }


    /*public static DBEncounterEntries fromString(String values) {
        String[] vals = values.split("\n");
        return new DBEncounterEntries(
                Long.parseLong(vals[0]),
                Long.parseLong(vals[1]),
                Long.parseLong(vals[2]),
                Long.parseLong(vals[3]),
                Base64.getDecoder().decode(vals[4]),
                Base64.getDecoder().decode(vals[5]),
                Base64.getDecoder().decode(vals[6]),
                Base64.getDecoder().decode(vals[7]),
                Base64.getDecoder().decode(vals[8]),
                Base64.getDecoder().decode(vals[9]),
                Boolean.parseBoolean(vals[10]),
                Boolean.parseBoolean(vals[11]),
                vals[10],
                vals[11],
                vals[12]);
    }*/
}
