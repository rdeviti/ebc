package org.mpisws.database.databaseSchema;

import org.mpisws.database.DBColumn;
import org.mpisws.database.DBModel;
import org.mpisws.database.DBValue;

import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import static org.mpisws.database.DBValue.TYPE.BYTES;
import static org.mpisws.database.DBValue.TYPE.LONG;
import static org.mpisws.database.DBValue.TYPE.STRING;

public class DBInitiatePrivateMessagingChannels extends DBModel {
    private long pkid;

    private String pubKeyTopicHandle;
    private byte[] myDHPrivKey;
    private byte[] myDHPubKey;

    private static DBInitiatePrivateMessagingChannels instance = new DBInitiatePrivateMessagingChannels();

    protected DBInitiatePrivateMessagingChannels(){};

    public DBInitiatePrivateMessagingChannels(long pkid, byte[] myDHPrivKey, byte[]myDHPubKey, String pubKeyTopicHandle) {
        this.pkid = pkid;
        this.myDHPrivKey = myDHPrivKey;
        this.myDHPubKey = myDHPubKey;
        this.pubKeyTopicHandle = pubKeyTopicHandle;
    }

    public static DBInitiatePrivateMessagingChannels getInstance() {
        return instance;
    }

    public class Columns extends DBModel.Columns {
        public static final String myDHPrivKey = "mydhprivkey";
        public static final String myDHPubKey = "mydhpubkey";
        public static final String pubKeyTopicHandle = "topicHandle";
    }

    @Override
    public String getTableName() {
        return DBInitiatePrivateMessagingChannels.class.getSimpleName();
    }

    @Override
    public List<DBColumn> getColumns() {
        final List<DBColumn> columns = new LinkedList<DBColumn>();
        columns.add(new DBColumn(Columns.pkid, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        columns.add(new DBColumn(Columns.myDHPrivKey, "BLOB"));
        columns.add(new DBColumn(Columns.myDHPubKey, "BLOB"));
        columns.add(new DBColumn(Columns.pubKeyTopicHandle, "CLOB"));
        return columns;
    }

    @Override
    public List<DBValue> getValues() {
        final List<DBValue> values = new LinkedList<>();
        values.add(new DBValue(Columns.pkid, LONG, this.pkid));
        values.add(new DBValue(Columns.myDHPrivKey, BYTES, this.myDHPrivKey));
        values.add(new DBValue(Columns.myDHPubKey, BYTES, this.myDHPubKey));
        values.add(new DBValue(Columns.pubKeyTopicHandle, STRING, this.pubKeyTopicHandle));
        return values;
    }

    public static String valsToString(Long pkid, byte[] myDHPrivKey, byte[]myDHPubKey,
                                      String pubKeyTopicHandle)  {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(pkid)).append("\n");
        sb.append(Base64.getEncoder().encodeToString(myDHPubKey)).append("\n");
        sb.append(Base64.getEncoder().encodeToString(myDHPrivKey)).append("\n");
        sb.append(pubKeyTopicHandle).append("\n");
        return sb.toString();
    }

    public static DBInitiatePrivateMessagingChannels fromString(String values) {
        String[] vals = values.split("\n");
        return new DBInitiatePrivateMessagingChannels(
                Long.parseLong(vals[0]),
                Base64.getDecoder().decode(vals[1]),
                Base64.getDecoder().decode(vals[2]),
                vals[3]);
    }
}
