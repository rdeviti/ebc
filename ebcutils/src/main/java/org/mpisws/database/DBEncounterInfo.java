package org.mpisws.database;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;

/**
 * Created by tslilyai on 2/20/18.
 */

/**
 * Contains information about an encounter (retrieved from the encounter database)
 */
public class DBEncounterInfo implements Serializable {

    /**
     * Get the length that this encounter intersected with the constraint used in the query
     * @return Duration of overlap with query constraint
     */
    public Pair<Long, Long> getTimeInterval() {
        return new ImmutablePair<>(startTime, endTime);
    }

    /**
     * Get the database PKID for this encounter
     * @return The database encounter PKID
     */
    public long getEncounterPKID() {
        return encounterPKID;
    }

    /**
     * Get the topic handle of my side of this encounter thread
     * @return The topic handle (if one exists)
     */
    public String getMyTopicHandle() {
        return myTopicHandle;
    }

    /**
     * Get the topic handle of my side of this encounter thread
     * @return The topic handle (if one exists)
     */
    public String getOtherTopicHandle() {
        return otherTopicHandle;
    }

    /**
     * Get the unique encounter ID of this encounter
     * @return The encounter ID
     */
    public byte[] getEid() {
        return eid;
    }

    /**
     * Get the unique shared secret used to encrypt communication with this encounter
     * @return The shared secret
     */
    public byte[] getSs() {
        return ss;
    }

    private long startTime;
    private long endTime;
    private long encounterPKID;
    private byte[] eid;
    private byte[] ss;
    private String myTopicHandle;
    private String otherTopicHandle;

    public DBEncounterInfo(long curPKPKID, byte[] bytes, byte[] bytes1, long l1, long l2, String topicHandle, String otherTopicHandle) {
        this.encounterPKID = curPKPKID;
        this.eid = bytes;
        this.ss = bytes1;
        this.startTime = l1;
        this.endTime = l2;
        this.myTopicHandle = topicHandle;
        this.otherTopicHandle = otherTopicHandle;
    }

    public void setOtherTopicHandle(String otherTopicHandle) {
        this.otherTopicHandle = otherTopicHandle;
    }
}
