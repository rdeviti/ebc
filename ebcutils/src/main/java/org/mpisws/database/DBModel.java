package org.mpisws.database;

import java.util.List;

/**
 * Created by tslilyai on 2/18/18.
 */
public abstract class DBModel {
    public class Columns {
        public static final String pkid = "_id";
    }

    public abstract String getTableName();
    public abstract List<DBColumn> getColumns();
    public abstract List<DBValue> getValues();
    public String getUnique() {return "";}
}
