package org.mpisws.database;

public class DBValue<T> {

    public static enum TYPE {
        LONG,
        DOUBLE,
        BYTES,
        BOOL,
        STRING
    }
    private final String name;
    private final T value;
    private final TYPE typ;

    public DBValue(String name, TYPE typ, T value) {
        this.name = name;
        this.value = value;
        this.typ = typ;
    }

    public String getName() {
        return name;
    }

    public TYPE getValueType() {
        return typ;
    }

    public T getValue() {
        return value;
    }
}