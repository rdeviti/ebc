package org.mpisws.messaging;

import org.mpisws.helpers.Coder;

import java.io.Serializable;

public class PrivateMessage implements Serializable {
    public static final String FLAG = "*****";
    public static final String FLAG_PATT = java.util.regex.Pattern.quote(FLAG);
    String message;

    public static class MessageChannelMessageBuilder {
        private PrivateMessage privateMessage = new PrivateMessage();
        public MessageChannelMessageBuilder addMessage(String message) {
            privateMessage.message = message;
            return this;
        }
        public PrivateMessage build() {
            if (privateMessage.message == null) {
                throw new IllegalArgumentException("Message cannot be null");
            }
            return privateMessage;
        }
    }

    public String getMsgText() {
        return message;
    }

    public String toSendMessageText(byte[] secret) {
        return Coder.encrypt(FLAG + message, secret);
    }

    public static PrivateMessage fromReceivedMessageText(String msgText) {
        String message = msgText.split(FLAG_PATT)[1];
        return new MessageChannelMessageBuilder().addMessage(message).build();
    }
}
