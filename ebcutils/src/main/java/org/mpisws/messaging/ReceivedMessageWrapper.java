package org.mpisws.messaging;

import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Utils;

import java.io.Serializable;

import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.ENCOUNTER_FORWARDING_MESSAGE;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.EPOCH_LINK;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.INITIATE_PRIVATE_MESSAGE;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.NONE;
import static org.mpisws.messaging.ReceivedMessageWrapper.MsgTyp.PRIVATE_MESSAGE;

public class ReceivedMessageWrapper implements Serializable {

    public enum MsgTyp {
        EPOCH_LINK,
        PRIVATE_MESSAGE,
        INITIATE_PRIVATE_MESSAGE,
        ENCOUNTER_FORWARDING_MESSAGE,
        NONE
    }

    MsgTyp msgTyp;
    Object message;
    ESMessage originalESMessage;
    byte[] secret;

    public boolean getIsMe() {
        return originalESMessage.getIsMe();
    }

    public MsgTyp getMsgType() {
        return msgTyp;
    }

    public String getTopicHandle() {
        return originalESMessage.getTopicHandle();
    }

    public String getCursor() {
        return originalESMessage.getCursor();
    }

    public String getEncounterID() { return originalESMessage.getTopicText(); }

    public long getTimestamp() { return originalESMessage.getTimestamp(); }

    public EpochLinkMessage getEpochLinkMessage() {
        Utils.myAssert(msgTyp == EPOCH_LINK);
        return EpochLinkMessage.class.cast(message);
    }

    public EncounterForwardingMessage getEncounterForwardingMessage() {
        Utils.myAssert(msgTyp == ENCOUNTER_FORWARDING_MESSAGE);
        return EncounterForwardingMessage.class.cast(message);
    }

    public InitiatePrivateMessagingMessage getInitiateMessageChannelMessage() {
        Utils.myAssert(msgTyp == INITIATE_PRIVATE_MESSAGE);
        return InitiatePrivateMessagingMessage.class.cast(message);
    }

    public PrivateMessage getPrivateMessage() {
        Utils.myAssert(msgTyp == PRIVATE_MESSAGE);
        return PrivateMessage.class.cast(message);
    }

    public ReceivedMessageWrapper(ESMessage esMsg, byte[] secret) {
        this.secret = secret;
        this.originalESMessage = esMsg;
        if (esMsg.getMsgText() == null) return;
        String decryptedMsgTxt = Coder.decrypt(esMsg.getMsgText(), secret);
        if (decryptedMsgTxt.split(EncounterForwardingMessage.DELIM_PATT).length == 4) {
            this.msgTyp = ENCOUNTER_FORWARDING_MESSAGE;
            this.message = EncounterForwardingMessage.fromReceivedMessageText(decryptedMsgTxt);
        } else if (decryptedMsgTxt.split(EpochLinkMessage.DELIM_PATT).length == 2) {
            this.msgTyp = EPOCH_LINK;
            this.message = EpochLinkMessage.fromReceivedMessageText(decryptedMsgTxt);
        } else if (decryptedMsgTxt.contains(InitiatePrivateMessagingMessage.FLAG)){
            this.msgTyp = INITIATE_PRIVATE_MESSAGE;
            this.message = InitiatePrivateMessagingMessage.fromReceivedMessageText(decryptedMsgTxt);
        } else if (decryptedMsgTxt.contains(PrivateMessage.FLAG)){
             this.msgTyp = PRIVATE_MESSAGE;
             this.message = PrivateMessage.fromReceivedMessageText(decryptedMsgTxt);
        } else{
            this.msgTyp = NONE;
        }
        System.out.println("RECEIVED_MESSAGE: " + this.msgTyp + ", message " + decryptedMsgTxt);
    }
}
