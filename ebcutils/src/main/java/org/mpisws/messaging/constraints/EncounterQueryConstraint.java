package org.mpisws.messaging.constraints;

import org.mpisws.database.DBValue;

import java.io.Serializable;

/**
 * Created by tslilyai on 2/19/18.
 */

/**
 * Abstract class that specifies a time/space/trajectory constraint.
 */
public abstract class EncounterQueryConstraint implements Serializable {
    public final static String STDELIMITER = "SPACETIME";
    public final static String TRAJDELIMITER = "TRAJ";
    public final static String ALLMARKER = "ALL";

    public Long duration = 0L;
    protected boolean causal = false;
    public boolean useCausalityConstraint() {
        return causal;
    }

    /**
     * Turn the constraint into a query on the encounter database
     * @return A SQL query representing the constraint
     */
    public abstract String toQueryString();

    /**
     * SpaceRegion specifies a given latitude, longitude, and radius around the location point
     */
    public static class SpaceRegion implements Serializable {
        /**
         * Construct a new SpaceRegion with a given location and radius
         * @param latitude
         * @param longitude
         * @param radius
         */
        public SpaceRegion(double latitude, double longitude, double radius) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
        }

        private double latitude;

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getRadius() {
            return radius;
        }

        private double longitude;
        private double radius;
    }
}
