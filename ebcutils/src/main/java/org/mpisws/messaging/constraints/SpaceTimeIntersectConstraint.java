package org.mpisws.messaging.constraints;

import org.mpisws.database.databaseSchema.DBLocation;
import org.mpisws.database.databaseSchema.DBEncounterEntries;

import java.io.Serializable;

/**
 * Created by tslilyai on 2/19/18.
 */

/**
 *
 */
public class SpaceTimeIntersectConstraint extends org.mpisws.messaging.constraints.EncounterQueryConstraint implements Serializable {
     /**
     * Construct a space-time region constraint.
     *
     * If causalEncounterStartTime is not null, the constraint selects all encounters whose end times occur after, and whose start times occur before the specified start time.
     * @param timestampStart The start of the time interval that the encounter must intersect
     * @param timestampEnd The end of the time interval that the encounter must intersect
     * @param overlapDuration The minimum period for which an encounter must overlap with the specified time-space region
     * @param spaceRegion The region of space the encounter must intersect
     * @param causal [Optional] The start time of the encounter that must have a
     *                           happens-before relationship with all encounters satisfying the constraint
     */
    public SpaceTimeIntersectConstraint(long timestampStart, long timestampEnd, long overlapDuration, SpaceRegion spaceRegion, Boolean causal) {
        this.timestampStart = timestampStart;
        this.timestampEnd = timestampEnd;
        this.duration = overlapDuration;
        this.spaceRegion = spaceRegion;
        this.causal = causal;
    }

    private long timestampStart;
    private long timestampEnd;
    private SpaceRegion spaceRegion;

    @Override
    public String toQueryString() {
        if (spaceRegion != null)
            return this.useCausalityConstraint()
                + STDELIMITER + timestampStart
                + STDELIMITER + timestampEnd
                + STDELIMITER + spaceRegion.getLatitude()
                + STDELIMITER + spaceRegion.getLongitude()
                + STDELIMITER + spaceRegion.getRadius();
        return this.useCausalityConstraint()
                + STDELIMITER + timestampStart
                + STDELIMITER + timestampEnd
                + STDELIMITER + ""
                + STDELIMITER + ""
                + STDELIMITER + "";
        /*String locTableName = DBLocation.getInstance().getTableName();
        String encTableName = DBEncounterEntries.getInstance().getTableName();

        // select the encounterPKID, newNonce, sharedSecret, and updatedTimestamp of this location entry
        String selectStr = String.format("SELECT %s.%s, quote(%s.%s), %s.%s, %s.%s FROM %s",
                encTableName, DBEncounterEntries.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.sharedSecret,
                encTableName, DBEncounterEntries.Columns.myTopicHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName);
        selectStr += String.format(" INNER JOIN %s ON (%s.%s = %s.%s) AND (%s.%s BETWEEN %s.%s AND %s.%s) OR (%s.%s = -1 AND %s.%s > %s.%s) ",
                locTableName,
                locTableName, DBLocation.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.userHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart);

        // only get confirmed encounters
        String whereEncStr = String.format("(%s.%s IS NOT NULL)", encTableName, DBEncounterEntries.Columns.sharedSecret);

        // get encounters whose duration overlaps long enough
        String whereTimeStrNoEnd = String.format("(%s.%s = -1 AND %s.%s >= %s AND %s.%s <= %s AND %s - %s.%s >= %s)",
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampEnd,
                this.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampStart, duration);
        String whereTimeStrEnded = String.format("(" +
                        "(%s.%s >= %s AND %s.%s <= %s AND %s >= %s.%s AND %s.%s - %s.%s >= %s)" +
                        " OR (%s.%s >= %s AND %s.%s <= %s AND %s < %s.%s AND %s - %s.%s >= %s)" +
                        " OR (%s.%s >= %s AND %s.%s <= %s AND %s >= %s.%s AND %s.%s - %s >= %s)" +
                        " OR (%s.%s >= %s AND %s.%s <= %s AND %s < %s.%s AND %s.%s - %s.%s >= %s)" +
                        ")",
                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampEnd,
                this.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampEnd,
                encTableName, DBEncounterEntries.Columns.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampStart, duration,

                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampStart, this.timestampEnd,
                this.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampEnd,
                this.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampStart, duration,

                encTableName, DBEncounterEntries.Columns.timestampEnd, this.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd, this.timestampEnd,
                this.timestampStart, encTableName, DBEncounterEntries.Columns.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd, this.timestampStart, duration,

                encTableName, DBEncounterEntries.Columns.timestampEnd, this.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd, this.timestampEnd,
                this.timestampStart, encTableName, DBEncounterEntries.Columns.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd, encTableName, DBEncounterEntries.Columns.timestampStart, duration);
        String whereTimeStr = "("+ whereTimeStrEnded + " OR " + whereTimeStrNoEnd + ")";

        String finalQuery = selectStr + " WHERE " + whereEncStr + " AND " + whereTimeStr;

        // constrain by being in the same location
        if (spaceRegion != null) {
            finalQuery += " AND " +
                    String.format("(%s.%s BETWEEN %s AND %s) AND (%s.%s BETWEEN %s AND %s)",
                        locTableName, DBLocation.Columns.latitude, spaceRegion.getLatitude() - spaceRegion.getRadius(), spaceRegion.getLatitude() + spaceRegion.getRadius(),
                        locTableName, DBLocation.Columns.longitude, spaceRegion.getLongitude() - spaceRegion.getRadius(), spaceRegion.getLongitude() + spaceRegion.getRadius());
        }

        // should we constrain for causality
        if (useCausalityConstraint()) {
            String whereCausalityStr = String.format("%s.%s >= %s AND %s.%s <= %s",
                    encTableName, DBEncounterEntries.Columns.timestampEnd, getReceivedFromEncStartingTimeMs(),
                    encTableName, DBEncounterEntries.Columns.timestampStart, getReceivedFromEncStartingTimeMs());
            finalQuery += " AND " + whereCausalityStr;
        }

        finalQuery += " ORDER BY " + encTableName + "." + DBEncounterEntries.Columns.sharedSecret
                + ", " + encTableName + "." + DBEncounterEntries.Columns.userHandle
                + ", " + locTableName + "." + DBLocation.Columns.updatedTimestamp;
        System.out.println("Executing SQL query: " + finalQuery);
        return finalQuery;*/
    }
}
