package org.mpisws.messaging.constraints;

import java.io.Serializable;

/**
 * Created by tslilyai on 2/19/18.
 */

public class ForwardingConstraint implements Serializable {
    public static final int EXPLOSION_LIMIT = 10000;

    public ForwardingConstraint(int hopsRemaining, int fanoutLimit, long lifetimeMs) {
        this.hopsRemaining = hopsRemaining;
        this.fanoutLimit = fanoutLimit;
        this.creationTimeMs = System.currentTimeMillis();
        this.lifetimeMs = lifetimeMs;
    }

    public boolean shouldForward() {
        return hopsRemaining > 0 && !expired();
    }

    public int getFanoutLimit() {
        return fanoutLimit;
    }

    public int getHopCount() {
        return hopsRemaining;
    }

    public long getExpirationTime() {
        return this.creationTimeMs + lifetimeMs;
    }

    public void update() {
        hopsRemaining--;
    }

    public boolean expired() {
        // the current time is greater than the lifetime
        return this.creationTimeMs + lifetimeMs < System.currentTimeMillis();
    }

    private int hopsRemaining;
    private int fanoutLimit;
    private long creationTimeMs;
    private long lifetimeMs;
}
