package org.mpisws.messaging.constraints;

import org.mpisws.database.databaseSchema.DBEncounterEntries;
import org.mpisws.database.databaseSchema.DBLocation;

public class AllEncountersConstraint extends org.mpisws.messaging.constraints.EncounterQueryConstraint {
    @Override
    public String toQueryString() {
        return ALLMARKER;
        /*String locTableName = DBLocation.getInstance().getTableName();
        String encTableName = DBEncounterEntries.getInstance().getTableName();

        // select the encounterPKID, newNonce, sharedSecret, and updatedTimestamp of this location entry
        String selectStr = String.format("SELECT %s.%s, quote(%s.%s), %s.%s, %s.%s FROM %s",
                encTableName, DBEncounterEntries.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.sharedSecret,
                encTableName, DBEncounterEntries.Columns.myTopicHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName);
        selectStr += String.format(" INNER JOIN %s ON (%s.%s = %s.%s) AND (%s.%s BETWEEN %s.%s AND %s.%s) OR (%s.%s = -1 AND %s.%s > %s.%s) ",
                locTableName,
                locTableName, DBLocation.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.userHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart);

        // only get confirmed encounters
        String whereEncStr = String.format("(%s.%s IS NOT NULL)", encTableName, DBEncounterEntries.Columns.sharedSecret);
        String finalQuery = selectStr + " WHERE " + whereEncStr ;
        finalQuery += " ORDER BY " + encTableName + "." + DBEncounterEntries.Columns.sharedSecret
                + ", " + encTableName + "." + DBEncounterEntries.Columns.userHandle
                + ", " + locTableName + "." + DBLocation.Columns.updatedTimestamp;
        System.out.println("Executing SQL query: " + finalQuery);
        return finalQuery;*/
    }
}
