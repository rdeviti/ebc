package org.mpisws.messaging.constraints;

import java.io.Serializable;

/**
 * Created by tslilyai on 2/19/18.
 */

/**
 * TrajectoryConstraint allows one to specify a constraint based on a particular trajectory, or
 * a sequence of time+location elements.
 */
public class TrajectoryConstraint extends EncounterQueryConstraint implements Serializable {
    // allow 10s for when timestamps for locations were put
    private static final int TIMESTAMP_BUFFER = 10000;

    /**
     * Construct a trajectory constraint using a sequence of trajectory elements and
     * optionally a minimum period for which an encounter satisfying the constraint must overlap with the specified trajectory.
     * If all elements contain timestamps and a minimum period is specified, then
     * the constraint specifies a trajectory intersection constraint.
     * Otherwise, if a minimum period is not specified or certain elements may not contain timestamps, then
     * the constraint specifies a trajectory projection constraint.
     *
     * If causalEncounterStartTime is not null, the constraint selects all encounters whose end times occur after, and whose start times occur before the specified start time.
     * @param startTime
     * @param endTime
     * @param duration The minimum period for which an encounter must overlap with the specified trajectory.
     * @param causal [Optional] The start time of the encounter that must have a
     *                                 happens-before relationship with all encounters satisfying the constraint.
     * @param projectIntoFuture Whether the constraint should match all future encounters along the trajectory or simply
     *                          those that occurred during the time the sender was there
     */
    public TrajectoryConstraint(Long startTime, Long endTime, Long duration, Boolean causal, Boolean projectIntoFuture) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = duration;
        this.projectIntoFuture = projectIntoFuture || duration == null;
        this.causal = causal;
    }

    boolean projectIntoFuture;
    long startTime;
    long endTime;

    @Override
    public String toQueryString() {
        return projectIntoFuture
                + TRAJDELIMITER + startTime
                + TRAJDELIMITER + endTime;

        /*String encTableName = DBEncounterEntries.getInstance().getTableName();
        String locTableName = DBLocation.getInstance().getTableName();

        // select the encounterPKID, newNonce, sharedSecret, and updatedTimestamp of this location entry
        String selectStr = String.format("SELECT %s.%s, quote(%s.%s), %s.%s, %s.%s FROM %s",
                encTableName, DBEncounterEntries.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.sharedSecret,
                encTableName, DBEncounterEntries.Columns.myTopicHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName);
        selectStr += String.format(" INNER JOIN %s ON (%s.%s = %s.%s) AND (%s.%s BETWEEN %s.%s AND %s.%s) OR (%s.%s = -1 AND %s.%s > %s.%s) ",
                locTableName,
                locTableName, DBLocation.Columns.userHandle,
                encTableName, DBEncounterEntries.Columns.userHandle,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                encTableName, DBEncounterEntries.Columns.timestampEnd,
                locTableName, DBLocation.Columns.updatedTimestamp,
                encTableName, DBEncounterEntries.Columns.timestampStart);

        // only get confirmed encounters
        String whereEncStr = String.format("(%s.%s IS NOT NULL)", encTableName, DBEncounterEntries.Columns.sharedSecret);

        // these filter down to only those encounter+location rows that actually intersect with the trajectory in question
        List<String> whereLocTimeStrsOr = new ArrayList<>();
        for (TrajectoryElement trajectoryElement : trajectoryElts) {
            SpaceRegion spaceRegion = trajectoryElement.location;
            String whereLocsStr, whereTimeStr;

            whereLocsStr = String.format("(%s.%s BETWEEN %s AND %s) AND (%s.%s BETWEEN %s AND %s)",
                locTableName, DBLocation.Columns.latitude, spaceRegion.getLatitude()-spaceRegion.getRadius(), spaceRegion.getLatitude()+spaceRegion.getRadius(),
                locTableName, DBLocation.Columns.longitude, spaceRegion.getLongitude()-spaceRegion.getRadius(), spaceRegion.getLongitude()+spaceRegion.getRadius());

            // should we care about the time at which you are at the same location
            if (!projectIntoFuture) {
                whereTimeStr = String.format("(%s.%s BETWEEN %s AND %s)",
                    locTableName, DBLocation.Columns.updatedTimestamp, trajectoryElement.timestamp-TIMESTAMP_BUFFER, trajectoryElement.timestamp+TIMESTAMP_BUFFER);
            } else {
                whereTimeStr = String.format("(%s.%s >= %s)",
                    locTableName, DBLocation.Columns.updatedTimestamp, trajectoryElement.timestamp);
            }
            whereLocTimeStrsOr.add(String.format("(%s AND %s)", whereLocsStr, whereTimeStr));
        }
        String whereLocTimeStr = StringUtils.join(whereLocTimeStrsOr, " OR ");

        String finalQuery = selectStr + " WHERE " + whereEncStr;
        if (whereLocTimeStr.length() > 0) {
            finalQuery += " AND (" + whereLocTimeStr + ") ";
        }
        // should we constrain for causality
        if (useCausalityConstraint()) {
            String whereCausalityStr = String.format("%s.%s >= %s AND %s.%s <= %s",
                    encTableName, DBEncounterEntries.Columns.timestampEnd, getReceivedFromEncStartingTimeMs(),
                    encTableName, DBEncounterEntries.Columns.timestampStart, getReceivedFromEncStartingTimeMs());
            finalQuery += " AND " + whereCausalityStr;
        }

        finalQuery += " ORDER BY " + encTableName + "." + DBEncounterEntries.Columns.sharedSecret
                    + ", " + encTableName + "." + DBEncounterEntries.Columns.userHandle
                    + ", " + locTableName + "." + DBLocation.Columns.updatedTimestamp;
        System.out.println("Executing SQL query: " + finalQuery);
        return finalQuery;*/
     }
}
