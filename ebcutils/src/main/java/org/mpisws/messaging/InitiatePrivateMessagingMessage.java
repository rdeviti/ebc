package org.mpisws.messaging;

import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.Utils;

import java.io.Serializable;

public class InitiatePrivateMessagingMessage implements Serializable {
    public static final String FLAG = "&^&^&^&^&^";
    public static final String FLAG_PATT = java.util.regex.Pattern.quote(FLAG);

    Identifier publicKey;

    public Identifier getPublicKey() {
        return publicKey;
    }

    public static class InitiateMessageChannelMessageBuilder {
        private InitiatePrivateMessagingMessage initiateMessageChannelMessage = new InitiatePrivateMessagingMessage();
        public InitiateMessageChannelMessageBuilder addPublicKey(Identifier publicKey) {
            initiateMessageChannelMessage.publicKey = publicKey;
            return this;
        }
        public InitiatePrivateMessagingMessage build() {
            if (initiateMessageChannelMessage.publicKey == null) {
                throw new IllegalArgumentException("Public key cannot be null");
            }
            return initiateMessageChannelMessage;
        }
    }

    public String toSendMessageText() {
        return FLAG + publicKey.toString();
    }

    public static InitiatePrivateMessagingMessage fromReceivedMessageText(String msgText) {
        String[] parts = msgText.split(FLAG_PATT);
        return new InitiateMessageChannelMessageBuilder()
                .addPublicKey(new Identifier(Utils.hexStringToByteArray(parts[1])))
                .build();
    }
}
