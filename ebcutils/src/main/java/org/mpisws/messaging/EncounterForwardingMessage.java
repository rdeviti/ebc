package org.mpisws.messaging;

import org.mpisws.helpers.Coder;
import org.mpisws.messaging.constraints.AllEncountersConstraint;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.ForwardingConstraint;

import java.io.IOException;
import java.io.Serializable;

import static org.mpisws.helpers.Utils.objFromString;
import static org.mpisws.helpers.Utils.objToString;
import static org.mpisws.messaging.constraints.ForwardingConstraint.EXPLOSION_LIMIT;

/**
 * Created by tslilyai on 2/20/18.
 */

/**
 * Describes the messages sent to and received from encounter channels
 */
public class EncounterForwardingMessage implements Serializable {
    private static final String DELIM = "$$$$$";
    protected static final String DELIM_PATT = java.util.regex.Pattern.quote(DELIM);
    private static final String DUMMY_TOPIC_HANDLE = "DUMMYTOPICHANDLE";

    String msgText;
    EncounterQueryConstraint encounterConstraint;
    ForwardingConstraint fwdingConstraint;
    String replyTopicHandle;

    public static class EncounterForwardingMessageBuilder {
        private EncounterForwardingMessage ebcMsgPackage = new EncounterForwardingMessage();
        public EncounterForwardingMessageBuilder addMessageText(String msg) {
            ebcMsgPackage.msgText = msg;
            return this;
        }
        public EncounterForwardingMessageBuilder addEncounterConstraint(EncounterQueryConstraint constraint) {
            ebcMsgPackage.encounterConstraint = constraint;
            return this;
        }
        public EncounterForwardingMessageBuilder addForwardingConstraint(ForwardingConstraint constraint) {
            ebcMsgPackage.fwdingConstraint = constraint;
            return this;
        }
        public EncounterForwardingMessageBuilder addReplyTopicHandle(String topicHandle) {
            ebcMsgPackage.replyTopicHandle = topicHandle;
            return this;
        }
        public EncounterForwardingMessage build() {
            if (ebcMsgPackage.msgText == null) {
                throw new IllegalArgumentException("Msg text cannot be null");
            }
            if (ebcMsgPackage.encounterConstraint == null) {
                ebcMsgPackage.encounterConstraint = new AllEncountersConstraint();
            }
            if (ebcMsgPackage.fwdingConstraint == null) {
                ebcMsgPackage.fwdingConstraint = new ForwardingConstraint(1, EXPLOSION_LIMIT, 60000*15);
            }
            if (ebcMsgPackage.replyTopicHandle == null) {
                ebcMsgPackage.replyTopicHandle = DUMMY_TOPIC_HANDLE;
            }
            return ebcMsgPackage;
        }
    }

    public String getMsgText() {
        return msgText;
    }

    public String getReplyTopicHandle() {
        return replyTopicHandle;
    }

    public EncounterQueryConstraint getEncounterConstraint() {
        return encounterConstraint;
    }

    public ForwardingConstraint getFwdingConstraint() {
        return fwdingConstraint;
    }

    public boolean shouldBeForwarded() {
        return fwdingConstraint.shouldForward();
    }

    public boolean expired() {
        return fwdingConstraint.expired();
    }

    public int getFwdingLimit() {
        return fwdingConstraint.getFanoutLimit();
    }

    public void updateForwardConstraint() {
        this.fwdingConstraint.update();
    }

    public String toSendMessageText(byte[] secret) {
        try {
            return Coder.encrypt(
                        msgText + DELIM
                                + objToString(getEncounterConstraint()) + DELIM
                                + objToString(getFwdingConstraint()) + DELIM
                                + ((replyTopicHandle == null || replyTopicHandle.compareTo("") == 0) ? DUMMY_TOPIC_HANDLE : replyTopicHandle),
                        secret);
        } catch (IOException e) {
            return null;
        }
    }

    public static EncounterForwardingMessage fromReceivedMessageText(String msgText) {
        if (msgText == null) return null;
        String[] parts = msgText.split(DELIM_PATT);
        try {
            EncounterQueryConstraint encounterConstraint = (EncounterQueryConstraint) objFromString(parts[1]);
            ForwardingConstraint fwdingConstraint = (ForwardingConstraint) objFromString(parts[2]);
            String replyTopicHandle = parts[3];
            if (replyTopicHandle.compareTo(DUMMY_TOPIC_HANDLE) == 0) {
                replyTopicHandle = "";
            }
            return new EncounterForwardingMessageBuilder()
                    .addMessageText(parts[0])
                    .addEncounterConstraint(encounterConstraint)
                    .addForwardingConstraint(fwdingConstraint)
                    .addReplyTopicHandle(replyTopicHandle)
                    .build();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
