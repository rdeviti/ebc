package org.mpisws.messaging;

import org.mpisws.helpers.Coder;

import java.io.Serializable;

public class EpochLinkMessage implements Serializable {
    private static final String DELIM = "!!!!!";
    protected static final String DELIM_PATT = java.util.regex.Pattern.quote(DELIM);

    String oldNonce;
    String newNonce;

    public String getOldNonce() {
        return oldNonce;
    }
    public String getNewNonce() {
        return newNonce;
    }

    public static class EpochLinkMessageBuilder {
        private EpochLinkMessage epochlinkingMessage = new EpochLinkMessage();
        public EpochLinkMessageBuilder addOldNonce(String oldNonce) {
            epochlinkingMessage.oldNonce = oldNonce;
            return this;
        }
        public EpochLinkMessageBuilder addNewNonce(String newNonce) {
            epochlinkingMessage.newNonce = newNonce;
            return this;
        }
        public EpochLinkMessage build() {
            if (epochlinkingMessage.oldNonce == null || epochlinkingMessage.newNonce == null) {
                throw new IllegalArgumentException("Nonces cannot be null");
            }
            return epochlinkingMessage;
        }
    }

    public String toSendMessageText(byte[] secret) {
        return Coder.encrypt(oldNonce + DELIM + newNonce, secret);
    }

    public static EpochLinkMessage fromReceivedMessageText(String msgText) {
        String[] parts = msgText.split(DELIM_PATT);
        return new EpochLinkMessageBuilder()
                .addOldNonce(parts[0])
                .addNewNonce(parts[1])
                .build();
    }
}
