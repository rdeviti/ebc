package org.mpisws.embeddedsocial.batchactions;

import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;
import com.microsoft.embeddedsocial.autorest.models.PostTopicRequest;
import com.microsoft.embeddedsocial.autorest.models.PostTopicResponse;
import com.microsoft.embeddedsocial.autorest.models.PublisherType;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.helpers.Identifier;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class CreateTopicAction extends BatchAction<PostTopicResponse> {
    private static final String TAG = CreateTopicAction.class.getSimpleName();
    private final List<String> topicHandles;
    private int index;
    private Identifier topicTitle;
    private Identifier topicText;
    private String authorization;

    public CreateTopicAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch,
                             int index, String authorization, Identifier topicTitle, Identifier topicText,
                             List<String> topicHandles) {
        super(batchOps, latch);
        this.authorization = authorization;
        this.topicTitle = topicTitle;
        this.topicText = topicText;
        this.index = index;
        this.topicHandles = topicHandles;
    }

    @Override
    public ServiceCall addToBatch() {
        PostTopicRequest req = new PostTopicRequest();
        req.setTitle(topicTitle.toString());
        req.setText(topicText.toString());
        req.setPublisherType(PublisherType.USER);
        return batchOps.getEsClient().getTopicsOperations().postTopicAsync(req, authorization, callback);
    }

    @Override
    public void processFailedResponse() { }

    @Override
    public void processSuccessfulResponse(ServiceResponse<PostTopicResponse> r) {
        if (r.getResponse().isSuccess()) this.topicHandles.set(index, r.getBody().getTopicHandle());
    }
}
