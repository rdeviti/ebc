package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.EmbeddedSocialBatchedClientImpl;
import com.microsoft.embeddedsocial.autorest.models.BuildsCurrentResponse;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by ssaroiu on 2/9/2018.
 */

public class GetBuildAction extends BatchAction<BuildsCurrentResponse> {
    private final int index;
    private final List<String> dateAndTimes;

    public GetBuildAction(org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, List<String> dateAndTimes) {
        super(batchOps, latch);
        this.index = index;
        this.dateAndTimes = dateAndTimes;
    }

    @Override
    public ServiceCall addToBatch() {
        return batchOps.getEsClient().getBuildsOperations().getBuildsCurrentAsync(callback);
    }

    @Override
    public void processFailedResponse() {
        System.out.println("Failed response");
        dateAndTimes.set(index, "Failed");
        return;
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<BuildsCurrentResponse> r) {
        if (r.getResponse().isSuccess()) {
            String dt;
            dt = r.getBody().getDateAndTime();
            dateAndTimes.set(index, dt);
        }
    }
}
