package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.PostFollowingTopicRequest;
import com.microsoft.embeddedsocial.autorest.models.TopicView;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class SubscribeTopicAction extends BatchAction<Object> {
    private final int index;
    private final List<String> topicHandles;
    private String authorization;

    public SubscribeTopicAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, String authorization, List<String> topicHandles) {
        super(batchOps, latch);
        this.authorization = authorization;
        this.topicHandles = topicHandles;
        this.index = index;
    }

    @Override
    public ServiceCall addToBatch() {
        if (authorization==null) return null;
        PostFollowingTopicRequest req = new PostFollowingTopicRequest();
        req.setTopicHandle(topicHandles.get(index));
        return batchOps.getEsClient().getMyFollowingOperations()
                .postFollowingTopicAsync(req, authorization, callback);
    }

    @Override
    public void processFailedResponse() {
        this.topicHandles.set(index, null);
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<Object> r) { }
}
