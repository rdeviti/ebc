package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;

import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class DeleteTopicAction extends BatchAction<Object> {
    private static final String TAG = DeleteTopicAction.class.getSimpleName();
    private final String topicHandle;
    private String authorization;

    public DeleteTopicAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, String authorization, String topicHandle) {
        super(batchOps, latch);
        this.authorization = authorization;
        this.topicHandle = topicHandle;
    }

    @Override
    public ServiceCall addToBatch() {
        return batchOps.getEsClient().getTopicsOperations()
                .deleteTopicAsync(topicHandle, authorization, callback);
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<Object> r) {}

    @Override
    public void processFailedResponse() {}
}
