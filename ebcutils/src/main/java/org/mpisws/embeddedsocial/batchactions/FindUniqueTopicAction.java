package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.FeedResponseTopicView;
import com.microsoft.embeddedsocial.autorest.models.TopicView;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;
import org.mpisws.helpers.Identifier;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class FindUniqueTopicAction extends BatchAction<FeedResponseTopicView> {
    private static final String TAG = FindUniqueTopicAction.class.getSimpleName();
    private final ConcurrentLinkedQueue<TopicView> toDelete;
    private final List<Pair<Identifier, Identifier>> toCreate;
    private final List<String> topicHandles;
    private Identifier topicTitle;
    private Identifier topicText;
    private String authorization;
    private int index;

    public FindUniqueTopicAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch,
                                 int index, String authorization, Identifier topicTitle, Identifier topicText,
                                 ConcurrentLinkedQueue<TopicView> toDelete, List<Pair<Identifier, Identifier>> toCreate,
                                 List<String> topicHandles) {
        super(batchOps, latch);
        this.index = index;
        this.topicTitle = topicTitle;
        this.topicText = topicText;
        this.authorization = authorization;
        this.toDelete = toDelete;
        this.toCreate = toCreate;
        this.topicHandles = topicHandles;
    }

    @Override
    public ServiceCall addToBatch() {
        if (authorization==null) return null;
        return batchOps.getEsClient().getSearchOperations().getTopicsAsync(topicTitle.toString(), authorization, callback);
    }

    @Override
    public void processFailedResponse() { }

    @Override
    public void processSuccessfulResponse(ServiceResponse<FeedResponseTopicView> r) {
        if (!r.getResponse().isSuccess()) {
            return;
        }
        List<TopicView> topics = r.getBody().getData();
        if (topics.size() == 0) {
            if (toCreate != null) {
                toCreate.set(index, new ImmutablePair<>(topicTitle, topicText));
            }
        } else {
            topicHandles.set(index, topics.get(0).getTopicHandle());
            if (topics.size() > 1) {
                for (int i = 1; i < topics.size(); i++) {
                    toDelete.add(topics.get(i));
                }
            }
        }
    }
}
