package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.CommentView;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.ESMessage;
import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;
import org.mpisws.embeddedsocial.ESUserAccount;
import org.mpisws.helpers.GlobalObjectRegistry;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class GetMsgTextAction extends BatchAction<CommentView> {
    private final int index;
    private final List<ESMessage> msgs;
    private String authorization;
    private String commentHandle;

    public GetMsgTextAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, String commentHandle, String authorization, List<ESMessage> msgs) {
        super(batchOps, latch);
        this.commentHandle = commentHandle;
        this.authorization = authorization;
        this.index = index;
        this.msgs = msgs;
    }

    @Override
    public ServiceCall addToBatch() {
        if (authorization==null) return null;
        return batchOps.getEsClient().getCommentsOperations()
                .getCommentAsync(this.commentHandle, this.authorization, callback);
    }

    @Override
    public void processFailedResponse() {
        return;
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<CommentView> r) {
        if (!r.getResponse().isSuccess()) return;
        CommentView cv = r.getBody();
        ESMessage msg = msgs.get(index);
        msg.setMsgText(cv.getText());
        msg.setIsMe(GlobalObjectRegistry.getObject(ESUserAccount.class).isMe(cv.getUser().getUserHandle()));
        msg.setCursor(commentHandle);
        msgs.set(index, msg);
        System.out.println("Got comment " + msg.getMsgText() + " with handle " + msg.getCursor());
    }
}
