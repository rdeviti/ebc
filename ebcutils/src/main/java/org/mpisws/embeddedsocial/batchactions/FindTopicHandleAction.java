package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.FeedResponseTopicView;
import com.microsoft.embeddedsocial.autorest.models.TopicView;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.ESUserAccount;
import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;
import org.mpisws.helpers.GlobalObjectRegistry;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class FindTopicHandleAction extends BatchAction<FeedResponseTopicView> {
    private static final String TAG = FindTopicHandleAction.class.getSimpleName();
    private final List<String> topicHandles;
    private final String topicTitle;
    private String authorization;
    private int index;

    public FindTopicHandleAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, String authorization, String topicTitle, List<String> topicTexts) {
        super(batchOps, latch);
        this.index = index;
        this.topicTitle = topicTitle;
        this.authorization = authorization;
        this.topicHandles = topicTexts;
    }

    @Override
    public ServiceCall addToBatch() {
        return batchOps.getEsClient().getSearchOperations().getTopicsAsync(topicTitle, authorization, callback);
    }

    @Override
    public void processFailedResponse() {
        return;
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<FeedResponseTopicView> r) {
        if (!r.getResponse().isSuccess()) {
            return;
        }
        List<TopicView> topics = r.getBody().getData();
        for (TopicView tv : topics) {
            System.out.println("FIND_TOPIC_HANDLE: Got " + topics.size() + " topics");
            if (!GlobalObjectRegistry.getObject(ESUserAccount.class).isMe(tv.getUser().getUserHandle())) {
                topicHandles.set(index, tv.getTopicHandle());
                System.out.println("FIND_TOPIC_HANDLE: Got topic handle " + tv.getTopicHandle());
            }
        }
    }
}
