package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceCallback;
import com.microsoft.rest.ServiceResponse;
import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;

import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public abstract class BatchAction<T> {
    EmbeddedSocialBatchedClientImpl batchOps;
    final CountDownLatch latch;
    final BatchServiceCallback<T> callback = new BatchServiceCallback<>(this);

    public BatchAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch) {
        this.batchOps = batchOps;
        this.latch = latch;
        System.out.println("BatchAction: New " + this.getClass().getSimpleName());
    }

    abstract ServiceCall addToBatch();

    abstract void processFailedResponse();

    abstract void processSuccessfulResponse(ServiceResponse<T> r);

    class BatchServiceCallback<T> extends ServiceCallback<T> {
        private BatchAction action;

        public BatchServiceCallback(BatchAction action) {
            this.action = action;
        }

        @Override
        public void failure(Throwable t) {
            System.out.println("ActionCallback: Processing failed result: " + t.toString());
            action.processFailedResponse();
            latch.countDown();
        }
        @Override
        public void success(ServiceResponse<T> result) {
            System.out.println("ActionCallback: Processing successful result in thread " + Thread.currentThread().getName());
            action.processSuccessfulResponse(result);
            latch.countDown();
        }
    }
}
