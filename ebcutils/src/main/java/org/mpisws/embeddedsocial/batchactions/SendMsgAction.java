package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.PostCommentRequest;
import com.microsoft.embeddedsocial.autorest.models.PostCommentResponse;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class SendMsgAction extends BatchAction<PostCommentResponse> {
    private static final String TAG = SendMsgAction.class.getSimpleName();
    private final String msg;
    private final String topicHandle;
    private final List<Boolean> successes;
    private final int index;
    private String authorization;

    public SendMsgAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, String authorization, String topicHandle, String msg, List<Boolean> successes) {
        super(batchOps, latch);
        this.index = index;
        this.authorization = authorization;
        this.topicHandle = topicHandle;
        this.msg = msg;
        this.successes = successes;
    }

    @Override
    public ServiceCall addToBatch() {
        if (authorization==null) return null;
        PostCommentRequest req = new PostCommentRequest();
        req.setText(msg);
        return batchOps.getEsClient().getTopicCommentsOperations()
                .postCommentAsync(topicHandle, req, authorization, callback);
    }

    @Override
    public void processFailedResponse() {
        successes.set(index, false);
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<PostCommentResponse> r) {
        System.out.println("Posted comment " + msg + " with handle " + r.getBody().getCommentHandle());
        successes.set(index, r.getResponse().isSuccess());
    }
}
