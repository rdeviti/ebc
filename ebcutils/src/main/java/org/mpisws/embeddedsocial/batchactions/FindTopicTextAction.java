package org.mpisws.embeddedsocial.batchactions;

import com.microsoft.embeddedsocial.autorest.models.FeedResponseTopicView;
import com.microsoft.embeddedsocial.autorest.models.TopicView;
import com.microsoft.rest.ServiceCall;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tslilyai on 2/9/18.
 */

public class FindTopicTextAction extends BatchAction<FeedResponseTopicView> {
    private static final String TAG = FindTopicTextAction.class.getSimpleName();
    private final List<String> topicTexts;
    private final String topicTitle;
    private String authorization;
    private int index;

    public FindTopicTextAction(EmbeddedSocialBatchedClientImpl batchOps, CountDownLatch latch, int index, String authorization, String topicTitle, List<String> topicTexts) {
        super(batchOps, latch);
        this.index = index;
        this.topicTitle = topicTitle;
        this.authorization = authorization;
        this.topicTexts = topicTexts;
    }

    @Override
    public ServiceCall addToBatch() {
        return batchOps.getEsClient().getSearchOperations()
                .getTopicsAsync(topicTitle, authorization, callback);
    }

    @Override
    public void processFailedResponse() {
        return;
    }

    @Override
    public void processSuccessfulResponse(ServiceResponse<FeedResponseTopicView> r) {
        if (!r.getResponse().isSuccess()) {
            return;
        }
        List<TopicView> topics = r.getBody().getData();
        if (topics.size() > 0) {
            System.out.println(topics.get(0).getText());
            topicTexts.set(index, topics.get(0).getText());
        }
    }
}
