package org.mpisws.embeddedsocial;

import com.microsoft.embeddedsocial.autorest.models.PostSessionRequest;
import com.microsoft.embeddedsocial.autorest.models.PostSessionResponse;
import com.microsoft.embeddedsocial.autorest.models.PostUserRequest;
import com.microsoft.embeddedsocial.autorest.models.PostUserResponse;
import com.microsoft.embeddedsocial.autorest.models.UserProfileView;
import com.microsoft.rest.ServiceException;
import com.microsoft.rest.ServiceResponse;

import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;

import java.io.IOException;

import static org.mpisws.embeddedsocial.ESClient.OAUTH_TEMPLATE;
import static org.mpisws.embeddedsocial.ESClient.SESSION_TEMPLATE;

/**
 * Created by tslilyai on 2/8/18.
 */

public class ESUserAccount {
    private String userHandle;
    private String googletoken;
    private String auth;

    public ESUserAccount() {}

    protected void setCredentials(String userHandle, String auth) {
        this.userHandle = userHandle;
        this.auth = auth;
    }
    private void setNewAccountData(String newUserHandle, String sessionToken) {
        userHandle = newUserHandle;
        auth = String.format(SESSION_TEMPLATE, sessionToken);
        GlobalObjectRegistry.getObject(ESCredentials.class).setUserHandle(newUserHandle);
        GlobalObjectRegistry.getObject(ESCredentials.class).setAuth(auth);
    }

    public void addGoogleToken(String googletoken) {
        System.out.println("USER " +"Set google token " + googletoken);
        this.googletoken = googletoken;
    }

    public boolean isSignedIn() {
        return (auth != null && auth.length() != 0);
    }

    public void getNewSessionWithGoogleToken() {
        userHandle = GlobalObjectRegistry.getObject(ESCredentials.class).getUserHandle();
        auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();

        String authorization = String.format(OAUTH_TEMPLATE, GlobalObjectRegistry.getObject(ESCredentials.class).getEsAPIKey(), googletoken);
        System.out.println("USER " +"Got auth to sign in " + authorization);
        ServiceResponse<UserProfileView> sResp = null;
        try {
             sResp = ESClient.getInstance().esclientOps.getUsersOperations().getMyProfile(authorization);
        } catch (ServiceException | IOException e) {
            System.out.println("USER " +"Exception Register: Failed to sign in " + e.getMessage());
            e.printStackTrace();
            return;
        }

        if (!sResp.getResponse().isSuccess()) {
            if (sResp.getResponse().code() == 404) {
                System.out.println("USER " +"Creating account");
                createAccount(authorization);
                return;
            } else {
                System.out.println("USER " +"Failed to sign in " + sResp.getResponse().code());
                return;
            }
        }
        userHandle = sResp.getBody().getUserHandle();
        final PostSessionRequest req = new PostSessionRequest();
        req.setUserHandle(userHandle);
        req.setInstanceId("ebc_id");

        try {
            ServiceResponse<PostSessionResponse> sResp2 = ESClient.getInstance().esclientOps.getSessionsOperations().postSession(req, authorization);
            if (!sResp2.getResponse().isSuccess()) {
                System.out.println("USER " +"PostSession: Failed to sign in");
                return;
            }
            setNewAccountData(sResp2.getBody().getUserHandle(), sResp2.getBody().getSessionToken());
            System.out.println("Logged in!");
        } catch (ServiceException |IOException e) {
            System.out.println("USER " +"Exception Session: Failed to sign in " + e.getMessage());
        }
    }
    private void createAccount(String auth) {
        final PostUserRequest req = new PostUserRequest();
        req.setFirstName("myfirstname");
        req.setLastName("mylastname");
        req.setInstanceId("ebc_id");

        ServiceResponse<PostUserResponse> serviceResponse;
        try {
            serviceResponse = ESClient.getInstance().esclientOps.getUsersOperations().postUser(req, auth);
        } catch (ServiceException | IOException e) {
            return;
        }
        if (!serviceResponse.getResponse().isSuccess()) {
            System.out.println("USER " +"Failed to register user");
            return;
        } else {
            System.out.println("USER " +"Registered user!");
            setNewAccountData(serviceResponse.getBody().getUserHandle(), serviceResponse.getBody().getSessionToken());
        }
    }

    public void deleteAccount() {
        try {
            ESClient.getInstance().esclientOps.getUsersOperations().deleteUser(auth);
            signOut();
        } catch (Exception e) {
            System.out.println("USER " +"Failed to delete");
        }
    }

    public void signOut() {
        try {
            ESClient.getInstance().esclientOps.getSessionsOperations().deleteSession(auth);
        } catch (Exception e) {
            System.out.println("USER " +"Failed to sign out");
        }
        GlobalObjectRegistry.getObject(ESCredentials.class).setAuth(null);
        GlobalObjectRegistry.getObject(ESCredentials.class).setUserHandle(null);
        userHandle = null;
        auth = null;
    }

    public boolean isMe(String otheruserHandle) {
        return userHandle != null && userHandle.equals(otheruserHandle);
    }
}
