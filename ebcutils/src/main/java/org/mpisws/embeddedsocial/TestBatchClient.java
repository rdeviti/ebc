package org.mpisws.embeddedsocial;

import com.microsoft.embeddedsocial.autorest.EmbeddedSocialClientImpl;
import com.microsoft.embeddedsocial.autorest.models.PostTopicRequest;
import com.microsoft.embeddedsocial.autorest.models.TopicView;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.batchactions.CreateTopicAction;
import org.mpisws.embeddedsocial.batchactions.DeleteTopicAction;
import org.mpisws.embeddedsocial.batchactions.FindUniqueTopicAction;
import org.mpisws.embeddedsocial.batchactions.GetBuildAction;
import org.mpisws.embeddedsocial.batchactions.SubscribeTopicAction;
import org.mpisws.helpers.Coder;
import org.mpisws.helpers.Identifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

import okhttp3.Response;

import static java.lang.System.exit;
import static org.mpisws.embeddedsocial.ESClient.baseURL;
import static org.mpisws.helpers.Utils.hexStringToByteArray;

/**
 * Created by tslilyai on 3/24/18.
 */

public class TestBatchClient {
    static final String auth = "SocialPlus TK=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzalNjZHFzN0paYVxuR29vZ2xlXG4xMTAyMDM3NjQyMDI5ODI4NDk3NTYiLCJpc3MiOiIzcWdtZzlWUmNaeFxuMmU1YTFjYzgtNWVhYi00ZGJkLThkNmQtNmE4NGVhYjIzMzc0IiwiZXhwIjoxNTQ3Mzg1NTA0fQ.DhURO4E5-BEFz_dWMdHZHQLq-E_fZf62_si-UlqF3Ko";
    static final String topicHandle = "3g_nGuswN8z";
    static final String ss = "0E31177095EE530101D89573C8261C14F51A3877D4676F74478F5CAF";

    public static void main(String[] args) {
        ESClient.getInstance().setESCredentials("user", auth);
        ESClient.getInstance().setESAPIKey("2e5a1cc8-5eab-4dbd-8d6d-6a84eab23374");
        /*new Thread( () -> {
            String topicHandle = ESClient.getInstance().createOneTopic(
                    new ImmutablePair<>(
                            new Identifier("hello".getBytes()),
                            new Identifier("hello".getBytes())));
            System.out.println(topicHandle);
        }).start();*/

        new Thread( () -> {
            System.out.println("Starting!");
            List<ESMessage> messages = ESClient.getInstance().getMessagesOnTopic(topicHandle);
            for (ESMessage msg : messages) {
                System.out.println(Coder.decrypt(msg.getMsgText(), hexStringToByteArray(ss)));
            }
        }).start();
    }

    public static void getBuilds(int k) {
        org.mpisws.embeddedsocial.EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                .batchSize(k)
                .baseUrl(baseURL)
                .build();
        CountDownLatch latch = new CountDownLatch(k);
        List<String> datesTimes = new ArrayList<>();
        List<Boolean> successes = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            datesTimes.add(null);
            successes.add(null);
            new GetBuildAction(batchOps, latch, i, datesTimes).addToBatch();
        }
        try {
            System.out.println("Issuing Batch");
            batchOps.issueBatch();
            latch.await();
        } catch (InterruptedException | IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Done issuing batch of " + k);
        for (int i = 0; i < k; i++) {
            System.out.println(i + ": " + datesTimes.get(i));
        }
    }
}
