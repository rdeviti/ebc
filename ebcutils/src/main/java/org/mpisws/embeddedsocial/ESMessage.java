package org.mpisws.embeddedsocial;

import org.mpisws.helpers.Coder;

import java.io.Serializable;

public class ESMessage implements Serializable {
    private final long timestamp;
    private boolean isUnread;
    private String topicText;
    private String msgText;
    private String topicHandle;
    private boolean msgSenderIsMe;
    private String cursor;

    /**
     * Construct a message contents object after retrieving a message
     * from a particular newNonce, with a particular msgText, and with the retrieved cursor
     * and sender information. Used during message retrieval and not for message sending
     *
     * @param msgText
     * @param topicText
     * @param topicHandle
     * @param msgSenderIsMe
     * @param cursor
     */
    public ESMessage(String msgText, String topicText, String topicHandle, boolean msgSenderIsMe, String cursor, boolean isUnread,
                     long timestamp) {
        this.msgText = msgText;
        this.topicText = topicText;
        this.topicHandle = topicHandle;
        this.msgSenderIsMe = msgSenderIsMe;
        this.cursor = cursor;
        this.isUnread = isUnread;
        this.timestamp = timestamp;
    }

    public boolean isUnread() {
        return isUnread;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMsgText() {
        return msgText;
    }

    public String getTopicHandle() {
        return topicHandle;
    }

    public String getTopicText() {
        return topicText;
    }

    public boolean getIsMe() {
        return msgSenderIsMe;
    }

    public String getCursor() {
        return cursor;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public void setTopicHandle(String topicHandle) {
        this.topicHandle = topicHandle;
    }

    public void setTopicText(String topicHandle) {
        this.topicText = topicHandle;
    }

    public void setIsMe(boolean isMe) {
        this.msgSenderIsMe = isMe;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
