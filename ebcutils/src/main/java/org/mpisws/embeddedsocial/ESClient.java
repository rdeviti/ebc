package org.mpisws.embeddedsocial;

import com.microsoft.embeddedsocial.autorest.EmbeddedSocialClient;
import com.microsoft.embeddedsocial.autorest.EmbeddedSocialClientImpl;
import com.microsoft.embeddedsocial.autorest.models.ActivityType;
import com.microsoft.embeddedsocial.autorest.models.ActivityView;
import com.microsoft.embeddedsocial.autorest.models.CommentView;
import com.microsoft.embeddedsocial.autorest.models.FeedResponseActivityView;
import com.microsoft.embeddedsocial.autorest.models.FeedResponseCommentView;
import com.microsoft.embeddedsocial.autorest.models.PostCommentRequest;
import com.microsoft.embeddedsocial.autorest.models.PostReportRequest;
import com.microsoft.embeddedsocial.autorest.models.PutNotificationsStatusRequest;
import com.microsoft.embeddedsocial.autorest.models.Reason;
import com.microsoft.embeddedsocial.autorest.models.TopicView;
import com.microsoft.rest.ServiceException;
import com.microsoft.rest.ServiceResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.mpisws.embeddedsocial.batchactions.CreateTopicAction;
import org.mpisws.embeddedsocial.batchactions.DeleteTopicAction;
import org.mpisws.embeddedsocial.batchactions.FindTopicHandleAction;
import org.mpisws.embeddedsocial.batchactions.FindTopicTextAction;
import org.mpisws.embeddedsocial.batchactions.GetMsgTextAction;
import org.mpisws.embeddedsocial.batchactions.SendMsgAction;
import org.mpisws.helpers.ESCredentials;
import org.mpisws.helpers.GlobalObjectRegistry;
import org.mpisws.helpers.Identifier;
import org.mpisws.helpers.Utils;
import org.mpisws.messaging.ReceivedMessageWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import okhttp3.Response;

import static java.util.concurrent.Executors.newCachedThreadPool;

/**
 * Created by tslilyai on 2/6/18.
 */

public class ESClient {
    private static ESClient instance = new ESClient();
    private static final String TAG = "ESClient";
	public static final String SESSION_TEMPLATE = "SocialPlus TK=%s";
	public static final String OAUTH_TEMPLATE = "Google AK=%s|TK=%s";
    public static final String baseURL = "https://ppe.embeddedsocial.microsoft.com/";
	protected final EmbeddedSocialClient esclientOps;

    private final ExecutorService executorService = newCachedThreadPool();

    private ESUserAccount esuser_client;

    private ESClient() {
        esclientOps = new EmbeddedSocialClientImpl(baseURL);
        GlobalObjectRegistry.addObject(new ESCredentials());
        GlobalObjectRegistry.addObject(new ESUserAccount());
        esuser_client = GlobalObjectRegistry.getObject(ESUserAccount.class);
   }

    public static ESClient getInstance() {
        return instance;
    }

    public void setESCredentials(String userHandle, String auth) {
        GlobalObjectRegistry.getObject(ESCredentials.class).setAuth(auth);
        GlobalObjectRegistry.getObject(ESCredentials.class).setUserHandle(userHandle);
        esuser_client.setCredentials(userHandle, auth);
    }

    public void setESAPIKey(String apiKey) {
        GlobalObjectRegistry.getObject(ESCredentials.class).setEsAPIKey(apiKey);
    }

    /******************************************************************
    * USER ACCOUNT / REGISTRATION FUNCTIONS *
    ******************************************************************/
    public void registerGoogleUser(String googletoken) {
        esuser_client.addGoogleToken(googletoken);
    }


    private class SignInRunnable implements Runnable {
        boolean done = false;
        @Override
        public void run() {
            esuser_client.getNewSessionWithGoogleToken();
            synchronized (this) {
                done = true;
                notifyAll();
            }
        }
    }
    public void signIn() {
        if (esuser_client.isSignedIn()) return;
        SignInRunnable signInRunnable = new SignInRunnable();
        executorService.execute(signInRunnable);
        synchronized (signInRunnable) {
            while (!signInRunnable.done) {
                try {
                    signInRunnable.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deleteAccount() {
        executorService.execute(() -> esuser_client.deleteAccount());
    }

    public void signOut() {
        executorService.execute(() -> esuser_client.signOut());
    }

    public boolean isSignedIn() {
        return esuser_client.isSignedIn();
    }

    public ESCredentials getCredentials() {
        return GlobalObjectRegistry.getObject(ESCredentials.class);
    }

    public void blockUsers(List<String> topicHandles) {
        if (topicHandles.size() == 0) return;
        topicHandles.removeIf(s -> s == null);
        DeleteTopicsRunnable deleteTopicsRunnable = new DeleteTopicsRunnable(topicHandles);
        executorService.execute(deleteTopicsRunnable);
    }

    private class DeleteTopicsRunnable implements Runnable {
        private final List<String> topicHandles;

        public DeleteTopicsRunnable(List<String> topicHandles) {
            this.topicHandles = topicHandles;
        }

        @Override
        public void run() {
            List<List<String>> topicHandleBunches = Utils.getBatches(topicHandles);
            for (List<String> ths: topicHandleBunches) {
                EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                        .batchSize(ths.size())
                        .baseUrl(baseURL)
                        .build();
                CountDownLatch latch = new CountDownLatch(ths.size());
                String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();

                for (int i = 0; i < ths.size(); i++) {
                    new DeleteTopicAction(batchOps, latch, auth, ths.get(i)).addToBatch();
                }
                try {
                    Response resp = batchOps.issueBatch();
                    latch.await();
                } catch (IOException | InterruptedException e) {
                    System.out.println(TAG + " Failed to compute batch");
                    e.printStackTrace();
                }
            }
        }
   }

    /******************************************************************
     *  TOPIC SEARCH AND TOPIC CREATION FUNCTIONS *
    ******************************************************************/
    private class CreateTopicsRunnable implements Runnable {
        private final List<List<Pair<Identifier, Identifier>>> titleTextPairBatches;
        protected List<String> topicHandles = new ArrayList<>();
        public boolean doneExecuting = false;

        public CreateTopicsRunnable (List<Pair<Identifier, Identifier>> titleTextPairs) {
            this.titleTextPairBatches = Utils.getBatches(titleTextPairs);
        }

        @Override
        public void run() {
            String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();

            for (int batch = 0; batch < titleTextPairBatches.size(); batch++) {
                List<Pair<Identifier, Identifier>> titleTextPairs = titleTextPairBatches.get(batch);
                List<String> foundTopics = new ArrayList<>(titleTextPairs.size());
                for (int i = 0; i < titleTextPairs.size(); i++) {
                    foundTopics.add(null);
                }
                int batchSize = 0;
                for (int i = 0; i < titleTextPairs.size(); i++) {
                    if (!(titleTextPairs.get(i) == null || titleTextPairs.get(i).getLeft() == null || titleTextPairs.get(i).getRight() == null)) {
                        batchSize++;
                    }
                }
                System.out.println(TAG + "\t Creating " + batchSize + " topics");
                CountDownLatch latch;

                if (batchSize > 0) {
                    EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                            .batchSize(batchSize)
                            .baseUrl(baseURL)
                            .build();
                    latch = new CountDownLatch(batchSize);
                    for (int i = 0; i < titleTextPairs.size(); i++) {
                        if (!(titleTextPairs.get(i) == null || titleTextPairs.get(i).getLeft() == null || titleTextPairs.get(i).getRight() == null)) {
                            new CreateTopicAction(batchOps, latch, i, auth, titleTextPairs.get(i).getLeft(), titleTextPairs.get(i).getRight(), foundTopics)
                                    .addToBatch();
                            System.out.println(TAG + "\t Creating topic " + titleTextPairs.get(i).getLeft());
                        }
                    }
                    try {
                        Response resp = batchOps.issueBatch();
                        latch.await();
                    } catch (InterruptedException | IOException e) {
                        e.printStackTrace();
                    }
                }
                // TODO need to check if appended?
                this.topicHandles.addAll(foundTopics);
            }
            synchronized(this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    private class GetTopicTextsRunnable implements Runnable {
        public boolean doneExecuting = false;
        private List<List<String>> advertStrsBatches;
        private List<String> topicTexts = new ArrayList<>();

        public GetTopicTextsRunnable(List<String> advertStrs) {
            this.advertStrsBatches = Utils.getBatches(advertStrs);
        }

        @Override
        public void run() {
            for (List<String> advertStrs : this.advertStrsBatches) {
                EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                        .baseUrl(baseURL)
                        .batchSize(advertStrs.size())
                        .build();
                CountDownLatch latch = new CountDownLatch(advertStrs.size());
                List<String> topicTexts = new ArrayList<>(advertStrs.size());
                for (int i = 0; i < advertStrs.size(); i++) {
                    topicTexts.add(null);
                }
                String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
                for (int i = 0; i < advertStrs.size(); i++) {
                    new FindTopicTextAction(batchOps, latch, i, auth, advertStrs.get(i), topicTexts).addToBatch();
                }
                try {
                    Response resp = batchOps.issueBatch();
                    latch.await();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                this.topicTexts.addAll(topicTexts);
            }
            synchronized (this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    private class GetTopicTitleRunnable implements Runnable {
        public boolean doneExecuting = false;
        String topicTitle;
        final String topicHandle;

        public GetTopicTitleRunnable(String topicHandle) {
            this.topicHandle = topicHandle;
        }

        @Override
        public void run() {
            String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
            try {
                ServiceResponse<TopicView> r = esclientOps.getTopicsOperations().getTopic(topicHandle, auth);
                if (r.getResponse().isSuccess()) {
                    topicTitle = r.getBody().getTitle();
                }
            } catch (IOException | ServiceException e) {
                    e.printStackTrace();
            }
            synchronized (this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    private class GetTopicHandlesRunnable implements Runnable {
        public boolean doneExecuting = false;
        private List<List<Identifier>> eidsBatches;
        private List<String> topicHandles = new ArrayList<>();

        public GetTopicHandlesRunnable(List<Identifier> eids) {
            this.eidsBatches = Utils.getBatches(eids);
        }

        @Override
        public void run() {
            for (List<Identifier> eids : eidsBatches) {
                EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                        .baseUrl(baseURL)
                        .batchSize(eids.size())
                        .build();
                CountDownLatch latch = new CountDownLatch(eids.size());
                List<String> topicHandles = new ArrayList<>(eids.size());
                for (int i = 0; i < eids.size(); i++) {
                    topicHandles.add(null);
                }
                String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
                for (int i = 0; i < eids.size(); i++) {
                    new FindTopicHandleAction(batchOps, latch, i, auth, eids.get(i).toString(), topicHandles).addToBatch();
                }
                try {
                    Response resp = batchOps.issueBatch();
                    latch.await();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                this.topicHandles.addAll(topicHandles);
            }
            synchronized (this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    public String createOneTopic(Pair<Identifier, Identifier> titleTextPair) {
        List pairs = new ArrayList();
        pairs.add(titleTextPair);
        List<String> results = createTopics(pairs);
        if (results.size() != 1) {
            return null;
        } else return results.get(0);
    }
    public List<String> createTopics(List<Pair<Identifier, Identifier>> titleTextPairs) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        if (titleTextPairs.size() == 0) return new ArrayList<>();
        CreateTopicsRunnable createTopicsRunnable = new CreateTopicsRunnable(titleTextPairs);
        executorService.execute(createTopicsRunnable);
        synchronized(createTopicsRunnable) {
            try {
                if (!createTopicsRunnable.doneExecuting)
                    createTopicsRunnable.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        return createTopicsRunnable.topicHandles;
    }

    public String getOneTopicTitle(String replyTopicHandle) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        GetTopicTitleRunnable getTopicTitleRunnable = new GetTopicTitleRunnable(replyTopicHandle);
        executorService.execute(getTopicTitleRunnable);
        synchronized(getTopicTitleRunnable) {
            try {
                if (!getTopicTitleRunnable.doneExecuting) {
                    getTopicTitleRunnable.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        return getTopicTitleRunnable.topicTitle;
    }
    public List<String> getTopicTexts(List<String> advertStrs) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        if (advertStrs.size() == 0) return new ArrayList<>();
        GetTopicTextsRunnable getTopicTextsRunnable = new GetTopicTextsRunnable(advertStrs);
        executorService.execute(getTopicTextsRunnable);
        synchronized(getTopicTextsRunnable) {
            try {
                if (!getTopicTextsRunnable.doneExecuting) {
                    getTopicTextsRunnable.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        return getTopicTextsRunnable.topicTexts;
    }

    public String getOneTopicHandle(Identifier id) {
        List toGet = new ArrayList();
        toGet.add(id);
        List<String> handles = getTopicHandles(toGet);
        if (handles.size() != 1) return null;
        return handles.get(0);
    }
    public List<String> getTopicHandles(List<Identifier> eids) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        if (eids.size() == 0) return new ArrayList<>();
        GetTopicHandlesRunnable getTopicHandlesRunnable = new GetTopicHandlesRunnable(eids);
        executorService.execute(getTopicHandlesRunnable);
        synchronized(getTopicHandlesRunnable) {
            try {
                if (!getTopicHandlesRunnable.doneExecuting) {
                    getTopicHandlesRunnable.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        return getTopicHandlesRunnable.topicHandles;
    }

    /******************************************************************
     *  MESSAGING FUNCTIONS *
    ******************************************************************/
    public static abstract class GetReceivedMessagesCallback {
        abstract public void processReceivedMessages(List<ReceivedMessageWrapper> msgs);
    }

    private class GetMessagesRunnable implements Runnable {
        private final boolean onlyUnread;
        public boolean doneExecuting = false;
        public List<ESMessage> msgs;
        private final int pageSize;
        private String startCursor;
        private String continuationCursor = null;

        public GetMessagesRunnable(String startCursor, int pageSize, boolean onlyUnread) {
            this.startCursor = startCursor;
            this.pageSize = pageSize;
            this.onlyUnread = onlyUnread;
        }

        @Override
        public void run() {
            String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
            this.msgs = new ArrayList<>();
            ServiceResponse<FeedResponseActivityView> r;
            try {
                r = esclientOps.getMyNotificationsOperations().getNotifications(auth, startCursor, pageSize);

                // process the response
                if (!r.getResponse().isSuccess()) {
                    System.out.println("Notifs: " + "Unsuccessful request " + r.toString() + r.getResponse().code());
                } else {
                    // set the cursor to continue
                    this.continuationCursor = r.getBody().getCursor();
                    System.out.println("Notifs: Set continutation cursor to " + r.getBody().getCursor());

                    int batchSize = 0;
                    List<ActivityView> activities = r.getBody().getData();
                    List<Integer> commentActivities = new ArrayList<>();
                    for (int i = 0; i < activities.size(); i++) {
                        // if we've read this message before and we only want unread messages, then we're done!
                        if (onlyUnread && !activities.get(i).getUnread()){
                            break;
                        }
                        if ((activities.get(i).getActivityType() == ActivityType.COMMENT)) {
                            batchSize++;
                            commentActivities.add(i);
                            msgs.add(new ESMessage(
                                    null, null, null, false,
                                    null, activities.get(i).getUnread(), -1));
                        }
                    }
                    if (batchSize > 0) {
                        // we don't need to split these messages into batches of size <32 because we know that
                        // the pageSize (max number of messages) is 20
                        EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                                .batchSize(batchSize)
                                .baseUrl(ESClient.baseURL)
                                .build();
                        CountDownLatch latch = new CountDownLatch(batchSize);
                        for (int i : commentActivities) {
                            msgs.get(i).setTopicHandle(activities.get(i).getActedOnContent().getContentHandle());
                            msgs.get(i).setTopicText(activities.get(i).getActedOnContent().getText());
                            new GetMsgTextAction(batchOps, latch, i, activities.get(i).getActivityHandle(), auth, msgs)
                                    .addToBatch();
                        }
                        Response resp = batchOps.issueBatch();
                        latch.await();
                    }
                }
            } catch (IOException | InterruptedException | ServiceException e) {
                System.out.println("Exception in getMsgs(): " + e.getMessage());
            }
            synchronized (this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    private class GetMessagesOnTopicRunnable implements Runnable {
        private final String cursor;
        public boolean doneExecuting = false;
        public List<ESMessage> msgs;
        private final String topicHandle;

        public GetMessagesOnTopicRunnable(String topicHandle, String cursor) {
            this.topicHandle = topicHandle;
            this.cursor = cursor;
        }

        @Override
        public void run() {
            String continuationCursor = cursor;
            String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
            this.msgs = new ArrayList<>();
            ServiceResponse<FeedResponseCommentView> r;
            int maxLoops = 50, loopIndex = 0;
            try {
                while(loopIndex < maxLoops) {
                    r = esclientOps.getTopicCommentsOperations().getTopicComments(topicHandle, auth, continuationCursor, 20);

                    // process the response
                    if (!r.getResponse().isSuccess()) {
                        System.out.println("Comments: " + "Unsuccessful request " + r.toString() + r.getResponse().code());
                    } else {
                        // set the cursor to continue
                        continuationCursor = r.getBody().getCursor();
                        for (CommentView comment : r.getBody().getData()) {
                            System.out.println("Comments: Got message! " + comment.getText());
                            msgs.add(new ESMessage(comment.getText(), "", comment.getTopicHandle(),
                                    esuser_client.isMe(comment.getUser().getUserHandle()),
                                    comment.getCommentHandle(), true, comment.getCreatedTime().getMillis()));
                        }
                        if (r.getBody().getData().size() < 20) {
                            break;
                        }
                    }
                    loopIndex++;
                }
            } catch (IOException | ServiceException e) {
                System.out.println("Exception in getMsgs(): " + e.getMessage());
            }
            synchronized (this) {
                doneExecuting = true;
                notifyAll();
            }
        }
    }

    private class SendMessagesRunnable implements Runnable {
        public List<Boolean> successes;
        private final List<List<ESMessage>> msgsToSendBatches;

        public SendMessagesRunnable(List<ESMessage> msgsToSend) {
            this.msgsToSendBatches = Utils.getBatches(msgsToSend);
        }

        @Override
        public void run() {
            for (List<ESMessage> msgsToSend : msgsToSendBatches) {
                EmbeddedSocialBatchedClientImpl batchOps = new EmbeddedSocialBatchedClientImpl.Builder()
                        .baseUrl(baseURL)
                        .batchSize(msgsToSend.size())
                        .build();
                CountDownLatch latch = new CountDownLatch(msgsToSend.size());
                this.successes = new ArrayList<>(msgsToSend.size());
                String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
                for (int i = 0; i < msgsToSend.size(); i++) {
                    successes.add(null);
                    new SendMsgAction(batchOps, latch, i, auth, msgsToSend.get(i).getTopicHandle(), msgsToSend.get(i).getMsgText(), successes)
                            .addToBatch();
                }
                try {
                    Response resp = batchOps.issueBatch();
                    latch.await();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void markPriorMessagesRead(String cursor) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        if (cursor == null) {
            return;
        }
        String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
        PutNotificationsStatusRequest req = new PutNotificationsStatusRequest();
        req.setReadActivityHandle(cursor);
        try {
            esclientOps.getMyNotificationsOperations().putNotificationsStatus(req, auth);
        } catch (ServiceException | IOException e) {
            e.printStackTrace();
        }

    }

    public List<ESMessage> getMessagesOnTopic(String topichandle) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        GetMessagesOnTopicRunnable getMessagesOnTopicRunnable = new GetMessagesOnTopicRunnable(topichandle, null);
        executorService.execute(getMessagesOnTopicRunnable);
        synchronized (getMessagesOnTopicRunnable) {
            if (!getMessagesOnTopicRunnable.doneExecuting) {
                try {
                    getMessagesOnTopicRunnable.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return getMessagesOnTopicRunnable.msgs;
    }

    public Pair<String, List<ESMessage>> getUnreadPageMessages(String cursor, int pageSize) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        GetMessagesRunnable getMessagesRunnable = new GetMessagesRunnable(cursor, pageSize, true);
        executorService.execute(getMessagesRunnable);
        synchronized (getMessagesRunnable) {
            if (!getMessagesRunnable.doneExecuting) {
                try {
                    getMessagesRunnable.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return new ImmutablePair<>(getMessagesRunnable.continuationCursor, getMessagesRunnable.msgs);
    }

    public Pair<String, List<ESMessage>> getPageMessages(String cursor, int pageSize) {
        Utils.myAssert(!Thread.currentThread().getName().contains("main"));
        GetMessagesRunnable getMessagesRunnable = new GetMessagesRunnable(cursor, pageSize, false);
        executorService.execute(getMessagesRunnable);
        synchronized (getMessagesRunnable) {
            if (!getMessagesRunnable.doneExecuting) {
                try {
                    getMessagesRunnable.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return new ImmutablePair<>(getMessagesRunnable.continuationCursor, getMessagesRunnable.msgs);
    }

    // TODO does not return true/false status
    public void sendOneMessage(String msgText, String topicHandle) {
        String auth = GlobalObjectRegistry.getObject(ESCredentials.class).getAuth();
        PostCommentRequest req = new PostCommentRequest();
        req.setText(msgText);
        try {
            esclientOps.getTopicCommentsOperations().postComment(topicHandle, req, auth);
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // TODO does not return true/false status
    public void sendMsgs(List<ESMessage> msgsToSend) {
        if (msgsToSend.size() == 0) return;
        SendMessagesRunnable sendMessagesRunnable = new SendMessagesRunnable(msgsToSend);
        executorService.execute(sendMessagesRunnable);
    }


    public void reportMsg(String cursor, Reason reason) {
        PostReportRequest req = new PostReportRequest();
        req.setReason(reason);
        try {
            esclientOps.getCommentReportsOperations().postReport(cursor, req,
                    GlobalObjectRegistry.getObject(ESCredentials.class).getAuth());
        } catch (ServiceException | IOException e) {
            e.printStackTrace();
        }
        return;
    }
}