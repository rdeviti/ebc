package org.mpisws.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Utils {

    private static final String TAG = getTAG(Utils.class);
    public static final int BATCH_SIZE = 32;

    public static String getTAG(final Class c) {
        return "EbN-Java-" + c.getSimpleName();
    }

    /** Read the object from Base64 string. */
    public static Object objFromString(String s) throws IOException ,
            ClassNotFoundException {
        byte [] data = Base64.getDecoder().decode( s );
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    /** Write the object to a Base64 string. */
    public static String objToString(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    public static <T> List<List<T>> getBatches(List<T> collection){
        int i = 0;
        List<List<T>> batches = new ArrayList<>();
        while(i<collection.size()){
            int nextInc = Math.min(collection.size()-i,BATCH_SIZE);
            List<T> batch = collection.subList(i,i+nextInc);
            batches.add(batch);
            i = i + nextInc;
        }

        return batches;
    }

    public static byte[] getHash(final byte[] target, final int numBytes) {
        final MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
        md.update(target, 0, target.length);
        final byte[] hash = md.digest();
        if (numBytes > hash.length) {
            throw new IllegalStateException("Requested " + numBytes + "-byte hash, algorithm only produced " + hash.length);
        }
        final byte[] truncatedHash = new byte[numBytes];
        System.arraycopy(hash, 0, truncatedHash, 0, numBytes);
        return truncatedHash;
    }

    public static String collectionToStringV2(final Collection<?> list, final String separator) {
        final StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Object s : list) {
            if (!first) {
                sb.append(separator);
            }
            sb.append(s.toString());
            first = false;
        }
        return sb.toString();
    }

    public static void myAssert(final boolean p) {
        if (!p) {
            throw new AssertionError();
        }
    }

    public static void myAssert(final boolean p, final String msg) {
        if (!p) {
            throw new AssertionError(msg);
        }
    }

    public static String getHexString(final byte[] b) {
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
            result.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString().toUpperCase();
    }

    public static byte[] hexStringToByteArray(final String s) {
        myAssert(s.length() % 2 == 0);
        final byte[] bytes = new byte[s.length() / 2];
        for (int i = 0; i < s.length() / 2; i++) {
            bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    public static double median(final List<Long> collection) {
        Collections.sort(collection);
        final int middle = collection.size() / 2;
        if (collection.size() % 2 == 1) {
            // Odd number of elements -- return the middle one
            return collection.get(middle);
        } else {
            // Even number -- return average of middle two
            return ((double) (collection.get(middle - 1) + collection.get(middle))) / 2d;
        }
    }

    public static int bytesToInt(final byte[] bytes) {
        int i = 0;
        i += (int) bytes[0] & 0xFF;
        i += ((int) bytes[1] & 0xFF) << 8;
        i += ((int) bytes[2] & 0xFF) << 16;
        i += ((int) bytes[3] & 0xFF) << 24;
        return i;
    }

    public static byte[] intToBytes(final int i) {
        final byte[] bytes = new byte[4];
        bytes[0] = (byte) (i & 0xFF);
        bytes[1] = (byte) ((i >> 8) & 0xFF);
        bytes[2] = (byte) ((i >> 16) & 0xFF);
        bytes[3] = (byte) ((i >> 24) & 0xFF);
        return bytes;
    }

    public static void streamCopy(final InputStream inStream, final OutputStream outStream) throws IOException {
        final byte[] buffer = new byte[4096];
        int count;
        while ((count = inStream.read(buffer)) >= 0) {
            outStream.write(buffer, 0, count);
        }
        inStream.close();
        outStream.close();
    }

    public static BigInteger ack(BigInteger m, BigInteger n) {
        return m.equals(BigInteger.ZERO)
                ? n.add(BigInteger.ONE)
                : ack(m.subtract(BigInteger.ONE),
                n.equals(BigInteger.ZERO) ? BigInteger.ONE : ack(m, n.subtract(BigInteger.ONE)));
    }

    public static byte[] SHA1(byte[] toHash) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md.digest(toHash);
    }

    public static boolean bytesAreAllZeros(byte[] oldnonce) {
        int sum = 0;
        for (byte b : oldnonce) {
            sum |= b;
        }
        return (sum == 0);
    }
}
