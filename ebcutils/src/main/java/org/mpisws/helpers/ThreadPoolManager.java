package org.mpisws.helpers;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadPoolManager {
    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);

    private static ThreadPoolManager instance = new ThreadPoolManager();

    private ThreadPoolManager() {};

    public static ThreadPoolManager getInstance() {
        return instance;
    }

    public void runTask(Runnable r) {
        executor.execute(r);
    }
}
