package org.mpisws.helpers;

/**
 * Created by tslilyai on 2/15/18.
 */

public class ESCredentials {
    private static final String TAG = ESCredentials.class.getSimpleName();

    private String esAPIKey;
    private String userHandle;
    private String auth;
    public ESCredentials() {}

    public String getUserHandle() {
        return userHandle;
    }

    public void setUserHandle(String userHandle) {
        this.userHandle = userHandle;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getEsAPIKey() {
        return esAPIKey;
    }

    public void setEsAPIKey(String esAPIKey) {
        this.esAPIKey = esAPIKey;
    }
}
