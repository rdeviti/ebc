package org.mpisws.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by tslilyai on 2/15/18.
 */

public class Coder {

    public static Identifier convertSharedSecretToID(Identifier secret) {
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-1");
            if (secret != null) {
                md.update(secret.getBytes(), 0, secret.getBytes().length);
                return new Identifier(md.digest());
            } else {
                return null;
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String encrypt(String strToEncrypt, byte[] key) {

        if (key== null) {
            return strToEncrypt;
        }
        try
        {
            SecretKeySpec secretKey;
            MessageDigest sha;
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(new byte[16]));
            String result = Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
            System.out.println("CODER: Encrypting message \n"
                + strToEncrypt
                + "\nwith secret " + new Identifier(key).toString()
                + "\nto " + result);
            return result;
        }
        catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, byte[] secret)
    {
        if (secret == null)
            return strToDecrypt;
        try
        {
            SecretKeySpec secretKey;
            MessageDigest sha;
            sha = MessageDigest.getInstance("SHA-1");
            secret = sha.digest(secret);
            secret = Arrays.copyOf(secret, 16);
            System.out.println("CODER: Decrypting message " + strToDecrypt + " \nwith secret " + new Identifier(secret).toString());
            secretKey = new SecretKeySpec(secret, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(new byte[16]));
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return strToDecrypt;
    }
}
