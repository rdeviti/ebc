package org.mpisws.exampleapp;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.microsoft.embeddedsocial.autorest.models.Reason;

import org.mpisws.embeddedsocial.ESClient;
import org.mpisws.encounters.EncounterBasedCommunication;
import org.mpisws.messaging.EncounterForwardingMessage;
import org.mpisws.messaging.PrivateMessage;
import org.mpisws.messaging.ReceivedMessageWrapper;
import org.mpisws.messaging.constraints.EncounterQueryConstraint;
import org.mpisws.messaging.constraints.SpaceTimeIntersectConstraint;
import org.mpisws.messaging.constraints.TrajectoryConstraint;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static org.mpisws.encounters.EncounterBasedCommunication.REQUEST_ENABLE_BT;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks {
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private static final int REQUEST_PERMISSION_GET_LOCATION = 1004;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = { "openid", "profile"};
    private static final String TAG = "EbCExampleApp";
    public static GoogleAccountCredential mCredential;

    private static EncounterBasedCommunication ebc = null;

    public static EncounterBasedCommunication getEbC(Context context){
        if (ebc == null) {
            ebc = EncounterBasedCommunication.getInstance();
            ebc.initialize(context, R.raw.ebc_config);
            try {
                ebc.getUserAccountClient().loginAccountWithGoogle(mCredential.getToken());
            } catch (IOException | GoogleAuthException e) {
                e.printStackTrace();
            }
        }
        return ebc;
    }

    /**
     * Create the main activity.
     *
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.signOut).setOnClickListener(this);
        findViewById(R.id.deleteAccount).setOnClickListener(this);
        findViewById(R.id.sendMsgs).setOnClickListener(this);
        findViewById(R.id.getMsgs).setOnClickListener(this);
        findViewById(R.id.startEfC).setOnClickListener(this);
        findViewById(R.id.stopEfC).setOnClickListener(this);

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                this, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        initializeApp();
    }

    private void initializeApp() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (!isGooglePlayServicesAvailable()) {
            Log.d(TAG, "acquire googleplay");
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
            Log.d(TAG, "choose account " + accountName);
            chooseAccount();
        } else if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "get location perm");
            getLocationPermissions();
        } else if (bluetoothManager.getAdapter() == null || ! bluetoothManager.getAdapter().isEnabled()) {
            Log.v(TAG, "Bluetooth not enabled");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            Log.d(TAG, "init ebc");
            new Thread(() -> ebc = getEbC(this)).start();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.signOut:
                getEbC(this).getUserAccountClient().signOut();
                break;
            case R.id.deleteAccount:
                getEbC(this).getUserAccountClient().deleteAccount();
                break;
            case R.id.sendMsgs:
                /* EXAMPLE: SENDING TO ENCOUNTERS WITHIN A SPECIFIED SPACE-TIME REGION */
                Long curTime = System.currentTimeMillis();
                EncounterQueryConstraint encounterConstraintSpaceTime = new SpaceTimeIntersectConstraint(
                        curTime - 60*20*1000,
                        curTime,
                        60*5*1000,
                        new EncounterQueryConstraint.SpaceRegion(
                                12003,
                                4000,
                                100),
                        null /*non-null values constrains to encounters overlapping with the specified time value*/);

                /* EXAMPLE: SENDING TO ENCOUNTERS THAT OCCURRED ON YOUR PATH (TRAJECTORY) */
                EncounterQueryConstraint encounterConstraintTrajectory = new TrajectoryConstraint(
                        System.currentTimeMillis()-(60*1)*1000, System.currentTimeMillis(),
                        (long)(60*10*1000),
                        false, // should use causality as a constraint
                        false // should project into the future
                         );

                getEbC(this).getCommunicationClient().sendMultiHopMessageToEncounters("msg_text", encounterConstraintSpaceTime, null, false);
                getEbC(this).getCommunicationClient().sendDirectMessageToEncounters("msg_text", encounterConstraintTrajectory, true);
                break;
            case R.id.getMsgs:
                ESClient.GetReceivedMessagesCallback callback = new ESClient.GetReceivedMessagesCallback() {
                    @Override
                    public void processReceivedMessages(List<ReceivedMessageWrapper> unreadMsgs) {
                        for (ReceivedMessageWrapper msg : unreadMsgs) {
                            String msgText;
                            switch (msg.getMsgType()) {
                                case ENCOUNTER_FORWARDING_MESSAGE:
                                    EncounterForwardingMessage encounterForwardingMessage = msg.getEncounterForwardingMessage();
                                    // you can read the message
                                    msgText = encounterForwardingMessage.getMsgText();
                                    break;
                                case PRIVATE_MESSAGE:
                                    PrivateMessage privateMessage = msg.getPrivateMessage();
                                    msgText = privateMessage.getMsgText();
                                    break;
                                default: // skip non-messaging type messages posted on embedded social
                                    msgText = "";
                                    break;
                            }
                            // you can get the topic text, i.e. the EncounterID to which this message corresponds
                            String whichEncounter = msg.getEncounterID();
                            // you can also report offensive text
                            if (isOffensive(msgText)) {
                                getEbC(getApplicationContext()).getCommunicationClient().reportMsg(msg.getCursor(), Reason.OFFENSIVECONTENT);
                            }
                        }
                    }
                };
                getEbC(this).getCommunicationClient().getUnreadMsgs(callback);
                break;
            case R.id.startEfC:
                boolean want_to_communicate_over_bluetooth = true;
                // start the encounter formation service running in the background
                getEbC(this).getSDDRClient().startFormingEncounters();

                if (isConnected()) {
                    // confirm all unconfirmed encounters (opening communication channels for these encounters)
                    // over the network. This requires WiFi or data to succeed.
                    getEbC(this).getSDDRClient().confirmAndCommunicateUsingNetwork();
                } else if (want_to_communicate_over_bluetooth) {
                    // confirm all unconfirmed encounters (opening communication channels for these encounters)
                    // over a Bluetooth connection. This requires the other end of the encounter to be within range
                    // and be available to connect over Bluetooth.
                    getEbC(this).getSDDRClient().confirmAndCommunicateUsingBLE();
                } else {
                    // all encounters will remain unconfirmed (and un-communicable) in
                    // the device's database. Encounter confirmation over the network
                    // should be initialiated when network access becomes available to
                    // enable communication with prior encounters.
                }
                break;
            case R.id.stopEfC:
                // if application wishes to stop encounter confirmation
                // (over either the network or BLE)
                getEbC(this).getSDDRClient().disableConfirmation();

                // when application wishes to stop encounter formation
                getEbC(this).getSDDRClient().stopFormingEncounters();
                break;
        }
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private boolean isOffensive(String msg) {
        return msg.contains("swear word");
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                Log.d(TAG, "Got account name! " + accountName);
                mCredential.setSelectedAccountName(accountName);
                Log.d(TAG, "Set account name! " + mCredential.getSelectedAccountName());
                initializeApp();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    public void getLocationPermissions() {
        // Request the FINE_LOCATION permission via a user dialog
        EasyPermissions.requestPermissions(
                this,
                "This app needs to access your location.",
                REQUEST_PERMISSION_GET_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode code indicating the result of the incoming
     *     activity result.
     * @param data Intent (containing result data) returned by incoming
     *     activity result.
     */
    @Override
    public void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(this,"This app requires Google Play Services. " +
                            "Please install Google Play Services on your device " +
                            "and relaunch this app.",
                            Toast.LENGTH_LONG).show();
                } else {
                    initializeApp();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        initializeApp();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) initializeApp();
                break;
            case REQUEST_ENABLE_BT:
                initializeApp();
                break;
        }
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     * @param requestCode The request code passed in
     *     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    /**
     * Callback for when a permission is granted using the EasyPermissions
     * library.
     * @param requestCode The request code associated with the requested
     *         permission
     * @param list The requested permission list. Never null.
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        if (requestCode == REQUEST_PERMISSION_GET_LOCATION)
            initializeApp();
    }

    /**
     * Callback for when a permission is denied using the EasyPermissions
     * library.
     * @param requestCode The request code associated with the requested
     *         permission
     * @param list The requested permission list. Never null.
     */
    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     * @return true if Google Play Services is available and up to
     *     date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode, MainActivity.this);
        }
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    public static void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode, Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                activity,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }
}

